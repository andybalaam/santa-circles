use actix_web::http::Cookie;
use speculoos::prelude::*;
use time::Duration;

mod util;

use crate::util::fake_api::FakeApi;
use crate::util::faulty_api::FaultyApi;
use crate::util::requests::*;

#[test]
fn flow_1_requesting_root_without_cookie_redirects_to_login() {
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    let resp = get_nocookie(&mut app, "/");
    assert_eq!(resp.status(), 302);
    assert_contains_header(&resp, "Location", "http://localhost:8080/login");
}

#[test]
fn flow_2_login_page_presents_a_form() {
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    let resp = get_nocookie(&mut app, "/login");
    let body = assert_status(resp, 200);
    assert_that!(body).contains(r#"<button type="submit">Log in</button>"#);
    assert_that!(body)
        .does_not_contain("Login failed or timed out - please try again");
}

#[test]
fn flow_2_login_page_after_failure_shows_error() {
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    let resp = get_nocookie(&mut app, "/login?error=login_failed");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("Login failed or timed out - please try again");
}

#[test]
fn flow_3a_submitting_incorrect_credentials_redirects_to_login() {
    let mut api = Box::new(FakeApi::new());
    api.add_user("a", "a");
    let mut app = new_app(api);
    let resp = post_form_no_cookie(&mut app, "/", "username=a&password=b");
    assert_eq!(resp.status(), 302);
    assert_contains_header(
        &resp,
        "Location",
        "http://localhost:8080/login?error=login_failed",
    );
}

#[test]
fn flow_3b_submitting_correct_credentials_provides_redirect_and_cookie() {
    let mut api = Box::new(FakeApi::new());
    api.add_user("a", "a");
    let mut app = new_app(api);

    // Correct login
    let resp =
        post_form_no_cookie(&mut app, "/", "username=%20a%20&password=a");

    // Redirects to user page, and provides an auth cookie
    assert_eq!(resp.status(), 302);
    assert_eq!(resp.cookies().next().unwrap().name(), "auth");
    assert_contains_header(&resp, "Location", "http://localhost:8080/circles");
}

#[test]
fn flow_4_cookie_from_correct_login_works_to_access_page() {
    let mut api = Box::new(FakeApi::new());
    api.add_user("a", "a");
    let mut app = new_app(api);

    // Log in and get auth cookie
    let mut resp = post_form_no_cookie(&mut app, "/", "username=a&password=a");
    assert_eq!(resp.status(), 302);
    assert_contains_header(&resp, "Location", "http://localhost:8080/circles");
    let auth_cookie: Cookie = resp.cookies().next().unwrap();
    assert_eq!(auth_cookie.name(), "auth");

    // Using the cookie works to get us the circles page
    resp = get(&mut app, &auth_cookie, "/circles");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("My Circles");
}

#[test]
fn remember_me_stores_max_age_in_cookie() {
    let mut api = Box::new(FakeApi::new());
    api.add_user("a", "a");
    let mut app = new_app(api.clone());

    // When I say "remember_me=on"
    let resp = post_form_no_cookie(
        &mut app,
        "/",
        "username=a&password=a&remember_me=on",
    );

    // Then a max_age is set on my cookie
    assert_eq!(resp.status(), 302);
    let auth_cookie: Cookie = resp.cookies().next().unwrap();
    assert_eq!(auth_cookie.name(), "auth");
    assert_eq!(auth_cookie.max_age(), Some(Duration::days(30)));

    // And the API was told to remember the token
    assert_eq!(api.get_remember_me("a"), Some(true));
}

#[test]
fn remember_me_missing_does_not_store_max_age_in_cookie() {
    let mut api = Box::new(FakeApi::new());
    api.add_user("a", "a");
    let mut app = new_app(api.clone());

    // When I don't mention "remember_me"
    let resp = post_form_no_cookie(&mut app, "/", "username=a&password=a");

    // Then the max_age is not set on my cookie
    assert_eq!(resp.status(), 302);
    let auth_cookie: Cookie = resp.cookies().next().unwrap();
    assert_eq!(auth_cookie.name(), "auth");
    assert_eq!(auth_cookie.max_age(), None);

    // And the API was told not to remember the token
    assert_eq!(api.get_remember_me("a"), Some(false));
}

#[test]
fn remember_me_off_does_not_store_max_age_in_cookie() {
    let mut api = Box::new(FakeApi::new());
    api.add_user("a", "a");
    let mut app = new_app(api.clone());

    // When I say "remember_me=off"
    let resp = post_form_no_cookie(
        &mut app,
        "/",
        "username=a&password=a&remember_me=off",
    );

    // Then the max_age is not set on my cookie
    assert_eq!(resp.status(), 302);
    let auth_cookie: Cookie = resp.cookies().next().unwrap();
    assert_eq!(auth_cookie.name(), "auth");
    assert_eq!(auth_cookie.max_age(), None);

    // And the API was told not to remember the token
    assert_eq!(api.get_remember_me("a"), Some(false));
}

#[test]
fn logging_out_clears_auth_cookie_and_deletes_token() {
    // Given I am logged in, and my cookie is not empty
    let api = Box::new(FakeApi::new());
    let (mut app, cookie) = logged_in_with_api("myuser", api.clone());
    assert_eq!(cookie.name(), "auth");
    assert_ne!(cookie.value(), "");

    // When I click "Log out"
    let resp = post_form(&mut app, &cookie, "/login", "");
    assert_eq!(200, resp.status());
    let new_cookie = resp.cookies().next().unwrap().into_owned();

    // Then the  session cookie has been wiped
    assert_eq!(new_cookie.name(), "auth");
    assert_eq!(new_cookie.value(), "");
    assert_eq!(new_cookie.max_age(), Some(Duration::seconds(0)));

    // And the token was deleted
    assert_eq!(api.get_deleted_tokens("myuser"), vec!["myuser-token"])
}

#[test]
fn with_no_circles_root_page_is_empty() {
    let (mut app, cookie) = logged_in_as("myuser");

    // No circles are displayed because we didn't create any
    let resp = get(&mut app, &cookie, "/circles");
    let body = assert_success(resp);
    assert_that!(body).contains("You are not in any circles yet");
}

#[test]
fn circles_page_lists_my_circles() {
    let (mut app, cookie) = logged_in_as("myuser");
    // Create some circles
    let postres1 = post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle1&organiser-name-1=&organiser-email-1=myuser",
    );
    let postres2 = post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle2&organiser-name-1=&organiser-email-1=myuser",
    );
    let postres3 = post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle3&member-name-1=&member-email-1=myuser&\
        organiser-name-1=&organiser-email-1=myuser",
    );

    // We get redirected to the page for the circle that was created
    assert_status(postres1, 302);
    assert_status(postres2, 302);
    assert_status(postres3, 302);

    // The circles should be listed
    let resp = get(&mut app, &cookie, "/circles");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("My Circle1");
    assert_that!(body).contains("My Circle2");
    assert_that!(body).contains("My Circle3");
}

#[test]
fn creating_a_circle_enables_us_to_organise_it() {
    // Given a circle
    let (mut app, cookie) = logged_in_as("myuser");
    post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&organiser-name-1=&organiser-email-1=%20myuser%20",
    );

    // When we ask to organise it
    let resp = get(&mut app, &cookie, "/organisecircle/id1");

    // Then we see a page for organising it
    let body = assert_status(resp, 200);
    assert_that!(body).contains("My Circle");
    assert_that!(body).contains("member-email-1");
    assert_that!(body).contains("member-email-2");
    assert_that!(body).does_not_contain("member-email-3");
    assert_that!(body).contains("organiser-email-1");
    assert_that!(body).contains("organiser-email-2");
    assert_that!(body).contains("organiser-email-3");
    assert_that!(body).does_not_contain("organiser-email-4");
}

#[test]
fn empty_member_and_organiser_lines_are_ignored_on_create() {
    // When we create a circle with empty members
    let (mut app, cookie) = logged_in_as("myuser");
    let create_resp = post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        organiser-name-2=&organiser-email-2=&\
        organiser-name-3=&organiser-email-3=&\
        organiser-name-4=O4&organiser-email-4=o4&\
        member-name-1=&member-email-1=&\
        member-name-2=&member-email-2=mem&\
        member-name-3=&member-email-3=&\
        ",
    );
    assert_status(create_resp, 302);

    // Then those members are not created
    let resp = get(&mut app, &cookie, "/organisecircle/id1");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("My Circle");
    assert_that!(body).contains("member-email-1");
    assert_that!(body).contains("mem");
    assert_that!(body).contains("member-email-2");
    assert_that!(body).contains("member-email-3");
    assert_that!(body).does_not_contain("member-email-4");
    assert_that!(body).contains("organiser-email-1");
    assert_that!(body).contains("myuser");
    assert_that!(body).contains("organiser-email-2");
    assert_that!(body).contains("o4");
    assert_that!(body).contains("organiser-email-3");
    assert_that!(body).contains("organiser-email-4");
    assert_that!(body).does_not_contain("organiser-email-5");
}

#[test]
fn empty_member_and_organiser_lines_are_ignored_on_update() {
    // Given we have a circle
    let (mut app, cookie) = logged_in_as("myuser");
    let create_resp = post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&organiser-name-1=&organiser-email-1=myuser",
    );
    assert_status(create_resp, 302);

    // When we update it with empty members
    let update_resp = post_form(
        &mut app,
        &cookie,
        "/organisecircle/id1",
        "name=My%20Circle&organiser-name-1=&organiser-email-1=%20myuser%20&organiser-name-2=&organiser-email-2=&organiser-name-3=&organiser-email-3=&member-name-1=&member-email-1=&member-name-2=&member-email-2=",
    );
    assert_status(update_resp, 302);

    // Then those members are not created
    let resp = get(&mut app, &cookie, "/organisecircle/id1");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("My Circle");
    assert_that!(body).contains("member-email-1");
    assert_that!(body).contains("member-email-2");
    assert_that!(body).does_not_contain("member-email-3");
    assert_that!(body).contains("organiser-email-1");
    assert_that!(body).contains("organiser-email-2");
    assert_that!(body).contains("organiser-email-3");
    assert_that!(body).does_not_contain("organiser-email-4");
}

#[test]
fn empty_circle_names_are_listed_as_unnamed() {
    let (mut app, cookie) = logged_in_as("myuser");
    // Create a circle with no name
    let postres = post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=&organiser-name-1=&organiser-email-1=myuser\
        &member-name-1=&member-email-1=myuser",
    );
    assert_status(postres, 302);

    // It should be displayed as "(unnamed)"
    let resp = get(&mut app, &cookie, "/circles");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("(unnamed)");
}

#[test]
fn creating_a_circle_with_info_saves_it() {
    // When we create a circle with draw_date and rules
    let (mut app, cookie) = logged_in_as("myuser");
    let create_resp = post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        draw_date=tomorrow&\
        rules=No%20buying%20stuff",
    );
    assert_status(create_resp, 302);

    // Then those details are saved and reflected
    let resp = get(&mut app, &cookie, "/organisecircle/id1");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("My Circle");
    assert_that!(body)
        .contains(r#"id="draw_date" name="draw_date" value="tomorrow""#);
    assert_that!(body)
        .contains(r#"id="rules" name="rules" value="No buying stuff""#);
}

#[test]
fn creating_a_circle_with_restrictions_saves_them() {
    // When we create a circle with restrictions
    let (mut app, cookie) = logged_in_as("myuser");
    let create_resp = post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=%20m1@example.com&\
        member-name-2=&member-email-2=m2@example.com%20&\
        member-name-3=&member-email-3=m3&\
        member-name-4=&member-email-4=m4&\
        restriction-from-1=m1@example.com&restriction-to-1=m2@example.com%20&\
        restriction-from-2=m2@example.com&restriction-to-2=%20m3",
    );
    assert_status(create_resp, 302);

    // Then those restrictions are saved and reflected
    let resp = get(&mut app, &cookie, "/organisecircle/id1");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("My Circle");
    assert_that!(body).contains(concat!(
        r#"restriction-from-1">"#,
        r#"<option value=""></option>"#,
        r#"<option value="m1@example.com" selected"#,
    ));
    assert_that!(body).contains(concat!(
        r#"restriction-to-1">"#,
        r#"<option value=""></option>"#,
        r#"<option value="m1@example.com">m1@example.com</option>"#,
        r#"<option value="m2@example.com" selected"#,
    ));
    assert_that!(body).contains(concat!(
        r#"restriction-from-2">"#,
        r#"<option value=""></option>"#,
        r#"<option value="m1@example.com">m1@example.com</option>"#,
        r#"<option value="m2@example.com" selected>"#,
    ));
    assert_that!(body).contains(concat!(
        r#"restriction-to-2">"#,
        r#"<option value=""></option>"#,
        r#"<option value="m1@example.com">m1@example.com</option>"#,
        r#"<option value="m2@example.com">m2@example.com</option>"#,
        r#"<option value="m3" selected"#,
    ));
}

#[test]
fn updating_a_circle_with_restrictions_saves_them() {
    // Given we have a circle
    let (mut app, cookie) = logged_in_as("myuser");
    let create_resp = post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=m1@example.com&\
        member-name-2=&member-email-2=m2@example.com&\
        member-name-3=&member-email-3=%20m3%20&\
        member-name-4=&member-email-4=m4",
    );
    assert_status(create_resp, 302);

    // When we update it with restrictions
    let create_resp = post_form(
        &mut app,
        &cookie,
        "/organisecircle/id1",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=m1@example.com&\
        member-name-2=&member-email-2=m2@example.com&\
        member-name-3=&member-email-3=m3&\
        member-name-4=&member-email-4=m4&\
        restriction-from-1=m1@example.com&restriction-to-1=%20m2@example.com%20&\
        restriction-from-2=m2@example.com&restriction-to-2=m3",
    );
    assert_status(create_resp, 302);

    // Then those restrictions are saved and reflected
    let resp = get(&mut app, &cookie, "/organisecircle/id1");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("My Circle");
    assert_that!(body).contains(concat!(
        r#"restriction-from-1">"#,
        r#"<option value=""></option>"#,
        r#"<option value="m1@example.com" selected"#,
    ));
}

#[test]
fn copying_a_circle_gives_a_create_circle_page_with_copied_info() {
    // Given we have a circle
    let (mut app, cookie) = logged_in_as("myuser");
    let create_resp = post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=m1@example.com&\
        member-name-2=&member-email-2=m2@example.com&\
        member-name-3=&member-email-3=%20m3%20&\
        member-name-4=&member-email-4=m4",
    );
    assert_status(create_resp, 302);

    // When we click "Make a copy"
    let create_resp = post_form(
        &mut app,
        &cookie,
        // Note: it is slightly odd, but harmless, that we are still on the
        // page with the old id even though we are about to create a new circle
        "/organisecircle/id1",
        "copy_circle=Make%20a%20copy&\
        name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=m1@example.com&\
        member-name-2=&member-email-2=m2@example.com&\
        member-name-3=&member-email-3=m3&\
        member-name-4=&member-email-4=m4&\
        draw-from-1=m3&draw-to-1=m4&\
        draw-from-2=m4&draw-to-2=m3&\
        restriction-from-1=m1@example.com&restriction-to-1=%20m2@example.com%20&\
        restriction-from-2=m2@example.com&restriction-to-2=m3",
    );

    // Then we see a create circle page with our info copied
    let body = assert_status(create_resp, 200);
    assert_that!(body).contains("Copy of My Circle");
    assert_that!(body).contains("Create a copy of a circle");
    // members:
    assert_that!(body).contains(r#"name="member-email-4" value="m4""#);
    // organisers:
    assert_that!(body).contains(r#"name="organiser-email-1" value="myuser""#);
    // restrictions:
    assert_that!(body)
        .contains(r#"<option value="m1@example.com" selected>m1@example.com"#);
    // but no draw:
    assert_that!(body).contains("The names have not yet been drawn");
}

#[test]
fn empty_restrictions_are_ignored() {
    // Given we have a circle
    let (mut app, cookie) = logged_in_as("myuser");
    let create_resp = post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=m1@example.com&\
        member-name-2=&member-email-2=m2@example.com&\
        member-name-3=&member-email-3=m3&\
        member-name-4=&member-email-4=m4",
    );
    assert_status(create_resp, 302);

    // When we update it with restrictions
    let create_resp = post_form(
        &mut app,
        &cookie,
        "/organisecircle/id1",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=m1@example.com&\
        member-name-2=&member-email-2=m2@example.com&\
        member-name-3=&member-email-3=m3&\
        member-name-4=&member-email-4=m4&\
        restriction-from-1=m1@example.com&restriction-to-1=m2@example.com&\
        restriction-from-2=&restriction-to-2=%20&\
        restriction-from-3=m2@example.com&restriction-to-3=m3",
    );
    assert_status(create_resp, 302);

    // Then those restrictions are saved and reflected
    let resp = get(&mut app, &cookie, "/organisecircle/id1");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("My Circle");
    assert_that!(body).contains(concat!(
        r#"restriction-from-1">"#,
        r#"<option value=""></option>"#,
        r#"<option value="m1@example.com" selected"#,
    ));
    assert_that!(body).contains(concat!(
        r#"restriction-from-2">"#,
        r#"<option value=""></option>"#,
        r#"<option value="m1@example.com""#, // Not selected, because 2 is
    ));
    assert_that!(body).contains("restriction-from-3");
    assert_that!(body).contains("restriction-from-4");
    assert_that!(body).does_not_contain("restriction-from-5");
}

#[test]
fn requesting_to_draw_does_it() {
    // Given a circle
    let (mut app, cookie) = logged_in_as("myuser");
    let create_resp = post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=m1@example.com&\
        member-name-2=&member-email-2=m2@example.com&\
        member-name-3=&member-email-3=m3&\
        member-name-4=&member-email-4=m4&",
    );
    assert_status(create_resp, 302);

    // When we request a draw
    let create_resp = post_form(
        &mut app,
        &cookie,
        "/organisecircle/id1",
        "name=My%20Circle&\
        draw_now=Draw+now!&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=m1@example.com&\
        member-name-2=&member-email-2=m2@example.com&\
        member-name-3=&member-email-3=%20m3&\
        member-name-4=&member-email-4=m4%20&",
    );
    assert_status(create_resp, 302);

    // A draw is done
    let resp = get(&mut app, &cookie, "/organisecircle/id1");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("My Circle");
    assert_that!(body).does_not_contain("The names have not yet been drawn");
    assert_that!(body).contains(concat!(
        r#"<option value="m4">m4</option></select>"#,
        r#"</td><td>is giving to</td><td class="reveal"><details>"#,
        r#"<select id="draw-to-1" name="draw-to-1">"#,
        r#"<option value=""></option>"#,
        r#"<option value="m1@example.com">m1@example.com</option>"#,
        r#"<option value="m2@example.com" selected>"#,
        r#"m2@example.com</option>"#,
        r#"<option value="m3">m3</option>"#,
        r#"<option value="m4">m4</option></select>"#,
    ));
}

#[test]
fn saving_a_draw_persists_it() {
    // When I create a circle with a draw
    let (mut app, cookie) = logged_in_as("myuser");
    let create_resp = post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=m1&\
        member-name-2=&member-email-2=m2&\
        restriction-from-1=m1&restriction-to-1=m2&\
        draw-from-1=%20m1%20&draw-to-1=m2&\
        draw-from-2=m2&draw-to-2=%20m1%20&\
        ",
    );
    assert_status(create_resp, 302);

    // The draw is saved
    let resp = get(&mut app, &cookie, "/organisecircle/id1");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("My Circle");
    assert_that!(body).does_not_contain("The names have not yet been drawn");
    assert_that!(body).contains(concat!(
        r#"<table class="draw"><tr><td>"#,
        r#"<select id="draw-from-1" name="draw-from-1">"#,
        r#"<option value=""></option>"#,
        r#"<option value="m1" selected>m1</option>"#,
        r#"<option value="m2">m2</option>"#,
        r#"</select></td><td>is giving to</td>"#,
        r#"<td class="reveal"><details>"#,
        r#"<select id="draw-to-1" name="draw-to-1">"#,
        r#"<option value=""></option>"#,
        r#"<option value="m1">m1</option>"#,
        r#"<option value="m2" selected>m2</option>"#,
        r#"</select><summary>(click to reveal)</summary>"#,
        r#"</details></td></tr>"#,
    ));
}

#[test]
fn empty_draw_items_are_ignored() {
    // When I create a circle with a draw
    let (mut app, cookie) = logged_in_as("myuser");
    let create_resp = post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=m1&\
        member-name-2=&member-email-2=m2&\
        restriction-from-1=m1&restriction-to-1=m2&\
        draw-from-1=m1&draw-to-1=m2&\
        draw-from-2=%20&draw-to-2=&\
        draw-from-3=&draw-to-3=%20&\
        draw-from-4=m2&draw-to-4=m1&\
        draw-from-5=&draw-to-5=&\
        ",
    );
    assert_status(create_resp, 302);

    // The draw is saved
    let resp = get(&mut app, &cookie, "/organisecircle/id1");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("My Circle");
    assert_that!(body).does_not_contain("The names have not yet been drawn");
    assert_that!(body).contains(concat!(
        r#"<table class="draw"><tr><td>"#,
        r#"<select id="draw-from-1" name="draw-from-1">"#,
        r#"<option value=""></option>"#,
        r#"<option value="m1" selected>m1</option>"#,
        r#"<option value="m2">m2</option>"#,
        r#"</select></td><td>is giving to</td>"#,
        r#"<td class="reveal"><details>"#,
        r#"<select id="draw-to-1" name="draw-to-1">"#,
        r#"<option value=""></option>"#,
        r#"<option value="m1">m1</option>"#,
        r#"<option value="m2" selected>m2</option>"#,
        r#"</select><summary>(click to reveal)</summary>"#,
        r#"</details></td></tr>"#,
    ));
    assert_that!(body).contains("draw-from-3");
    assert_that!(body).contains("draw-from-4");
    assert_that!(body).does_not_contain("draw-from-5");
}

#[test]
fn members_with_empty_usernames_are_not_selected_in_new_draw() {
    // Given a member with no username
    let (mut app, cookie) = logged_in_as("myuser");
    let create_resp = post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=M1&member-email-1=%20&\
        member-name-2=&member-email-2=m2&\
        member-name-2=&member-email-2=m3&\
        restriction-from-1=m1&restriction-to-1=m2&\
        draw-from-1=m2&draw-to-1=m3&\
        ",
    );
    assert_status(create_resp, 302);

    // The extra draw items don't select that member when they
    // are supposed to be blank.
    let resp = get(&mut app, &cookie, "/organisecircle/id1");
    let body = assert_status(resp, 200);
    assert_that!(body).contains(r#"option value="" selected><"#);
    assert_that!(body).does_not_contain(r#"option value="" selected>M1<"#);
}

#[test]
fn saving_a_bad_draw_shows_warnings() {
    // When I create a circle with a draw that breaks rules
    let (mut app, cookie) = logged_in_as("myuser");
    let create_resp = post_form(
        &mut app,
        &cookie, // Note: circle name is hard-coded in FakeApi to give warnings
        "/createcircle",
        "name=Circle%20with%20warnings&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=m1&\
        member-name-2=&member-email-2=m2&\
        member-name-3=Morris+3&member-email-3=m3&\
        restriction-from-1=m1&restriction-to-1=m2&\
        draw-from-1=m1&draw-to-1=m2&\
        draw-from-2=m1&draw-to-2=m2&\
        draw-from-3=m2&draw-to-3=m1&\
        draw-from-4=m3&draw-to-4=m2&\
        ",
    );
    assert_status(create_resp, 302);

    // We show warnings
    let resp = get(&mut app, &cookie, "/organisecircle/id1");
    let body = assert_status(resp, 200);
    assert_that!(body).contains(
        "Zz has no email address - they can't be included in the draw!",
    );
    assert_that!(body).contains("No-one is giving to Morris 3!");
    assert_that!(body).contains("Morris 3 is not giving to anyone!");
    assert_that!(body).contains("m1 is giving to 2 people");
    assert_that!(body).contains("2 people are giving to m2");
    assert_that!(body)
        .contains("m1 is giving to m2 but that breaks a restriction!");
}

#[test]
fn a_cancelled_draw_shows_no_warnings() {
    // When I create a circle with a draw that breaks rules
    let (mut app, cookie) = logged_in_as("myuser");
    let create_resp = post_form(
        &mut app,
        &cookie, // Note: circle name is hard-coded in FakeApi to give warnings
        "/createcircle",
        "name=Circle%20with%20warnings&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=m1&\
        member-name-2=&member-email-2=m2&\
        member-name-3=Morris+3&member-email-3=m3&\
        restriction-from-1=m1&restriction-to-1=m2&\
        draw-from-1=m1&draw-to-1=m2&\
        draw-from-2=m1&draw-to-2=m2&\
        draw-from-3=m2&draw-to-3=m1&\
        draw-from-4=m3&draw-to-4=m2&\
        ",
    );
    assert_status(create_resp, 302);

    // Update to still have warnings (same name) but no draw
    let create_resp = post_form(
        &mut app,
        &cookie,
        "/organisecircle/id1",
        "name=Circle%20with%20warnings&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=m1&\
        member-name-2=&member-email-2=m2&\
        member-name-3=Morris+3&member-email-3=m3&\
        restriction-from-1=m1&restriction-to-1=m2&\
        ",
    );
    assert_status(create_resp, 302);

    // We don't show warnings because the draw is empty
    let resp = get(&mut app, &cookie, "/organisecircle/id1");
    let body = assert_status(resp, 200);
    assert_that!(body).does_not_contain("No-one is giving to");
}

#[test]
fn requesting_to_cancel_draw_does_it() {
    // Given a circle with a draw
    let (mut app, cookie) = logged_in_as("myuser");
    let create_resp = post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=m1&\
        member-name-2=&member-email-2=m2&\
        member-name-3=&member-email-3=%20m3%20&\
        member-name-4=&member-email-4=m4&\
        restriction-from-1=m1&restriction-to-1=m2&\
        draw-from-1=m1&draw-to-1=m2&\
        draw-from-2=m2&draw-to-2=m3&\
        ",
    );
    assert_status(create_resp, 302);

    // When we request to cancel the draw
    let update_resp = post_form(
        &mut app,
        &cookie,
        "/organisecircle/id1",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=m1&\
        member-name-2=&member-email-2=m2&\
        member-name-3=&member-email-3=m3&\
        member-name-4=&member-email-4=m4&\
        restriction-from-1=m1&restriction-to-1=m2&\
        cancel_draw=Cancel+the+draw+now!&\
        draw-from-1=m1&draw-to-1=m2&\
        draw-from-2=m2&draw-to-2=m3&\
        ",
    );
    assert_status(update_resp, 302);

    // Then the draw is gone
    let resp = get(&mut app, &cookie, "/organisecircle/id1");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("My Circle");
    assert_that!(body).contains("The names have not yet been drawn");
    assert_that!(body).does_not_contain("is giving to");

    // And the restrictions remain
    assert_that!(body).contains(concat!(
        r#"restriction-from-1">"#,
        r#"<option value=""></option>"#,
        r#"<option value="m1" selected"#,
    ));
}

#[test]
fn creating_a_circle_enables_us_to_be_a_member() {
    // Given a circle
    let api = Box::new(FakeApi::new());
    let (mut app, cookie) = logged_in_with_api("myuser", api.clone());
    post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&\
        rules=The%20Rules!&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=%20a%20",
    );

    let (mut app_a, cookie_a) = logged_in_with_api("a", api);

    // When we ask to see the circle page
    let resp = get(&mut app_a, &cookie_a, "/circle/id1");

    // Then we see a page for it
    let body = assert_status(resp, 200);
    assert_that!(body).contains("My Circle");
    assert_that!(body).contains("The Rules!");
}

#[test]
fn members_can_see_their_recipients() {
    // Given a circle where a has 2 recipients in the draw
    let api = Box::new(FakeApi::new());
    let (mut app, cookie) = logged_in_with_api("myuser", api.clone());
    post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=a&\
        member-name-2=&member-email-2=b&\
        member-name-3=C&member-email-3=c&\
        draw-from-1=a&draw-to-1=b&\
        draw-from-2=a&draw-to-2=c",
    );

    let (mut app_a, cookie_a) = logged_in_with_api("a", api);

    // When we ask to see the circle page
    let resp = get(&mut app_a, &cookie_a, "/circle/id1");

    // Then we see our two recipients
    let body = assert_status(resp, 200);
    assert_that!(body).contains(r#"<p class="recipient">b</p>"#);
    assert_that!(body).contains(r#"<p class="recipient">C</p>"#);
}

#[test]
fn members_can_update_their_wishlists() {
    // Given a circle where I am a member
    let api = Box::new(FakeApi::new());
    let (mut app, cookie) = logged_in_with_api("myuser", api.clone());
    post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=myuser&\
        ",
    );

    // When I update my wishlist
    let resp = post_form(
        &mut app,
        &cookie,
        "/circle/id1",
        "\
    wishlist-text=My%20wishes&\
    wishlist-link-text-1=l1&wishlist-link-url-1=http://foo.com&\
    wishlist-link-text-2=l2&wishlist-link-url-2=http://fo2.com&\
    wishlist-link-text-3=l3&wishlist-link-url-3=http://fo3.com&\
    ",
    );
    assert_status(resp, 302);

    // Then we see the update
    let resp = get(&mut app, &cookie, "/circle/id1");
    let body = assert_status(resp, 200);
    assert_that!(body).contains(r#"name="wishlist-text" value="My wishes">"#);

    assert_that!(body).contains(r#"name="wishlist-link-text-1" value="l1">"#);
    assert_that!(body)
        .contains(r#"name="wishlist-link-url-1" value="http://foo.com""#);

    assert_that!(body).contains(r#"name="wishlist-link-text-2" value="l2">"#);
    assert_that!(body)
        .contains(r#"name="wishlist-link-url-2" value="http://fo2.com""#);

    assert_that!(body).contains(r#"name="wishlist-link-text-3" value="l3">"#);
    assert_that!(body)
        .contains(r#"name="wishlist-link-url-3" value="http://fo3.com""#);
}

#[test]
fn blank_wishlist_links_are_ignored() {
    // Given a circle where I am a member
    let api = Box::new(FakeApi::new());
    let (mut app, cookie) = logged_in_with_api("myuser", api.clone());
    post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=myuser&\
        ",
    );

    // When I update my wishlist with blank lines
    let resp = post_form(
        &mut app,
        &cookie,
        "/circle/id1",
        "\
    wishlist-text=My%20wishes&\
    wishlist-link-text-1=l1&wishlist-link-url-1=http://foo.com&\
    wishlist-link-text-2=&wishlist-link-url-2=&\
    wishlist-link-text-3=&wishlist-link-url-3=&\
    ",
    );
    assert_status(resp, 302);

    // Then the blank lines are ignored
    let resp = get(&mut app, &cookie, "/circle/id1");
    let body = assert_status(resp, 200);
    assert_that!(body).contains(r#"name="wishlist-text" value="My wishes">"#);

    assert_that!(body).contains(r#"name="wishlist-link-text-1" value="l1">"#);
    assert_that!(body)
        .contains(r#"name="wishlist-link-url-1" value="http://foo.com""#);

    // 2 blank lines are added, but not two additional ones
    assert_that!(body).does_not_contain("wishlist-link-text-4");
    assert_that!(body).does_not_contain("wishlist-link-url-4");
    assert_that!(body).does_not_contain("wishlist-link-text-5");
    assert_that!(body).does_not_contain("wishlist-link-url-5");
}

#[test]
fn wishlist_links_after_gap_are_saved() {
    // Given a circle where I am a member
    let api = Box::new(FakeApi::new());
    let (mut app, cookie) = logged_in_with_api("myuser", api.clone());
    post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=My%20Circle&\
        organiser-name-1=&organiser-email-1=myuser&\
        member-name-1=&member-email-1=myuser&\
        ",
    );

    // When I update my wishlist with blank lines
    let resp = post_form(
        &mut app,
        &cookie,
        "/circle/id1",
        "\
    wishlist-text=My%20wishes&\
    wishlist-link-text-1=l1&wishlist-link-url-1=http://foo.com&\
    wishlist-link-text-2=&wishlist-link-url-2=&\
    wishlist-link-text-3=&wishlist-link-url-3=&\
    wishlist-link-text-4=l4&wishlist-link-url-4=http://fo4.com&\
    ",
    );
    assert_status(resp, 302);

    // Then the blank lines are ignored
    let resp = get(&mut app, &cookie, "/circle/id1");
    let body = assert_status(resp, 200);
    assert_that!(body).contains(r#"name="wishlist-text" value="My wishes">"#);

    assert_that!(body).contains(r#"name="wishlist-link-text-1" value="l1">"#);
    assert_that!(body)
        .contains(r#"name="wishlist-link-url-1" value="http://foo.com""#);

    assert_that!(body).contains(r#"name="wishlist-link-text-2" value="l4">"#);
    assert_that!(body)
        .contains(r#"name="wishlist-link-url-2" value="http://fo4.com""#);

    // 2 blank lines are added, but no more
    assert_that!(body).does_not_contain("wishlist-link-text-5");
    assert_that!(body).does_not_contain("wishlist-link-url-5");
}

#[test]
fn can_delete_a_circle() {
    // Given 2 circles exist
    let (mut app, cookie) = logged_in_as("myuser");
    post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=C&organiser-name-1=&organiser-email-1=myuser",
    );
    post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=D&organiser-name-1=&organiser-email-1=myuser",
    );

    // Delete one
    let resp = post_form(
        &mut app,
        &cookie,
        "/deletecircle/id1",
        "delete_circle_sure=on",
    );

    // We get redirected to My Circles page
    assert_eq!(resp.status(), 302);
    assert_contains_header(
        &resp,
        "Location",
        "http://localhost:8080/circles?message=circle_deleted",
    );

    // It is gone
    let resp = get(&mut app, &cookie, "/circles?message=circle_deleted");
    let body = assert_status(resp, 200);
    assert_that!(body).contains(r#"organisecircle/id2">D</a>"#);
    assert_that!(body).does_not_contain(r#">C<"#);
}

#[test]
fn circle_is_not_deleted_if_not_sure_and_error_shown() {
    // Given 2 circles exist
    let (mut app, cookie) = logged_in_as("myuser");
    post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=C&organiser-name-1=&organiser-email-1=myuser",
    );
    post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=D&organiser-name-1=&organiser-email-1=myuser",
    );

    // Try to delete one, but don't tick "I am sure"
    let resp = post_form(&mut app, &cookie, "/deletecircle/id1", "");

    // We get redirected to organisecircle page
    assert_eq!(resp.status(), 302);
    assert_contains_header(
        &resp,
        "Location",
        "http://localhost:8080/organisecircle/id1?error=sure_required",
    );

    // The page we are redirected to shows an error
    let resp =
        get(&mut app, &cookie, "/organisecircle/id1?error=sure_required");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("You must tick the &quot;I am sure&quot; box");
}

#[test]
fn circle_is_not_deleted_if_no_cookie() {
    // Given 2 circles exist
    let (mut app, cookie) = logged_in_as("myuser");
    post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=C&organiser-name-1=&organiser-email-1=myuser",
    );
    post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=D&organiser-name-1=&organiser-email-1=myuser",
    );

    // Try to delete one with no cookie
    // Delete one
    let resp = post_form_no_cookie(
        &mut app,
        "/deletecircle/id1",
        "delete_circle_sure=on",
    );

    // We get redirected to login page
    assert_eq!(resp.status(), 302);
    assert_contains_header(&resp, "Location", "http://localhost:8080/login");

    // The circle is still there
    let resp = get(&mut app, &cookie, "/circle/id1");
    assert_eq!(resp.status(), 200);
    let body = assert_status(resp, 200);
    assert!(body.contains("id1"));
}

#[test]
fn error_shown_on_delete_if_circle_does_not_exist() {
    // Given 2 circles exist
    let (mut app, cookie) = logged_in_as("myuser");
    post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=C&organiser-name-1=&organiser-email-1=myuser",
    );
    post_form(
        &mut app,
        &cookie,
        "/createcircle",
        "name=D&organiser-name-1=&organiser-email-1=myuser",
    );

    // Try to delete one that does not exist
    let resp = post_form(
        &mut app,
        &cookie,
        "/deletecircle/INVALIDID",
        "delete_circle_sure=on",
    );

    // We get an error
    assert_eq!(resp.status(), 500);
}

#[test]
fn internal_error_shown_if_api_fails() {
    let (_app, cookie) = logged_in_as("myuser");

    let faulty_api = FaultyApi {};
    let mut faulty_app = new_app(Box::new(faulty_api));

    // Attempt something using the api that fails everything
    let resp = get(&mut faulty_app, &cookie, "/circles");

    // We get an error status and text
    let body = assert_status(resp, 500);
    assert_that!(body).contains("Internal error");
    assert_that!(body).contains("contact the server administrators");
}

#[test]
fn prefix_is_added_to_each_url_path() {
    // Given I have set prefix of "ui"
    let (mut app, cookie) = logged_in_with_prefix("myuser", String::from("ui"));

    // When I get a simple page
    let resp = get(&mut app, &cookie, "/circles");
    let body = assert_status(resp, 200);

    assert_that!(body).contains("http://localhost:8080/ui/circles");
    assert_that!(body).does_not_contain("http://localhost:8080/circles");
}

#[test]
fn signup_flow_1_signup_page_presents_a_form() {
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    let resp = get_nocookie(&mut app, "/signup");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("h1>Sign up</h1");
}

#[test]
fn signup_flow_2a_submitting_incorrect_user_details_displays_error() {
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    let resp = post_form_no_cookie(
        &mut app,
        "/register",
        "username=newuser&password=ps&password_repeat=WRONG",
    );
    let body = assert_status(resp, 200);
    assert_that!(body).contains("The passwords you typed do not match");
}

#[test]
fn signup_flow_2b_submitting_existing_user_details_displays_error() {
    let (mut app, _cookie) = logged_in_as("myuser");
    let resp = post_form_no_cookie(
        &mut app,
        "/register",
        "username=%20myuser%20&password=ps&password_repeat=ps",
    );
    let body = assert_status(resp, 200);
    assert_that!(body).contains("That email address is already claimed");
}

#[test]
fn signup_flow_2c_submitting_unregistered_user_details_displays_error() {
    // Given an unregistered user
    let (mut app, _cookie) = logged_in_as("myuser");
    post_form_no_cookie(
        &mut app,
        "/register",
        "username=unr&password=ps&password_repeat=ps",
    );

    // When I try to sign up again as them, we get a "user exists" page
    let resp = post_form_no_cookie(
        &mut app,
        "/register",
        "username=unr&password=ps&password_repeat=ps",
    );
    let body = assert_status(resp, 200);
    assert_that!(body).contains("That email address is already claimed");
}

#[test]
fn signup_flow_2_submitting_user_details_displays_registration_page() {
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    let resp = post_form_no_cookie(
        &mut app,
        "/register",
        "username=%20newuser%20&password=ps&password_repeat=ps",
    );
    let body = assert_status(resp, 200);
    assert_that!(body).contains("Enter registration code");
}

#[test]
fn signup_flow_3a_submitting_incorrect_registration_code_shows_error() {
    // Sign up
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    post_form_no_cookie(
        &mut app,
        "/register",
        "username=newuser&password=ps&password_repeat=ps",
    );

    // Try to register, with wrong code
    let resp = post_form_no_cookie(
        &mut app,
        "/afterregister",
        "username=newuser&registration_code=WRONG&real_registration_code=reg1",
    );
    let body = assert_status(resp, 200);
    assert_that!(body).contains("Registration failed.  Please try again");
}

#[test]
fn signup_flow_3b_logging_in_when_unregistered_asks_to_register() {
    // Sign up
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    post_form_no_cookie(
        &mut app,
        "/register",
        "username=newuser%20&password=ps&password_repeat=ps",
    );

    // Try to log in
    let resp =
        post_form_no_cookie(&mut app, "/", "username=newuser&password=ps");

    // Registration page is shown
    let body = assert_status(resp, 200);
    assert_that!(body).contains("You must register before you can log in");
}

#[test]
fn signup_flow_3_submitting_registration_code_shows_success() {
    // Sign up
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    post_form_no_cookie(
        &mut app,
        "/register",
        "username=%20newuser&password=ps&password_repeat=ps",
    );

    // Register with correct code
    let resp = post_form_no_cookie(
        &mut app,
        "/afterregister",
        "username=newuser%20&registration_code=reg1&real_registration_code=reg1",
    );
    let body = assert_status(resp, 200);
    assert_that!(body).contains("User created");
}

#[test]
fn contact_page_displays() {
    let api = Box::new(FakeApi::new());
    let mut app = new_app_with_contact_info(api, "Insert contacts here");
    let resp = get_nocookie(&mut app, "/contact");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("Insert contacts here");
}

#[test]
fn privacy_page_displays() {
    let api = Box::new(FakeApi::new_with_privacy("Insert privacy here"));
    let mut app = new_app(api);
    let resp = get_nocookie(&mut app, "/privacy");
    let body = assert_status(resp, 200);
    assert_that!(body).contains("Insert privacy here");
}

#[test]
fn can_change_password() {
    let (mut app, cookie) = logged_in_as("myuser");
    let resp = post_form(
        &mut app,
        &cookie,
        "/account",
        "password=pwn&password_repeat=pwn",
    );

    // We get a success response
    let body = assert_status(resp, 200);
    assert_that!(body).contains("Password changed.");

    // We can log in with the new password
    let resp =
        post_form_no_cookie(&mut app, "/", "username=myuser%20&password=pwn");
    assert_eq!(resp.status(), 302);
    assert_contains_header(&resp, "Location", "http://localhost:8080/circles");

    // And we can't with the old
    let resp =
        post_form_no_cookie(&mut app, "/", "username=myuser&password=pw");
    assert_eq!(resp.status(), 302);
    assert_contains_header(
        &resp,
        "Location",
        "http://localhost:8080/login?error=login_failed",
    );
}

#[test]
fn changing_password_with_non_matching_passwords_fails() {
    let (mut app, cookie) = logged_in_as("myuser");
    let resp = post_form(
        &mut app,
        &cookie,
        "/account",
        "password=pwn&password_repeat=AAA",
    );

    // We get a failure message
    let body = assert_status(resp, 400);
    assert_that!(body).contains("Passwords didn't match!");

    // We can't log in with the new password
    let resp =
        post_form_no_cookie(&mut app, "/", "username=myuser&password=pwn");
    assert_eq!(resp.status(), 302);
    assert_contains_header(
        &resp,
        "Location",
        "http://localhost:8080/login?error=login_failed",
    );

    // But we can with old
    let resp =
        post_form_no_cookie(&mut app, "/", "username=myuser&password=pw");
    assert_eq!(resp.status(), 302);
    assert_contains_header(&resp, "Location", "http://localhost:8080/circles");
}

#[test]
fn changing_password_with_empty_password_fails() {
    let (mut app, cookie) = logged_in_as("myuser");
    let resp =
        post_form(&mut app, &cookie, "/account", "password=&password_repeat=");

    // We get a failure message
    let body = assert_status(resp, 400);
    assert_that!(body).contains("Passwords can't be empty!");

    // We can't log in with the empty password
    let resp = post_form_no_cookie(&mut app, "/", "username=myuser&password=");
    assert_eq!(resp.status(), 302);
    assert_contains_header(
        &resp,
        "Location",
        "http://localhost:8080/login?error=login_failed",
    );

    // But we can with old
    let resp =
        post_form_no_cookie(&mut app, "/", "username=myuser&password=pw");
    assert_eq!(resp.status(), 302);
    assert_contains_header(&resp, "Location", "http://localhost:8080/circles");
}

#[test]
fn delete_user_flow_1a_not_sure_shows_message() {
    let (mut app, cookie) = logged_in_as("myuser");

    // Try to delete without ticking sure
    let resp = post_form(&mut app, &cookie, "/deleteaccount", "");

    // We get redirected
    assert_eq!(resp.status(), 302);
    assert_contains_header(
        &resp,
        "Location",
        "http://localhost:8080/account?error=sure_required",
    );

    // And the resulting page shows an error
    let resp = get(
        &mut app,
        &cookie,
        "http://localhost:8080/account?error=sure_required",
    );
    let body = assert_status(resp, 200);
    assert_that!(body).contains("You must choose &quot;I am sure&quot;.");
}

#[test]
fn delete_user_flow_1_sure_shows_delete_user_page() {
    let (mut app, cookie) = logged_in_as("myuser");

    // Ask to delete a user
    let resp = post_form(&mut app, &cookie, "/deleteaccount", "sure=on");

    // We get asked one more time
    let body = assert_status(resp, 200);
    assert_that!(body).contains("I am REALLY sure");
}

#[test]
fn delete_user_flow_2a_not_really_sure_shows_message() {
    let (mut app, cookie) = logged_in_as("myuser");

    // Try to delete without ticking really sure
    let resp =
        post_form(&mut app, &cookie, "/deleteaccount", "sure=on&really_sure=");

    // We are reminded just how sure we need to be
    let body = assert_status(resp, 200);
    assert_that!(body)
        .contains("Confirm by choosing &quot;I am REALLY sure&quot;.");
}

#[test]
fn delete_user_flow_2_user_is_gone_and_redirected_to_login() {
    let (mut app, cookie) = logged_in_as("myuser");

    // Ask to delete
    let resp = post_form(
        &mut app,
        &cookie,
        "/deleteaccount",
        "sure=on&really_sure=on",
    );

    // Get redirected to login page
    assert_eq!(resp.status(), 302);
    assert_contains_header(
        &resp,
        "Location",
        "http://localhost:8080/login?message=account_deleted",
    );

    // And the resulting page shows it was deleted
    let resp = get(
        &mut app,
        &cookie,
        "http://localhost:8080/login?message=account_deleted",
    );
    let body = assert_status(resp, 200);
    assert_that!(body).contains("Account deleted. Please log in.");
}

#[test]
fn can_reset_password() {
    let (mut app, cookie) = logged_in_as("myuser");

    // Request a password reset
    let resp =
        post_form(&mut app, &cookie, "/resetpassword", "username=a@b.com");

    let body = assert_status(resp, 200);
    assert_that!(body)
        .contains("Your password reset code has been sent to a@b.com");

    // Now actually reset it
    let resp = post_form(
        &mut app,
        &cookie,
        "/resetpassword",
        "username=a@b.com&password_reset_code=jjj&password=a&password_repeat=a",
    );

    // Then we are redirected to the login page
    assert_eq!(resp.status(), 302);
    assert_contains_header(
        &resp,
        "Location",
        "http://localhost:8080/login?message=password_reset",
    );

    // And the resulting page shows it was reset
    let resp = get(
        &mut app,
        &cookie,
        "http://localhost:8080/login?message=password_reset",
    );
    let body = assert_status(resp, 200);
    assert_that!(body)
        .contains("Password was successfully changed.  Please log in.");
}

#[test]
fn resetting_password_with_nonmatching_password_is_an_error() {
    let (mut app, cookie) = logged_in_as("myuser");

    // Request a password reset
    let resp =
        post_form(&mut app, &cookie, "/resetpassword", "username=a@b.com");

    let body = assert_status(resp, 200);
    assert_that!(body)
        .contains("Your password reset code has been sent to a@b.com");

    // Now actually reset it
    let resp = post_form(
        &mut app,
        &cookie,
        "/resetpassword",
        "username=a@b.com&password_reset_code=jjj&password=a&password_repeat=X",
    );

    // Then we receive an error message
    let body = assert_status(resp, 400);
    assert_that!(body).contains("The passwords you typed do not match");
}
