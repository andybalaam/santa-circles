use actix_http::body::Body;
use actix_http::{Request, Response};
use actix_web::dev::{Service, ServiceResponse};
use actix_web::http::Cookie;
use actix_web::web::{Bytes, BytesMut, HttpResponse};
use actix_web::{test, App, Error};
use futures::executor;
use futures::stream::StreamExt;

use santa_circles::api::Api;
use santa_circles::config;
use santa_circles::contact_page::ContactPage;
use santa_circles::cookie_session;
use santa_circles::data_holder::DataHolder;
use santa_circles::urls::UrlPrefix;

use crate::util::fake_api::FakeApi;

pub fn new_app(
    api: Box<dyn Api>,
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    new_app_all(api, None, None)
}

pub fn new_app_with_prefix(
    api: Box<dyn Api>,
    prefix: String,
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    new_app_all(api, Some(prefix), None)
}

pub fn new_app_with_contact_info(
    api: Box<dyn Api>,
    contact_info: &str,
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    new_app_all(api, None, Some(contact_info))
}

pub fn new_app_all(
    api: Box<dyn Api>,
    prefix: Option<String>,
    contact_info: Option<&str>,
) -> impl Service<Request = Request, Response = ServiceResponse, Error = Error>
{
    let contact_info = contact_info
        .unwrap_or(r#"<p>email <a href="mailto:me@example.com">me</a>.</p>"#);
    executor::block_on(test::init_service(
        App::new()
            .data(DataHolder::new(
                api,
                ContactPage::new(String::from(contact_info)),
                UrlPrefix::new(prefix),
            ))
            .wrap(cookie_session::new_session())
            .configure(config::config),
    ))
}

pub fn logged_in_as(
    username: &str,
) -> (
    impl Service<Request = Request, Response = ServiceResponse, Error = Error>,
    Cookie,
) {
    let mut api = Box::new(FakeApi::new());
    api.add_user(username, "pw");
    let mut app = new_app(api);
    let resp = post_form_no_cookie(
        &mut app,
        "/",
        &format!("username={}&password=pw", username),
    );
    (app, resp.cookies().next().unwrap().into_owned())
}

pub fn logged_in_with_api(
    username: &str,
    mut api: Box<FakeApi>,
) -> (
    impl Service<Request = Request, Response = ServiceResponse, Error = Error>,
    Cookie,
) {
    api.add_user(username, "pw");
    let mut app = new_app(api);
    let resp = post_form_no_cookie(
        &mut app,
        "/",
        &format!("username={}&password=pw", username),
    );
    (app, resp.cookies().next().unwrap().into_owned())
}

pub fn logged_in_with_prefix(
    username: &str,
    prefix: String,
) -> (
    impl Service<Request = Request, Response = ServiceResponse, Error = Error>,
    Cookie,
) {
    let mut api = Box::new(FakeApi::new());
    api.add_user(username, "pw");
    let mut app = new_app_with_prefix(api, prefix);
    let resp = post_form_no_cookie(
        &mut app,
        "/",
        &format!("username={}&password=pw", username),
    );
    (app, resp.cookies().next().unwrap().into_owned())
}

pub fn req<'a, S>(
    app: &'a mut S,
    req_type: test::TestRequest,
    path: &str,
) -> HttpResponse
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    let req = req_type.uri(path);

    executor::block_on(app.call(req.to_request()))
        .map(|sr| sr.into())
        .unwrap_or_else(|e| HttpResponse::from_error(e))
}

pub fn get<'a, S>(
    app: &'a mut S,
    auth_cookie: &Cookie,
    path: &str,
) -> HttpResponse
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    req(
        app,
        test::TestRequest::get().cookie(auth_cookie.clone()),
        path,
    )
}

pub fn post_form<'a, S>(
    app: &'a mut S,
    auth_cookie: &Cookie,
    path: &str,
    payload: &str,
) -> HttpResponse
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    req(
        app,
        test::TestRequest::post()
            .cookie(auth_cookie.clone())
            .header("Content-Type", "application/x-www-form-urlencoded")
            .set_payload(Bytes::from(String::from(payload))),
        path,
    )
}

pub fn get_nocookie<'a, S>(app: &'a mut S, path: &str) -> HttpResponse
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    req(app, test::TestRequest::get(), path)
}

pub fn post_form_no_cookie<'a, S>(
    app: &'a mut S,
    path: &str,
    payload: &str,
) -> HttpResponse
where
    S: Service<
        Request = Request,
        Response = ServiceResponse<Body>,
        Error = Error,
    >,
{
    req(
        app,
        test::TestRequest::post()
            .header("Content-Type", "application/x-www-form-urlencoded")
            .set_payload(Bytes::from(String::from(payload))),
        path,
    )
}

pub fn assert_contains_header(
    resp: &HttpResponse,
    header_name: &str,
    header_value: &str,
) {
    resp.headers()
        .get(header_name)
        .map(|v| assert_eq!(v, header_value))
        .unwrap_or_else(|| panic!("No header '{}' returned.", header_name))
}

pub fn assert_success(resp: HttpResponse) -> String {
    let status = resp.status();
    let body = fetch_body(resp);

    if status.is_success() {
        return body;
    } else {
        panic!(
            "Request failed with status {} and response {}.",
            status, body
        );
    }
}

pub fn assert_status(resp: HttpResponse, expected_status: u16) -> String {
    let status = resp.status();
    let body = fetch_body(resp);

    if status == expected_status {
        return body;
    } else {
        panic!(
            "Request status was {}, but we expected {}. Response: {}.",
            status, expected_status, body
        );
    }
}

async fn fetch_body_bytes(mut resp: Response<Body>) -> Bytes {
    let mut body = resp.take_body();
    let mut bytes = BytesMut::new();
    while let Some(item) = body.next().await {
        bytes.extend_from_slice(&item.unwrap());
    }
    bytes.freeze()
}

pub fn fetch_body(resp: Response<Body>) -> String {
    let bytes = executor::block_on(fetch_body_bytes(resp));
    String::from_utf8(bytes[..].to_vec()).unwrap()
}
