use async_trait::async_trait;

use santa_circles::api::{Api, ApiError};
use santa_circles::credentials::Credentials;

pub struct FaultyApi {}

#[async_trait]
impl Api for FaultyApi {
    async fn get(
        &self,
        _credentials: &Credentials,
        _path: &str,
    ) -> Result<serde_json::Value, ApiError> {
        Err(ApiError::InternalError(String::from("get error")))
    }

    async fn get_no_credentials(
        &self,
        _path: &str,
    ) -> Result<serde_json::Value, ApiError> {
        Err(ApiError::InternalError(String::from(
            "get_no_credentials error",
        )))
    }

    async fn put(
        &self,
        _credentials: &Credentials,
        _path: &str,
        _body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        Err(ApiError::InternalError(String::from("put error")))
    }

    async fn put_no_credentials(
        &self,
        _path: &str,
        _body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        Err(ApiError::InternalError(String::from(
            "put_no_credentials error",
        )))
    }

    async fn post(
        &self,
        _credentials: &Credentials,
        _path: &str,
        _body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        Err(ApiError::InternalError(String::from("post error")))
    }

    async fn post_no_credentials(
        &self,
        _path: &str,
        _body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        Err(ApiError::InternalError(String::from(
            "post_no_credentials error",
        )))
    }

    async fn delete(
        &self,
        _credentials: &Credentials,
        _path: &str,
        _body: Option<serde_json::Value>,
    ) -> Result<serde_json::Value, ApiError> {
        Err(ApiError::InternalError(String::from("delete error")))
    }
}
