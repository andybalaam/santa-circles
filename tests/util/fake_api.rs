use async_trait::async_trait;
use regex::Regex;
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};

use santa_circles::api::{Api, ApiError};
use santa_circles::credentials::Credentials;

#[derive(Clone, Deserialize, Serialize)]
pub struct CircleRef {
    pub circleid: String,
    pub name: String,
}

impl From<&Circle> for CircleRef {
    fn from(circle: &Circle) -> Self {
        Self {
            circleid: circle.circleid.clone(),
            name: circle.name.clone(),
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Circle {
    circleid: String,
    name: String,
    draw_date: String,
    rules: String,
    members: Vec<Member>,
    organisers: Vec<UsernameNick>,
    draw: Vec<DrawItem>,
    disallowed_draw: Vec<DrawItem>,
    draw_warnings: Vec<DrawWarning>,
}

impl Circle {
    fn new(circleid: String, new_circle: NewCircle) -> Self {
        Self {
            circleid,
            name: new_circle.name,
            draw_date: new_circle.draw_date,
            rules: new_circle.rules,
            members: fill_wishlist(new_circle.members),
            organisers: new_circle.organisers,
            draw: new_circle.draw,
            disallowed_draw: new_circle.disallowed_draw,
            draw_warnings: Vec::new(),
        }
    }
}

fn fill_wishlist(members: Vec<NewMember>) -> Vec<Member> {
    members
        .into_iter()
        .map(|mem| Member {
            username: mem.username,
            nickname: mem.nickname,
            wishlist_text: mem.wishlist_text.unwrap_or(String::from("")),
            wishlist_links: mem.wishlist_links.unwrap_or(Vec::new()),
        })
        .collect()
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct MemberCircle {
    circleid: String,
    name: String,
    draw_date: String,
    rules: String,
    members: Vec<Member>,
    organisers: Vec<UsernameNick>,
    recipients: Option<Vec<String>>,
}

#[derive(Deserialize, Serialize)]
pub struct Circles {
    pub member_circles: Vec<CircleRef>,
    pub organiser_circles: Vec<CircleRef>,
}

#[derive(Deserialize)]
struct RegistrationCode {
    registration_code: String,
}

#[derive(Deserialize)]
struct NewUser {
    username: String,
    password: String,
}

#[derive(Clone)]
struct User {
    username: String,
    password: String,
    remember_me: Option<bool>,
    deleted_tokens: Vec<String>,
    registered: bool,
    member_circleids: Vec<String>,
    organiser_circleids: Vec<String>,
}

impl User {
    fn change_password(
        &mut self,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        body.get("password")
            .ok_or(ApiError::InternalError(String::from(
                "No password supplied",
            )))
            .and_then(|j| {
                j.as_str().ok_or(ApiError::InternalError(String::from(
                    "No password supplied",
                )))
            })
            .map(|pw| {
                self.password = String::from(pw);
                json!({"notused?":"notused"})
            })
    }

    fn info(&self) -> serde_json::Value {
        serde_json::Value::String(String::from("some user info"))
    }

    fn delete_token(
        &mut self,
        token_owner_username: &str,
        token: &str,
    ) -> Result<serde_json::Value, ApiError> {
        if token_owner_username != self.username
            || token != format!("{}-token", self.username)
        {
            Err(ApiError::InternalError(String::from("Wrong token format")))
        } else {
            self.deleted_tokens.push(String::from(token));
            Ok(json!({}))
        }
    }
}

#[derive(Debug)]
enum PathType {
    CircleOrganiser(String),
    Circle(String),
    CircleMemberWishlist(String, String),
    Circles,
    User,
    Login,
    LoginUser(String),
}

impl PathType {
    fn parse(username: &str, path: &str) -> Option<PathType> {
        let circle_re = Regex::new(r"^/v1/circle/([^/?]+).*$").unwrap();
        let circle_org_re =
            Regex::new(r"^/v1/circle/([^/?]+)\?view=organise$").unwrap();
        let circle_mem_re =
            Regex::new(r"^/v1/circle/([^/?]+)/members/([^/?]+)/wishlist.*$")
                .unwrap();
        let login_re = Regex::new(r"^/v1/login/([^/?]+).*$").unwrap();

        if path == "/v1/login" {
            Some(PathType::Login)
        } else if let Some(captures) = login_re.captures(path) {
            Some(PathType::LoginUser(String::from(
                captures.get(1).unwrap().as_str(),
            )))
        } else if path == "/v1/circle" {
            Some(PathType::Circles)
        } else if let Some(captures) = circle_org_re.captures(path) {
            Some(PathType::CircleOrganiser(String::from(
                captures.get(1).unwrap().as_str(),
            )))
        } else if let Some(captures) = circle_mem_re.captures(path) {
            Some(PathType::CircleMemberWishlist(
                String::from(captures.get(1).unwrap().as_str()),
                String::from(captures.get(2).unwrap().as_str()),
            ))
        } else if let Some(captures) = circle_re.captures(path) {
            Some(PathType::Circle(String::from(
                captures.get(1).unwrap().as_str(),
            )))
        } else if path == format!("/v1/user/{}", username) {
            Some(PathType::User)
        } else {
            None
        }
    }
}

enum NoCredPathType {
    NewUser,
    Privacy,
    UpdateUser(String),
    Reset(String),
    DoReset(String),
}

impl NoCredPathType {
    fn parse(path: &str) -> Option<NoCredPathType> {
        let user_re = Regex::new(r"^/v1/newuser/([^/]*)$").unwrap();
        let reset_re = Regex::new(r"^/v1/reset/([^/?]+).*$").unwrap();
        let do_reset_re = Regex::new(r"^/v1/do_reset/([^/?]+).*$").unwrap();

        if path == "/v1/newuser" {
            Some(NoCredPathType::NewUser)
        } else if let Some(captures) = user_re.captures(path) {
            Some(NoCredPathType::UpdateUser(String::from(
                captures.get(1).unwrap().as_str(),
            )))
        } else if path == "/v1/privacy" {
            Some(NoCredPathType::Privacy)
        } else if let Some(captures) = reset_re.captures(path) {
            Some(NoCredPathType::Reset(String::from(
                captures.get(1).unwrap().as_str(),
            )))
        } else if let Some(captures) = do_reset_re.captures(path) {
            Some(NoCredPathType::DoReset(String::from(
                captures.get(1).unwrap().as_str(),
            )))
        } else {
            None
        }
    }
}

#[derive(Clone)]
pub struct FakeApi {
    users: Arc<Mutex<HashMap<String, User>>>,
    circles: Arc<Mutex<HashMap<String, Circle>>>,
    nid: Arc<Mutex<u32>>,
    privacy: String,
}

impl FakeApi {
    pub fn new() -> FakeApi {
        FakeApi {
            users: Arc::new(Mutex::new(HashMap::new())),
            circles: Arc::new(Mutex::new(HashMap::new())),
            nid: Arc::new(Mutex::new(0)),
            privacy: String::from("No privacy info provided"),
        }
    }

    pub fn new_with_privacy(privacy: &str) -> FakeApi {
        FakeApi {
            users: Arc::new(Mutex::new(HashMap::new())),
            circles: Arc::new(Mutex::new(HashMap::new())),
            nid: Arc::new(Mutex::new(0)),
            privacy: String::from(privacy),
        }
    }

    fn next_id(&self) -> String {
        let mut nid = self.nid.lock().unwrap();
        *nid += 1;
        format!("id{}", *nid)
    }

    pub fn add_user(&mut self, username: &str, password: &str) {
        self.users.lock().unwrap().insert(
            String::from(username),
            User {
                username: String::from(username),
                password: String::from(password),
                remember_me: None,
                deleted_tokens: vec![],
                registered: true,
                member_circleids: vec![],
                organiser_circleids: vec![],
            },
        );
    }

    fn delete_user(
        &self,
        username: &str,
    ) -> Result<serde_json::Value, ApiError> {
        self.users
            .lock()
            .unwrap()
            .remove(username)
            .map(|_| json!({"unused":"unused?"}))
            .ok_or(ApiError::InternalError(String::from(
                "Unable to delete user",
            )))
    }

    pub fn get_user_reg_code(
        &self,
        _username: &str,
    ) -> Result<serde_json::Value, ApiError> {
        Ok(json!({"registration_code":"reg1"}))
    }

    pub fn register_user(
        &self,
        username: &str,
        details: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        // TODO: track whether a user is registered and return an error
        //       here if so?
        let registration_code: RegistrationCode =
            serde_json::from_value(details)
                .expect("Unable to parse RegistrationCode");
        self.users
            .lock()
            .unwrap()
            .get_mut(username)
            .ok_or(ApiError::InternalError(String::from("Unable to find user")))
            .and_then(|mut user| {
                if registration_code.registration_code == "reg1" {
                    user.registered = true;
                    Ok(json!({}))
                } else {
                    Err(ApiError::InternalError(String::from(
                        "Wrong registration code",
                    )))
                }
            })
    }

    pub fn add_unregistered_user(
        &self,
        details: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        let user_details: NewUser =
            serde_json::from_value(details).expect("Unable to parse NewUser");
        let mut users = self.users.lock().unwrap();

        if users.contains_key(&user_details.username) {
            return Err(ApiError::UserAlreadyExists);
        }

        users.insert(
            String::from(&user_details.username),
            User {
                username: String::from(&user_details.username),
                password: String::from(&user_details.password),
                remember_me: None,
                deleted_tokens: vec![],
                registered: false,
                member_circleids: vec![],
                organiser_circleids: vec![],
            },
        );

        Ok(serde_json::to_value(
            vec![
                ("username", user_details.username.as_str()),
                ("registration_code", "reg1"),
            ]
            .iter()
            .cloned()
            .collect::<HashMap<&str, &str>>(),
        )
        .expect("Failed to serialize unregistered user"))
    }

    pub fn get_remember_me(&self, username: &str) -> Option<bool> {
        self.users
            .lock()
            .unwrap()
            .get(username)
            .expect("User not found.")
            .remember_me
    }

    pub fn get_deleted_tokens(&self, username: &str) -> Vec<String> {
        self.users
            .lock()
            .unwrap()
            .get(username)
            .expect("User not found.")
            .deleted_tokens
            .clone()
    }

    fn check_credentials<F>(
        &self,
        credentials: &Credentials,
        f: F,
    ) -> Result<serde_json::Value, ApiError>
    where
        F: FnOnce(&mut User) -> Result<serde_json::Value, ApiError>,
    {
        // Same as check_credentials_user, but we don't need to clone a User
        self.users
            .lock()
            .unwrap()
            .get_mut(&credentials.username)
            .ok_or(ApiError::AuthenticationFailed)
            .and_then(|user| {
                if credentials.token
                    == format!("{}-token", credentials.username)
                    || credentials.token == user.password
                {
                    Ok(user)
                } else {
                    Err(ApiError::AuthenticationFailed)
                }
            })
            .and_then(f)
    }

    fn check_credentials_user(
        &self,
        credentials: &Credentials,
    ) -> Result<User, ApiError> {
        self.users
            .lock()
            .unwrap()
            .get_mut(&credentials.username)
            .ok_or(ApiError::AuthenticationFailed)
            .and_then(|user| {
                if credentials.token
                    == format!("{}-token", credentials.username)
                    || credentials.token == user.password
                {
                    Ok(user)
                } else {
                    Err(ApiError::AuthenticationFailed)
                }
            })
            .map(|u| u.clone())
    }

    fn circle_refs(&self, circleids: &Vec<String>) -> Vec<CircleRef> {
        circleids
            .iter()
            .filter_map(|circleid| {
                self.circles
                    .lock()
                    .unwrap()
                    .get(circleid)
                    .map(|circle| circle.into())
            })
            .collect()
    }

    fn get_circle(&self, circleid: &str) -> Circle {
        let mut ret = self
            .circles
            .lock()
            .unwrap()
            .get(circleid)
            .cloned()
            .expect(&format!("Circle with id {} should exist!", circleid));

        if ret.name == "Circle with warnings" {
            ret.draw_warnings = vec![
                DrawWarning::NoUsername(String::from("Zz")),
                DrawWarning::NotReceiving(String::from("m3")),
                DrawWarning::NotGiving(String::from("m3")),
                DrawWarning::GivingMultiple(
                    String::from("m1"),
                    vec![String::from("m2"), String::from("m2")],
                ),
                DrawWarning::ReceivingMultiple(
                    String::from("m2"),
                    vec![String::from("m1"), String::from("m3")],
                ),
                DrawWarning::BreaksRestriction(DrawItem {
                    from_username: String::from("m1"),
                    to_username: String::from("m2"),
                }),
            ];
        }

        ret
    }

    fn delete_circle(
        &self,
        circleid: String,
    ) -> Result<serde_json::Value, ApiError> {
        let circle = {
            let mut circles = self.circles.lock().unwrap();
            circles.remove(&circleid)
        };

        if let Some(circle) = circle {
            let mut users = self.users.lock().unwrap();
            for member in circle.members {
                let user = users.get_mut(&member.username);
                if let Some(user) = user {
                    user.member_circleids.retain(|id| id != &circleid);
                }
            }
            for organiser in circle.organisers {
                let user = users.get_mut(&organiser.username);
                if let Some(user) = user {
                    user.organiser_circleids.retain(|id| id != &circleid);
                }
            }
            Ok(json! { { "circleid": circleid } })
        } else {
            Err(ApiError::InternalError(String::from(
                "Circle does not exist.",
            )))
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct UsernameNick {
    username: String,
    nickname: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct WishlistLink {
    text: String,
    url: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct Member {
    username: String,
    nickname: String,
    wishlist_text: String,
    wishlist_links: Vec<WishlistLink>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct DrawItem {
    from_username: String,
    to_username: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
enum DrawWarning {
    BreaksRestriction(DrawItem),
    EmptyFrom(String),
    EmptyTo(String),
    GivingMultiple(String, Vec<String>),
    NoUsername(String),
    NotGiving(String),
    NotReceiving(String),
    ReceivingMultiple(String, Vec<String>),
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct NewMember {
    username: String,
    nickname: String,
    wishlist_text: Option<String>,
    wishlist_links: Option<Vec<WishlistLink>>,
}

#[derive(Deserialize)]
struct NewCircle {
    name: String,
    draw_date: String,
    rules: String,
    members: Vec<NewMember>,
    organisers: Vec<UsernameNick>,
    request_draw: Option<bool>,
    draw: Vec<DrawItem>,
    disallowed_draw: Vec<DrawItem>,
}

#[derive(Serialize)]
struct NewCircleResult {
    circleid: String,
}

#[derive(Debug, Deserialize)]
struct NewWishlist {
    wishlist_text: String,
    wishlist_links: Vec<WishlistLink>,
}

#[async_trait]
impl Api for FakeApi {
    async fn get(
        &self,
        credentials: &Credentials,
        path: &str,
    ) -> Result<serde_json::Value, ApiError> {
        self.check_credentials(credentials, |user| {
            match PathType::parse(&user.username, path) {
                Some(PathType::Circles) => Ok(serde_json::to_value(Circles {
                    member_circles: self.circle_refs(&user.member_circleids),
                    organiser_circles: self
                        .circle_refs(&user.organiser_circleids),
                })
                .unwrap()),
                Some(PathType::CircleOrganiser(circleid)) => {
                    Ok(serde_json::to_value(self.get_circle(&circleid))
                        .unwrap())
                }
                Some(PathType::CircleMemberWishlist(_, _)) => Err(
                    ApiError::InternalError(String::from("GET for wishlist")),
                ),
                Some(PathType::Circle(circleid)) => {
                    Ok(serde_json::to_value(member_circle(
                        self.get_circle(&circleid),
                        &credentials.username,
                    ))
                    .unwrap())
                }
                Some(PathType::Login) => {
                    Err(ApiError::InternalError(String::from("GET from login")))
                }
                Some(PathType::LoginUser(_)) => Err(ApiError::InternalError(
                    String::from("GET from login user"),
                )),
                Some(PathType::User) => Ok(user.info()),
                None => Err(ApiError::InternalError(String::from(
                    "Unrecognised path type",
                ))),
            }
        })
    }

    async fn get_no_credentials(
        &self,
        path: &str,
    ) -> Result<serde_json::Value, ApiError> {
        match NoCredPathType::parse(path) {
            Some(NoCredPathType::NewUser) => {
                Err(ApiError::InternalError(String::from("GET from newuser")))
            }
            Some(NoCredPathType::Privacy) => Ok(json!(self.privacy)),
            Some(NoCredPathType::UpdateUser(username)) => {
                self.get_user_reg_code(&username)
            }
            Some(NoCredPathType::Reset(_)) => {
                Err(ApiError::InternalError(String::from("GET to reset")))
            }
            Some(NoCredPathType::DoReset(_)) => {
                Err(ApiError::InternalError(String::from("GET to do_reset")))
            }
            None => Err(ApiError::InternalError(String::from(
                "Unrecognised path type",
            ))),
        }
    }

    async fn put(
        &self,
        credentials: &Credentials,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        self.check_credentials(credentials, |user| {
            match PathType::parse(&user.username, path) {
                Some(PathType::Circles) => {
                    Err(ApiError::InternalError(String::from("PUT to circles")))
                }
                Some(PathType::CircleOrganiser(_circleid)) => {
                    Err(ApiError::InternalError(String::from(
                        "PUT to circle?view=organise",
                    )))
                }
                Some(PathType::CircleMemberWishlist(circleid, username)) => {
                    let new_wishlist = NewWishlist::deserialize(body).unwrap();
                    let mut circles_lock = self.circles.lock().unwrap();
                    if let Some(circle) = circles_lock.get_mut(&circleid) {
                        circle
                            .members
                            .iter_mut()
                            .find(|m| m.username == username)
                            .map(|m| {
                                m.wishlist_text = new_wishlist.wishlist_text;
                                m.wishlist_links = new_wishlist.wishlist_links;
                            });
                        Ok(json!({
                            "circleid": circleid,
                            "username": username
                        }))
                    } else {
                        Err(ApiError::InternalError(String::from(
                            "No circle found!",
                        )))
                    }
                }
                Some(PathType::Circle(circleid)) => {
                    let new_circle = NewCircle::deserialize(body).unwrap();
                    if !new_circle
                        .organisers
                        .iter()
                        .any(|pr| pr.username == user.username)
                    {
                        return Err(ApiError::CircleCreatorMustBeAnOrganiser);
                    }

                    let mut circles_lock = self.circles.lock().unwrap();
                    let circle = circles_lock.get_mut(&circleid);
                    if let Some(circle) = circle {
                        let added_orgs = set_diff_un(
                            &new_circle.organisers,
                            &circle.organisers,
                        );
                        let removed_orgs = set_diff_un(
                            &circle.organisers,
                            &new_circle.organisers,
                        );
                        let added_members =
                            set_diff(&new_circle.members, &circle.members);
                        let removed_members =
                            set_diff2(&circle.members, &new_circle.members);

                        for organiser in added_orgs {
                            self.users.lock().unwrap().get_mut(organiser).map(
                                |user| {
                                    user.organiser_circleids
                                        .push(circleid.clone())
                                },
                            );
                        }

                        for organiser in removed_orgs {
                            self.users.lock().unwrap().get_mut(organiser).map(
                                |user| {
                                    user.organiser_circleids
                                        .retain(|cid| *cid != circleid)
                                },
                            );
                        }

                        for member in added_members {
                            self.users.lock().unwrap().get_mut(member).map(
                                |user| {
                                    user.member_circleids.push(circleid.clone())
                                },
                            );
                        }

                        for member in removed_members {
                            self.users.lock().unwrap().get_mut(member).map(
                                |user| {
                                    user.member_circleids
                                        .retain(|cid| *cid != circleid)
                                },
                            );
                        }

                        circle.name = new_circle.name;
                        circle.members = fill_wishlist(new_circle.members);
                        circle.organisers = new_circle.organisers;
                        circle.disallowed_draw = new_circle.disallowed_draw;

                        let request_draw =
                            new_circle.request_draw.unwrap_or(false);

                        circle.draw = if request_draw {
                            dumb_draw(&circle.members)
                        } else {
                            new_circle.draw
                        };

                        Ok(serde_json::to_value(NewCircleResult { circleid })
                            .unwrap())
                    } else {
                        Err(ApiError::CircleDoesNotExist)
                    }
                }
                Some(PathType::Login) => {
                    Err(ApiError::InternalError(String::from("PUT to login")))
                }
                Some(PathType::LoginUser(_)) => Err(ApiError::InternalError(
                    String::from("PUT to login user"),
                )),
                Some(PathType::User) => user.change_password(body),
                None => Err(ApiError::InternalError(String::from(
                    "Unrecognised path type",
                ))),
            }
        })
    }

    async fn put_no_credentials(
        &self,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        match NoCredPathType::parse(path) {
            Some(NoCredPathType::NewUser) => Err(ApiError::InternalError(
                String::from("PUT to newuser without credentials"),
            )),
            Some(NoCredPathType::Privacy) => Err(ApiError::InternalError(
                String::from("PUT to privacy without credentials"),
            )),
            Some(NoCredPathType::UpdateUser(username)) => {
                self.register_user(&username, body)
            }
            Some(NoCredPathType::Reset(_)) => {
                Err(ApiError::InternalError(String::from("PUT to reset")))
            }
            Some(NoCredPathType::DoReset(_)) => {
                Err(ApiError::InternalError(String::from("PUT to do_reset")))
            }
            None => Err(ApiError::InternalError(String::from(
                "Unrecognised path type",
            ))),
        }
    }

    async fn post(
        &self,
        credentials: &Credentials,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        let user = self.check_credentials_user(credentials)?;
        match PathType::parse(&user.username, path) {
            Some(PathType::Circles) => {
                let circleid = self.next_id();
                let circle = Circle::new(
                    circleid.clone(),
                    NewCircle::deserialize(body).unwrap(),
                );

                if !circle
                    .organisers
                    .iter()
                    .any(|pr| pr.username == user.username)
                {
                    return Err(ApiError::CircleCreatorMustBeAnOrganiser);
                }

                for organiser in &circle.organisers {
                    self.users
                        .lock()
                        .unwrap()
                        .get_mut(&organiser.username)
                        .map(|user| {
                            user.organiser_circleids.push(circleid.clone())
                        });
                }

                for member in &circle.members {
                    self.users.lock().unwrap().get_mut(&member.username).map(
                        |user| user.member_circleids.push(circleid.clone()),
                    );
                }

                self.circles
                    .lock()
                    .unwrap()
                    .insert(circleid.clone(), circle);

                Ok(serde_json::to_value(NewCircleResult { circleid }).unwrap())
            }
            Some(PathType::Circle(_)) => {
                Err(ApiError::InternalError(String::from("POST to circle")))
            }
            Some(PathType::CircleOrganiser(_)) => Err(ApiError::InternalError(
                String::from("POST to circle?view=organise"),
            )),
            Some(PathType::CircleMemberWishlist(_, _)) => {
                Err(ApiError::InternalError(String::from(
                    "POST to circle/id/members/username/wishlist",
                )))
            }
            Some(PathType::Login) => {
                if user.registered {
                    self.users.lock().unwrap().get_mut(&user.username).map(
                        |user| {
                            user.remember_me = Some(
                                body.get("remember_me")
                                    .unwrap()
                                    .as_bool()
                                    .unwrap(),
                            )
                        },
                    );
                    Ok(json!(
                        {
                            "username": String::from(&user.username),
                            "token": format!("{}-token", &user.username),
                        }
                    ))
                } else {
                    Err(ApiError::Forbidden)
                }
            }
            Some(PathType::LoginUser(_)) => {
                Err(ApiError::InternalError(String::from("POST to loginuser")))
            }
            Some(PathType::User) => {
                Err(ApiError::InternalError(String::from("POST to user")))
            }
            None => Err(ApiError::InternalError(String::from(
                "POST to unrecognised path type",
            ))),
        }
    }

    async fn post_no_credentials(
        &self,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        match NoCredPathType::parse(path) {
            Some(NoCredPathType::NewUser) => self.add_unregistered_user(body),
            Some(NoCredPathType::Privacy) => Err(ApiError::InternalError(
                String::from("POST to privacy no credentials"),
            )),
            Some(NoCredPathType::UpdateUser(_)) => {
                Err(ApiError::InternalError(String::from(
                    "POST to updateuser no credentials",
                )))
            }
            Some(NoCredPathType::Reset(username)) => {
                Ok(json!({ "username": username }))
            }
            Some(NoCredPathType::DoReset(username)) => {
                Ok(json!({ "username": username }))
            }
            None => Err(ApiError::InternalError(String::from(
                "POST to unrecognised path no credentials",
            ))),
        }
    }

    async fn delete(
        &self,
        credentials: &Credentials,
        path: &str,
        body: Option<serde_json::Value>,
    ) -> Result<serde_json::Value, ApiError> {
        let mut delete_user = false;
        let mut delete_circle = None;
        let ret = self.check_credentials(credentials, |user| {
            match PathType::parse(&user.username, path) {
                Some(PathType::Circles) => Err(ApiError::InternalError(
                    String::from("DELETE to circles"),
                )),
                Some(PathType::Circle(circleid)) => {
                    delete_circle = Some(circleid);
                    Err(ApiError::InternalError(String::from("dummy")))
                }
                Some(PathType::CircleOrganiser(_)) => {
                    Err(ApiError::InternalError(String::from(
                        "DELETE to circle?view=organise",
                    )))
                }
                Some(PathType::CircleMemberWishlist(_, _)) => {
                    Err(ApiError::InternalError(String::from(
                        "DELETE to circle/id/members/username/wishlist",
                    )))
                }
                Some(PathType::Login) => Err(ApiError::InternalError(
                    String::from("DELETE to login"),
                )),
                Some(PathType::LoginUser(username)) => user.delete_token(
                    &username,
                    &body
                        .expect(
                            "No request body supplied when deleting a token.",
                        )
                        .get("token")
                        .expect(
                            "No 'token' property in JSON body of delete \
                            request.",
                        )
                        .as_str()
                        .expect("'token' property was not a string."),
                ),
                Some(PathType::User) => {
                    delete_user = true;
                    Err(ApiError::InternalError(String::from("dummy")))
                }
                None => Err(ApiError::InternalError(String::from(
                    "DELETE to unrecognised path type",
                ))),
            }
        });
        if delete_user {
            // check_credentials has a lock on users, so we must do
            // this outside.
            self.delete_user(&credentials.username)
        } else if let Some(circleid) = delete_circle {
            self.delete_circle(circleid)
        } else {
            ret
        }
    }
}

fn member_circle(circle: Circle, username: &str) -> MemberCircle {
    let recipients = find_recipients(&circle, username);
    MemberCircle {
        circleid: circle.circleid,
        name: circle.name,
        rules: circle.rules,
        draw_date: circle.draw_date,
        organisers: circle.organisers,
        members: circle.members,
        recipients,
    }
}

fn find_recipients(circle: &Circle, username: &str) -> Option<Vec<String>> {
    if circle.draw.is_empty() {
        None
    } else {
        Some(
            circle
                .draw
                .iter()
                .filter_map(|item| {
                    if item.from_username == username {
                        Some(item.to_username.clone())
                    } else {
                        None
                    }
                })
                .collect(),
        )
    }
}

fn dumb_draw(members: &Vec<Member>) -> Vec<DrawItem> {
    let mut i = 1;
    members
        .iter()
        .map(|m| {
            let ret = DrawItem {
                from_username: m.username.clone(),
                to_username: members[i].username.clone(),
            };
            i += 1;
            if i >= members.len() {
                i = 0;
            }
            ret
        })
        .collect()
}

fn set_diff_un<'a>(
    left: &'a Vec<UsernameNick>,
    right: &'a Vec<UsernameNick>,
) -> Vec<&'a str> {
    left.iter()
        .filter_map(|l| {
            if right.iter().any(|r| r.username == l.username) {
                None
            } else {
                Some(l.username.as_str())
            }
        })
        .collect()
}

fn set_diff<'a>(
    left: &'a Vec<NewMember>,
    right: &'a Vec<Member>,
) -> Vec<&'a str> {
    left.iter()
        .filter_map(|l| {
            if right.iter().any(|r| r.username == l.username) {
                None
            } else {
                Some(l.username.as_str())
            }
        })
        .collect()
}

fn set_diff2<'a>(
    left: &'a Vec<Member>,
    right: &'a Vec<NewMember>,
) -> Vec<&'a str> {
    left.iter()
        .filter_map(|l| {
            if right.iter().any(|r| r.username == l.username) {
                None
            } else {
                Some(l.username.as_str())
            }
        })
        .collect()
}
