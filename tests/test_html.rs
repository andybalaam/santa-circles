extern crate santa_circles;

use maud::{html, Markup};
use quick_xml::events::{BytesStart, Event};
use quick_xml::{Reader, Writer};
use santa_circles::draw_warning::DrawWarning;
use santa_circles::recipient::Recipient;
use similar_asserts::assert_str_eq;
use std::collections::HashMap;
use std::io::Cursor;

use santa_circles::draw_item::DrawItem;
use santa_circles::pages::{
    account, afterregister, circle, circles, contact, createcircle,
    deleteaccount, forgotpassword, login, organisecircle, privacy, register,
    resetpassword, signup,
};
use santa_circles::string_or_bool::StringOrBool;
use santa_circles::usernamenick::{UsernameNick, UsernameNickHasWishlist};
use santa_circles::wishlist_link::WishlistLink;

#[test]
fn login() {
    assert_matches_design(
        include_str!("../design/login.html"),
        login::render(&urls(), None, None),
    );
}

#[test]
fn login_failure() {
    assert_matches_design(
        include_str!("../design/login_failure.html"),
        login::render(
            &urls(),
            Some(html! { "Login failed or timed out - please try again" }),
            None,
        ),
    );
}

#[test]
fn login_loggedout() {
    assert_matches_design(
        include_str!("../design/login_loggedout.html"),
        login::render(&urls(), None, Some(html! { "Logged out." })),
    );
}

#[test]
fn signup() {
    assert_matches_design(
        include_str!("../design/signup.html"),
        signup::render(&urls(), signup::Message::NoMessage),
    );
}

#[test]
fn register_passwords_do_not_match() {
    assert_matches_design(
        include_str!("../design/register_passwords_do_not_match.html"),
        signup::render(&urls(), signup::Message::PasswordsDoNotMatch),
    );
}

#[test]
fn register_username_taken() {
    assert_matches_design(
        include_str!("../design/register_username_taken.html"),
        signup::render(&urls(), signup::Message::UsernameTaken),
    );
}

#[test]
fn register() {
    assert_matches_design(
        include_str!("../design/register.html"),
        register::render(
            "name@example.com",
            &StringOrBool::S(String::from("12345678")),
            &urls(),
            register::Message::NoMessage,
        ),
    );
}

#[test]
fn register_can_email() {
    assert_matches_design(
        include_str!("../design/register_can_email.html"),
        register::render(
            "name@example.com",
            &StringOrBool::B(true),
            &urls(),
            register::Message::NoMessage,
        ),
    );
}

#[test]
fn register_must_register() {
    assert_matches_design(
        include_str!("../design/register_must_register.html"),
        register::render(
            "name@example.com",
            &StringOrBool::S(String::from("12345678")),
            &urls(),
            register::Message::MustRegister,
        ),
    );
}

#[test]
fn signup_postregistration() {
    assert_matches_design(
        include_str!("../design/afterregister.html"),
        afterregister::render(&urls()),
    );
}

#[test]
fn circles() {
    let info = circles::Info {
        member_circles: vec![
            circles::CircleRef {
                name: String::from("Greenfold Road Party!!"),
                circleid: String::from("432"),
            },
            circles::CircleRef {
                name: String::from("Hansome family"),
                circleid: String::from("433"),
            },
        ],
        organiser_circles: vec![],
    };
    assert_matches_design(
        include_str!("../design/circles.html"),
        circles::render("alice@example.com", info, &urls(), None),
    );
}

#[test]
fn circles_deleted() {
    let info = circles::Info {
        member_circles: vec![],
        organiser_circles: vec![circles::CircleRef {
            name: String::from("Greenfold Road Party!!"),
            circleid: String::from("432"),
        }],
    };
    assert_matches_design(
        include_str!("../design/circles_deleted.html"),
        circles::render(
            "alice@example.com",
            info,
            &urls(),
            Some(html! { "Circle deleted." }),
        ),
    );
}

#[test]
fn circles_organiser() {
    let info = circles::Info {
        member_circles: vec![],
        organiser_circles: vec![circles::CircleRef {
            name: String::from("Greenfold Road Party!!"),
            circleid: String::from("432"),
        }],
    };
    assert_matches_design(
        include_str!("../design/circles_organiser.html"),
        circles::render("alice@example.com", info, &urls(), None),
    );
}

#[test]
fn circles_unnamed() {
    let info = circles::Info {
        member_circles: vec![circles::CircleRef {
            name: String::from(""),
            circleid: String::from("432"),
        }],
        organiser_circles: vec![circles::CircleRef {
            name: String::from(""),
            circleid: String::from("432"),
        }],
    };
    assert_matches_design(
        include_str!("../design/circles_unnamed.html"),
        circles::render("alice@example.com", info, &urls(), None),
    );
}

#[test]
fn createcircle() {
    assert_matches_design(
        include_str!("../design/createcircle.html"),
        createcircle::render("alice@example.com", &urls()),
    );
}

#[test]
fn organisecircle() {
    assert_matches_design(
        include_str!("../design/organisecircle-432.html"),
        organisecircle::render(
            "alice@example.com",
            &urls(),
            "Upstairs room 2",
            "432",
            "Thursday",
            "Free presents only!",
            &vec![
                UsernameNick {
                    username: String::from("alice@example.com"),
                    nickname: String::from("Alice"),
                },
                UsernameNick {
                    username: String::from("bob@example.com"),
                    nickname: String::from(""),
                },
            ],
            &vec![
                UsernameNickHasWishlist {
                    username: String::from("tine@example.com"),
                    nickname: String::from("Tine"),
                    wishlist_text: Some(String::from("nice stuff")),
                    wishlist_links: None,
                },
                UsernameNickHasWishlist {
                    username: String::from("tune@example.com"),
                    nickname: String::from("Tune"),
                    wishlist_text: None,
                    wishlist_links: None,
                },
            ],
            &vec![],
            &vec![DrawItem {
                from_username: String::from("tine@example.com"),
                to_username: String::from("tune@example.com"),
            }],
            &vec![],
            false,
        ),
    );
}

#[test]
fn organisecircle_drawdone() {
    assert_matches_design(
        include_str!("../design/organisecircle-432-drawdone.html"),
        organisecircle::render(
            "alice@example.com",
            &urls(),
            "Upstairs room 2",
            "432",
            "Thursday",
            "Free presents only!",
            &vec![
                UsernameNick {
                    username: String::from("alice@example.com"),
                    nickname: String::from("Alice"),
                },
                UsernameNick {
                    username: String::from("bob@example.com"),
                    nickname: String::from(""),
                },
            ],
            &vec![
                UsernameNickHasWishlist {
                    username: String::from("tine@example.com"),
                    nickname: String::from("Tine"),
                    wishlist_text: None,
                    wishlist_links: None,
                },
                UsernameNickHasWishlist {
                    username: String::from("tune@example.com"),
                    nickname: String::from("Tune"),
                    wishlist_text: Some(String::from("c")),
                    wishlist_links: None,
                },
                UsernameNickHasWishlist {
                    username: String::from("x@example.com"),
                    nickname: String::from(""),
                    wishlist_text: Some(String::from("")),
                    wishlist_links: Some(vec![WishlistLink {
                        text: String::from("link"),
                        url: String::from("http://example.com"),
                    }]),
                },
                UsernameNickHasWishlist {
                    username: String::from("y@example.com"),
                    nickname: String::from("Y"),
                    wishlist_text: None,
                    wishlist_links: None,
                },
            ],
            &vec![
                DrawItem {
                    from_username: String::from("tine@example.com"),
                    to_username: String::from("tune@example.com"),
                },
                DrawItem {
                    from_username: String::from("y@example.com"),
                    to_username: String::from("x@example.com"),
                },
                DrawItem {
                    from_username: String::from("x@example.com"),
                    to_username: String::from("y@example.com"),
                },
                DrawItem {
                    from_username: String::from("y@example.com"),
                    to_username: String::from("tune@example.com"),
                },
            ],
            &vec![
                DrawItem {
                    from_username: String::from("x@example.com"),
                    to_username: String::from("y@example.com"),
                },
                DrawItem {
                    from_username: String::from("y@example.com"),
                    to_username: String::from("tine@example.com"),
                },
            ],
            &vec![
                DrawWarning::NotReceiving(String::from("tine@example.com")),
                DrawWarning::NotGiving(String::from("tune@example.com")),
                DrawWarning::BreaksRestriction(DrawItem {
                    from_username: String::from("x@example.com"),
                    to_username: String::from("y@example.com"),
                }),
                DrawWarning::GivingMultiple(
                    String::from("y@example.com"),
                    vec![
                        String::from("x@example.com"),
                        String::from("tune@example.com"),
                    ],
                ),
                DrawWarning::ReceivingMultiple(
                    String::from("tune@example.com"),
                    vec![
                        String::from("tine@example.com"),
                        String::from("y@example.com"),
                    ],
                ),
                DrawWarning::EmptyFrom(String::from("y@example.com")),
                DrawWarning::EmptyTo(String::from("y@example.com")),
            ],
            false,
        ),
    );
}

#[test]
fn organisecircle_mustbesure_drawdone() {
    assert_matches_design(
        include_str!("../design/organisecircle-mustbesure-432-drawdone.html"),
        organisecircle::render(
            "alice@example.com",
            &urls(),
            "Upstairs room 2",
            "432",
            "Thursday",
            "Free presents only!",
            &vec![
                UsernameNick {
                    username: String::from("alice@example.com"),
                    nickname: String::from("Alice"),
                },
                UsernameNick {
                    username: String::from("bob@example.com"),
                    nickname: String::from(""),
                },
            ],
            &vec![
                UsernameNickHasWishlist {
                    username: String::from("tine@example.com"),
                    nickname: String::from("Tine"),
                    wishlist_text: None,
                    wishlist_links: None,
                },
                UsernameNickHasWishlist {
                    username: String::from("tune@example.com"),
                    nickname: String::from("Tune"),
                    wishlist_text: None,
                    wishlist_links: None,
                },
                UsernameNickHasWishlist {
                    username: String::from("x@example.com"),
                    nickname: String::from(""),
                    wishlist_text: None,
                    wishlist_links: None,
                },
                UsernameNickHasWishlist {
                    username: String::from("y@example.com"),
                    nickname: String::from("Y"),
                    wishlist_text: None,
                    wishlist_links: None,
                },
            ],
            &vec![
                DrawItem {
                    from_username: String::from("tine@example.com"),
                    to_username: String::from("tune@example.com"),
                },
                DrawItem {
                    from_username: String::from("y@example.com"),
                    to_username: String::from("x@example.com"),
                },
                DrawItem {
                    from_username: String::from("x@example.com"),
                    to_username: String::from("y@example.com"),
                },
                DrawItem {
                    from_username: String::from("y@example.com"),
                    to_username: String::from("tune@example.com"),
                },
            ],
            &vec![
                DrawItem {
                    from_username: String::from("x@example.com"),
                    to_username: String::from("y@example.com"),
                },
                DrawItem {
                    from_username: String::from("y@example.com"),
                    to_username: String::from("tine@example.com"),
                },
            ],
            &vec![
                DrawWarning::NotReceiving(String::from("tine@example.com")),
                DrawWarning::NotGiving(String::from("tune@example.com")),
                DrawWarning::BreaksRestriction(DrawItem {
                    from_username: String::from("x@example.com"),
                    to_username: String::from("y@example.com"),
                }),
                DrawWarning::GivingMultiple(
                    String::from("y@example.com"),
                    vec![
                        String::from("x@example.com"),
                        String::from("tune@example.com"),
                    ],
                ),
                DrawWarning::ReceivingMultiple(
                    String::from("tune@example.com"),
                    vec![
                        String::from("tine@example.com"),
                        String::from("y@example.com"),
                    ],
                ),
                DrawWarning::EmptyFrom(String::from("y@example.com")),
                DrawWarning::EmptyTo(String::from("y@example.com")),
            ],
            true,
        ),
    );
}

#[test]
fn organisecircle_missingemail() {
    assert_matches_design(
        include_str!("../design/organisecircle-missingemail-432.html"),
        organisecircle::render(
            "alice@example.com",
            &urls(),
            "Upstairs room 2",
            "432",
            "Thursday",
            "Free presents only!",
            &vec![
                UsernameNick {
                    username: String::from("alice@example.com"),
                    nickname: String::from("Alice"),
                },
                UsernameNick {
                    username: String::from("bob@example.com"),
                    nickname: String::from(""),
                },
            ],
            &vec![
                UsernameNickHasWishlist {
                    username: String::from("tine@example.com"),
                    nickname: String::from("Tine"),
                    wishlist_text: None,
                    wishlist_links: None,
                },
                UsernameNickHasWishlist {
                    username: String::from("tune@example.com"),
                    nickname: String::from("Tune"),
                    wishlist_text: None,
                    wishlist_links: None,
                },
                UsernameNickHasWishlist {
                    username: String::from(""),
                    nickname: String::from("Nomail"),
                    wishlist_text: None,
                    wishlist_links: None,
                },
            ],
            &vec![],
            &vec![DrawItem {
                from_username: String::from("tine@example.com"),
                to_username: String::from("tune@example.com"),
            }],
            &vec![],
            false,
        ),
    );
}

#[test]
fn organisecircle_missingemail_drawdone() {
    assert_matches_design(
        include_str!("../design/organisecircle-missingemail-432-drawdone.html"),
        organisecircle::render(
            "alice@example.com",
            &urls(),
            "Upstairs room 2",
            "432",
            "Thursday",
            "Free presents only!",
            &vec![
                UsernameNick {
                    username: String::from("alice@example.com"),
                    nickname: String::from("Alice"),
                },
                UsernameNick {
                    username: String::from("bob@example.com"),
                    nickname: String::from(""),
                },
            ],
            &vec![
                UsernameNickHasWishlist {
                    username: String::from("tine@example.com"),
                    nickname: String::from("Tine"),
                    wishlist_text: None,
                    wishlist_links: None,
                },
                UsernameNickHasWishlist {
                    username: String::from("tune@example.com"),
                    nickname: String::from("Tune"),
                    wishlist_text: None,
                    wishlist_links: None,
                },
                UsernameNickHasWishlist {
                    username: String::from("x@example.com"),
                    nickname: String::from(""),
                    wishlist_text: None,
                    wishlist_links: None,
                },
                UsernameNickHasWishlist {
                    username: String::from("y@example.com"),
                    nickname: String::from("Y"),
                    wishlist_text: Some(String::from("y things")),
                    wishlist_links: Some(vec![WishlistLink {
                        text: String::from("t"),
                        url: String::from("u"),
                    }]),
                },
                UsernameNickHasWishlist {
                    username: String::from(""),
                    nickname: String::from("Nomail"),
                    wishlist_text: None,
                    wishlist_links: Some(Vec::new()),
                },
            ],
            &vec![
                DrawItem {
                    from_username: String::from("tine@example.com"),
                    to_username: String::from("tune@example.com"),
                },
                DrawItem {
                    from_username: String::from("y@example.com"),
                    to_username: String::from("x@example.com"),
                },
                DrawItem {
                    from_username: String::from("x@example.com"),
                    to_username: String::from("y@example.com"),
                },
                DrawItem {
                    from_username: String::from("y@example.com"),
                    to_username: String::from("tune@example.com"),
                },
            ],
            &vec![
                DrawItem {
                    from_username: String::from("x@example.com"),
                    to_username: String::from("y@example.com"),
                },
                DrawItem {
                    from_username: String::from("y@example.com"),
                    to_username: String::from("tine@example.com"),
                },
            ],
            &vec![
                DrawWarning::NotReceiving(String::from("tine@example.com")),
                DrawWarning::NotGiving(String::from("tune@example.com")),
                DrawWarning::BreaksRestriction(DrawItem {
                    from_username: String::from("x@example.com"),
                    to_username: String::from("y@example.com"),
                }),
                DrawWarning::GivingMultiple(
                    String::from("y@example.com"),
                    vec![
                        String::from("x@example.com"),
                        String::from("tune@example.com"),
                    ],
                ),
                DrawWarning::ReceivingMultiple(
                    String::from("tune@example.com"),
                    vec![
                        String::from("tine@example.com"),
                        String::from("y@example.com"),
                    ],
                ),
                DrawWarning::EmptyFrom(String::from("y@example.com")),
                DrawWarning::EmptyTo(String::from("y@example.com")),
            ],
            false,
        ),
    );
}

#[test]
fn circle() {
    assert_matches_design(
        include_str!("../design/circle-433.html"),
        circle::render(
            "alice@example.com",
            &urls(),
            "Greenfold Road Party!!",
            "433",
            "12th December 2021",
            "\u{00A3}10 limit, exchange gifts at the party at J's house.",
            &Some(vec![Recipient::new(
                "Bob Brown",
                "Something chocolate, or a Pokemon toy",
                vec![
                    WishlistLink {
                        text: String::from("Choccy bits"),
                        url: String::from("https://choccy.example.com"),
                    },
                    WishlistLink {
                        text: String::from("Pokeball Z2"),
                        url: String::from("http://pokeball.example.com/z2"),
                    },
                ],
            )]),
            &Recipient::new(
                "",
                "Something unique to you, not from a shop",
                vec![
                    WishlistLink {
                        text: String::from("Thing I like"),
                        url: String::from("https://likeable.example.com"),
                    },
                    WishlistLink {
                        text: String::from("Something pretty"),
                        url: String::from(
                            "https://likeable.example.com/pretty",
                        ),
                    },
                ],
            ),
        ),
    );
}

#[test]
fn circle_no_draw() {
    assert_matches_design(
        include_str!("../design/circle-no-draw-433.html"),
        circle::render(
            "alice@example.com",
            &urls(),
            "Greenfold Road Party!!",
            "433",
            "12th December 2021",
            "\u{00A3}10 limit, exchange gifts at the party at J's house.",
            &None,
            &Recipient::new(
                "",
                "Something unique to you, not from a shop",
                vec![
                    WishlistLink {
                        text: String::from("Thing I like"),
                        url: String::from("https://likeable.example.com"),
                    },
                    WishlistLink {
                        text: String::from("Something pretty"),
                        url: String::from(
                            "https://likeable.example.com/pretty",
                        ),
                    },
                ],
            ),
        ),
    );
}

#[test]
fn circle_recipient_no_wishlist() {
    assert_matches_design(
        include_str!("../design/circle-recipient-no-wishlist-433.html"),
        circle::render(
            "alice@example.com",
            &urls(),
            "Greenfold Road Party!!",
            "433",
            "12th December 2021",
            "\u{00A3}10 limit, exchange gifts at the party at J's house.",
            &Some(vec![Recipient::new("Bob Brown", "", Vec::new())]),
            &Recipient::new(
                "",
                "Something unique to you, not from a shop",
                vec![
                    WishlistLink {
                        text: String::from("Thing I like"),
                        url: String::from("https://likeable.example.com"),
                    },
                    WishlistLink {
                        text: String::from("Something pretty"),
                        url: String::from(
                            "https://likeable.example.com/pretty",
                        ),
                    },
                ],
            ),
        ),
    );
}

#[test]
fn circle_no_recipients() {
    assert_matches_design(
        include_str!("../design/circle-no-recipients-433.html"),
        circle::render(
            "alice@example.com",
            &urls(),
            "Greenfold Road Party!!",
            "433",
            "12th December 2021",
            "\u{00A3}10 limit, exchange gifts at the party at J's house.",
            &Some(vec![]),
            &Recipient::new(
                "",
                "Something unique to you, not from a shop",
                vec![
                    WishlistLink {
                        text: String::from("Thing I like"),
                        url: String::from("https://likeable.example.com"),
                    },
                    WishlistLink {
                        text: String::from("Something pretty"),
                        url: String::from(
                            "https://likeable.example.com/pretty",
                        ),
                    },
                ],
            ),
        ),
    );
}

#[test]
fn circle_multiple_recipients() {
    assert_matches_design(
        include_str!("../design/circle-multiple-recipients-433.html"),
        circle::render(
            "alice@example.com",
            &urls(),
            "Greenfold Road Party!!",
            "433",
            "12th December 2021",
            "\u{00A3}10 limit, exchange gifts at the party at J's house.",
            &Some(vec![
                Recipient::new(
                    "Bob Brown",
                    "Something chocolate, or a Pokemon toy",
                    vec![
                        WishlistLink {
                            text: String::from("Choccy bits"),
                            url: String::from("https://choccy.example.com"),
                        },
                        WishlistLink {
                            text: String::from("Pokeball Z2"),
                            url: String::from("http://pokeball.example.com/z2"),
                        },
                    ],
                ),
                Recipient::new("Chloe Cabbage", "Veggies!", vec![]),
            ]),
            &Recipient::new(
                "",
                "Something unique to you, not from a shop",
                vec![
                    WishlistLink {
                        text: String::from("Thing I like"),
                        url: String::from("https://likeable.example.com"),
                    },
                    WishlistLink {
                        text: String::from("Something pretty"),
                        url: String::from(
                            "https://likeable.example.com/pretty",
                        ),
                    },
                ],
            ),
        ),
    );
}

#[test]
fn contact() {
    let s = r#"You can contact us on
     <a href="mailto:address@example.com">address@example.com</a>."#;

    assert_matches_design(
        include_str!("../design/contact.html"),
        contact::render(&urls(), s),
    );
}

#[test]
fn privacy() {
    let s = "None of your data will be eaten in our underwater \
        containment facility.";

    assert_matches_design(
        include_str!("../design/privacy.html"),
        privacy::render(&urls(), s),
    );
}

#[test]
fn account() {
    assert_matches_design(
        include_str!("../design/account.html"),
        account::render(
            "alice@example.com",
            account::Info::new(),
            &urls(),
            None,
            None,
        ),
    );
}

#[test]
fn deleteaccount() {
    assert_matches_design(
        include_str!("../design/deleteaccount.html"),
        deleteaccount::render("alice@example.com", &urls(), None, None),
    );
}

#[test]
fn forgotpassword() {
    assert_matches_design(
        include_str!("../design/forgotpassword.html"),
        forgotpassword::render(&urls()),
    );
}

#[test]
fn resetpassword() {
    assert_matches_design(
        include_str!("../design/resetpassword.html"),
        resetpassword::render(
            "alice@example.com",
            &urls(),
            resetpassword::Message::NoMessage,
        ),
    );
}

#[test]
fn resetpassword_failed_to_send() {
    assert_matches_design(
        include_str!("../design/resetpassword_failed_to_send.html"),
        resetpassword::render(
            "alice@example.com",
            &urls(),
            resetpassword::Message::ResetRequestFailed,
        ),
    );
}

// ---

fn urls() -> HashMap<String, String> {
    HashMap::<_, _>::from_iter(
        [
            (String::from("account"), String::from("account.html")),
            (
                String::from("afterregister"),
                String::from("afterregister.html"),
            ),
            (String::from("circle"), String::from("circle-{}.html")),
            (String::from("circles"), String::from("circles.html")),
            (String::from("contact"), String::from("contact.html")),
            (
                String::from("createcircle"),
                String::from("createcircle.html"),
            ),
            (
                String::from("deleteaccount"),
                String::from("deleteaccount.html"),
            ),
            (
                String::from("deletecircle"),
                String::from("deletecircle-{}.html"),
            ),
            (
                String::from("forgotpassword"),
                String::from("forgotpassword.html"),
            ),
            (String::from("login"), String::from("login.html")),
            (
                String::from("organisecircle"),
                String::from("organisecircle-{}.html"),
            ),
            (String::from("privacy"), String::from("privacy.html")),
            (String::from("register"), String::from("register.html")),
            (
                String::from("resetpassword"),
                String::from("resetpassword.html"),
            ),
            (String::from("root"), String::from("index.html")),
            (String::from("signup"), String::from("signup.html")),
            (String::from("style_css"), String::from("style.css")),
        ]
        .into_iter(),
    )
}

fn assert_matches_design(expected: &str, actual: Markup) {
    assert_str_eq!(
        normalize_xml(expected),
        normalize_xml(&actual.into_string()),
    )
}

fn normalize_xml(input: &str) -> String {
    let mut reader = Reader::from_str(input);
    reader.trim_text(true);
    reader.check_end_names(false);
    reader.expand_empty_elements(true);
    let mut writer =
        Writer::new_with_indent(Cursor::new(Vec::new()), ' ' as u8, 4);
    let mut buf = Vec::new();
    loop {
        match reader.read_event(&mut buf) {
            Ok(Event::Eof) => {
                return String::from_utf8(writer.into_inner().into_inner())
                    .expect("Unable to decode XML as UTF-8!")
            }
            Ok(Event::Start(bytes_start)) => {
                // Strip whitespace between attributes
                let name = bytes_start.name();
                let attrs = bytes_start
                    .html_attributes()
                    .map(|a| a.unwrap())
                    .filter(|a| a.key.len() != 0);
                let new_bytes =
                    BytesStart::owned_name(name.clone()).with_attributes(attrs);
                assert!(writer.write_event(Event::Start(new_bytes)).is_ok())
            }
            Ok(event) => assert!(writer.write_event(event).is_ok()),
            Err(err) => panic!("{}", err),
        }
        buf.clear();
    }
}
