use serde::{Deserialize, Deserializer};

#[derive(Deserialize)]
pub struct LoginCredentials {
    pub username: String,
    pub password: String,
    #[serde(default)]
    #[serde(deserialize_with = "on_off_to_bool")]
    pub remember_me: bool,
}
fn on_off_to_bool<'de, D>(d: D) -> Result<bool, D::Error>
where
    D: Deserializer<'de>,
{
    String::deserialize(d).map(|s| s == "on")
}
