use std::collections::HashMap;

use actix_http::Payload;
use actix_web::error::UrlGenerationError;
use actix_web::{web, FromRequest, HttpRequest};
use futures::future::{self, FutureExt, Map};
use url::Url;

use crate::data_holder::DataHolder;

#[derive(Clone)]
pub struct UrlPrefix {
    prefix: Option<String>,
}

impl UrlPrefix {
    pub fn new(prefix: Option<String>) -> Self {
        Self { prefix }
    }

    pub fn bind<'a>(&'a self, req: &'a HttpRequest) -> BoundUrls<'a> {
        BoundUrls {
            req,
            prefix: self.prefix.clone(),
        }
    }
}

impl FromRequest for UrlPrefix {
    type Error = actix_web::Error; // We can't return an LscError since it
                                   // requires a Urls!
    type Future = Map<
        future::Ready<Result<web::Data<DataHolder>, actix_web::Error>>,
        Box<
            dyn FnOnce(
                Result<web::Data<DataHolder>, actix_web::Error>,
            ) -> Result<Self, Self::Error>,
        >,
    >;

    type Config = ();

    fn from_request(
        req: &HttpRequest,
        payload: &mut Payload,
    ) -> <Self as FromRequest>::Future {
        web::Data::<DataHolder>::from_request(req, payload).map(Box::new(
            |res_data_holder: Result<
                web::Data<DataHolder>,
                actix_web::Error,
            >| {
                res_data_holder
                    .map(|data_holder| data_holder.url_prefix.clone())
            },
        ))
    }
}

pub trait Urls {
    fn url_for<U, I>(
        &self,
        name: &str,
        elements: U,
    ) -> Result<Url, UrlGenerationError>
    where
        U: IntoIterator<Item = I>,
        I: AsRef<str>;

    /**
     * Get a url which takes path substitutions.
     */
    fn u2<U, I>(&self, name: &str, elements: U) -> String
    where
        U: IntoIterator<Item = I>,
        I: AsRef<str>;

    fn url_for_static(&self, name: &str) -> Result<Url, UrlGenerationError>;

    /**
     * Get a url with a fixed path.
     */
    fn u(&self, name: &str) -> String;
}

impl Urls for HashMap<String, String> {
    fn url_for<U, I>(
        &self,
        name: &str,
        elements: U,
    ) -> Result<Url, UrlGenerationError>
    where
        U: IntoIterator<Item = I>,
        I: AsRef<str>,
    {
        Url::parse(&self.u2(name, elements))
            .map_err(|e| UrlGenerationError::ParseError(e))
    }

    fn u2<U, I>(&self, name: &str, elements: U) -> String
    where
        U: IntoIterator<Item = I>,
        I: AsRef<str>,
    {
        let mut url = self
            .get(name)
            .cloned()
            .unwrap_or(format!("missing:{}:elements", name));
        for e in elements {
            url = url.replacen("{}", e.as_ref(), 1);
        }
        url
    }

    fn url_for_static(&self, name: &str) -> Result<Url, UrlGenerationError> {
        Url::parse(&self.u(name)).map_err(|e| UrlGenerationError::ParseError(e))
    }

    fn u(&self, name: &str) -> String {
        self.get(name)
            .cloned()
            .unwrap_or(format!("missing:{}", name))
    }
}

pub struct BoundUrls<'a> {
    req: &'a HttpRequest,
    prefix: Option<String>,
}

impl<'a> BoundUrls<'a> {
    fn prepend_prefix(&self, mut url: Url) -> Url {
        if let Some(prefix) = &self.prefix {
            url.set_path(&format!("{}{}", prefix, url.path()))
        }
        url
    }
}

impl<'a> Urls for BoundUrls<'a> {
    fn url_for<U, I>(
        &self,
        name: &str,
        elements: U,
    ) -> Result<Url, UrlGenerationError>
    where
        U: IntoIterator<Item = I>,
        I: AsRef<str>,
    {
        self.req
            .url_for(name, elements)
            .map(|url| self.prepend_prefix(url))
            .map_err(|_| panic!("Missing resource: {}", name))
    }

    fn u2<U, I>(&self, name: &str, elements: U) -> String
    where
        U: IntoIterator<Item = I>,
        I: AsRef<str>,
    {
        self.url_for(name, elements).unwrap().to_string()
    }

    fn url_for_static(&self, name: &str) -> Result<Url, UrlGenerationError> {
        self.req
            .url_for_static(name)
            .map(|url| self.prepend_prefix(url))
            .map_err(|_| panic!("Missing static resource: {}", name))
    }

    fn u(&self, name: &str) -> String {
        self.url_for_static(name).unwrap().to_string()
    }
}
