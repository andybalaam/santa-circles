use std::collections::HashMap;

fn keys_with_prefixes(
    form: &HashMap<String, String>,
    prefix_1: &str,
    prefix_2: &str,
) -> Vec<(String, String)> {
    let mut visited_k2s = Vec::new();
    let mut ret = Vec::new();
    for k1 in form.keys().filter(|k| k.starts_with(prefix_1)) {
        let suffix = &k1[prefix_1.len()..];
        let k2 = format!("{}{}", prefix_2, suffix);
        if form.contains_key(&k2) {
            visited_k2s.push(k2.clone());
        }
        ret.push((String::from(k1), k2));
    }
    for k2 in form
        .keys()
        .filter(|k| k.starts_with(prefix_2) && !visited_k2s.contains(k))
    {
        let suffix = &k2[prefix_2.len()..];
        let k1 = format!("{}{}", prefix_1, suffix);
        ret.push((k1, String::from(k2)));
    }
    ret.sort();
    ret
}

pub fn form_pairs<T>(
    form: &HashMap<String, String>,
    prefix_1: &str,
    prefix_2: &str,
    f: &dyn Fn(String, String) -> T,
) -> Vec<T> {
    let keys = keys_with_prefixes(&form, prefix_1, prefix_2);

    keys.iter()
        .filter_map(|(left_key, right_key)| {
            let left = form.get(left_key);
            let right = form.get(right_key);

            if left.is_none() && right.is_none() {
                None
            } else {
                let left = left.map(|s| s.as_str().trim()).unwrap_or("");
                let right = right.map(|s| s.as_str().trim()).unwrap_or("");
                if left == "" && right == "" {
                    None
                } else {
                    Some(f(
                        String::from(left.trim()),
                        String::from(right.trim()),
                    ))
                }
            }
        })
        .collect()
}
