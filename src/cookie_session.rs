use actix_http::cookie::SameSite;

use crate::actix_session_remember::CookieSession;

// TODO: load or generate and store a private key
const SESSION_PRIVATE_KEY: [u8; 32] = [0; 32];

pub fn new_session() -> CookieSession {
    // TODO: when we have a real private key, make a private session
    CookieSession::signed(&SESSION_PRIVATE_KEY)
        .name("auth")
        .http_only(true)
        .secure(false) // TODO: this should be a config option
        .same_site(SameSite::Lax)
}
