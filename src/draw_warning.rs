use serde::{Deserialize, Serialize};

use crate::draw_item::DrawItem;

pub enum DrawWarningSeverity {
    Info,
    Warning,
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub enum DrawWarning {
    BreaksRestriction(DrawItem),
    EmptyFrom(String),
    EmptyTo(String),
    GivingMultiple(String, Vec<String>),
    NoUsername(String),
    NotGiving(String),
    NotReceiving(String),
    ReceivingMultiple(String, Vec<String>),
}

impl DrawWarning {
    pub fn severity(&self) -> DrawWarningSeverity {
        match self {
            DrawWarning::NotReceiving(_) => DrawWarningSeverity::Warning,
            DrawWarning::NotGiving(_) => DrawWarningSeverity::Warning,
            DrawWarning::BreaksRestriction(_) => DrawWarningSeverity::Warning,

            DrawWarning::GivingMultiple(_, _) => DrawWarningSeverity::Info,
            DrawWarning::NoUsername(_) => DrawWarningSeverity::Warning,
            DrawWarning::ReceivingMultiple(_, _) => DrawWarningSeverity::Info,
            DrawWarning::EmptyFrom(_) => DrawWarningSeverity::Info,
            DrawWarning::EmptyTo(_) => DrawWarningSeverity::Info,
        }
    }
}
