use actix_web::middleware::normalize::TrailingSlash;
use actix_web::{middleware, App, HttpServer};

use santa_circles::config;
use santa_circles::contact_page::ContactPage;
use santa_circles::cookie_session;
use santa_circles::data_holder::DataHolder;
use santa_circles::real_api::RealApi;
use santa_circles::urls::UrlPrefix;

struct Args {
    server_url: String,
    bind: String,
    port: String,
    path_prefix: Option<String>,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::Builder::from_env(
        env_logger::Env::default().default_filter_or("info"),
    )
    .init();

    let contact_page = ContactPage::load("contact.html");
    let args = cmdline_args();
    let server_url = String::from(args.server_url);
    let path_prefix: Option<String> = args.path_prefix.clone();

    println!("Connecting to server at {}", server_url);
    println!("Listening on http://{}:{}", args.bind, args.port);
    HttpServer::new(move || {
        App::new()
            .wrap(middleware::NormalizePath::new(TrailingSlash::Trim))
            .data(DataHolder::new(
                Box::new(RealApi::new(server_url.clone())),
                contact_page.clone(),
                UrlPrefix::new(path_prefix.clone()),
            ))
            .wrap(cookie_session::new_session())
            .wrap(middleware::Logger::default())
            .configure(config::config)
    })
    .bind(format!("{}:{}", args.bind, args.port))?
    .run()
    .await
}

fn cmdline_args() -> Args {
    let matches = clap::App::new("santa-circles")
        .version(&clap::crate_version!()[..])
        .about(
            "A secret santa web site.\n\
             See https://gitlab.com/andybalaam/santa-circles",
        )
        .arg(
            clap::Arg::new("server_url")
                .long("server_url")
                .value_name("URL")
                .help(
                    "Specify server to connect to \
                     [default: http://localhost:8088]",
                ),
        )
        .arg(
            clap::Arg::new("bind")
                .long("bind")
                .value_name("ADDRESS")
                .help(
                    "Specify alternate bind address [default: all interfaces]",
                ),
        )
        .arg(
            clap::Arg::new("PORT")
                .index(1)
                .help("Specify alternate port [default: 8089]"),
        )
        .arg(
            clap::Arg::new("path-prefix")
                .long("path-prefix")
                .value_name("PREFIX")
                .help(
                    "Specify a prefix to add to each path.  If you are running
                    the server inside a subdirectory via a proxy, you should
                    supply the subdirectory name here.
                    [default: no prefix is added]",
                ),
        )
        .get_matches();

    Args {
        server_url: String::from(
            matches
                .value_of("server_url")
                .unwrap_or("http://localhost:8088"),
        ),
        bind: String::from(matches.value_of("bind").unwrap_or("0.0.0.0")),
        port: String::from(matches.value_of("PORT").unwrap_or("8089")),
        path_prefix: matches.value_of("path-prefix").map(String::from),
    }
}
