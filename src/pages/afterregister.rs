use actix_web::{web, HttpRequest};
use maud::{html, Markup};
use serde::{Deserialize, Serialize};
use serde_json::json;

use crate::api::ApiError;
use crate::data_holder::DataHolder;
use crate::pages::{all, register};
use crate::string_or_bool::StringOrBool;
use crate::urls::{UrlPrefix, Urls};

#[derive(Deserialize, Serialize)]
pub struct RegistrationCode {
    pub username: String,
    pub registration_code: String,
    pub real_registration_code: Option<String>,
}

pub async fn post(
    req: HttpRequest,
    url_prefix: UrlPrefix,
    data_holder: web::Data<DataHolder>,
    registration_code: web::Form<RegistrationCode>,
) -> Markup {
    let username = registration_code.username.trim();
    let urls = url_prefix.bind(&req);
    let real_registration_code =
        if let Some(code) = &registration_code.real_registration_code {
            StringOrBool::S(code.clone())
        } else {
            StringOrBool::B(false)
        };

    register_user(username, &data_holder, &registration_code.registration_code)
        .await
        .map(|_| render(&urls))
        .unwrap_or_else(|_| {
            register::render(
                // We hack here by passing the real code in at the same
                // time we ask the user to enter it.  To avoid this we
                // would remove real_registration_code from
                // render_register(), and email it to the user instead
                // of just telling them.
                username,
                &real_registration_code,
                &urls,
                register::Message::RegistrationFailed,
                // TODO: Different messages for:
                // * UserAlreadyRegistered
                // * IncorrectRegistrationCode
                // * unknown error
            )
        })
}

async fn register_user(
    username: &str,
    data_holder: &DataHolder,
    registration_code: &str,
) -> Result<(), ApiError> {
    let url = format!("/v1/newuser/{}", username);
    data_holder
        .api
        .put_no_credentials(
            &url,
            json!({ "registration_code": registration_code }),
        )
        .await
        .map(|_| ())
}

pub fn render(urls: &impl Urls) -> Markup {
    all::render(
        None,
        "Registration succeeded",
        urls,
        None,
        None,
        None,
        html! {
            h2 { "User created" }
            p { "You are registered!" }
            h2 { "Log in" }
            p {
                "Please "
                a class="emphasised" href={(urls.u("login"))} {"log in"}
                " to start using Santa Circles."
            }
        },
    )
}
