use crate::data_holder::DataHolder;
use crate::login_credentials::LoginCredentials;
use crate::lsc_error::LscError;
use crate::pages::register;
use crate::string_or_bool::StringOrBool;
use crate::urls::UrlPrefix;
use crate::{credentials::Credentials, urls::Urls};
use actix_web::{http, web, FromRequest, HttpRequest, HttpResponse};

pub async fn post(
    req: HttpRequest,
    url_prefix: UrlPrefix,
    login_credentials: web::Form<LoginCredentials>,
    data_holder: web::Data<DataHolder>,
) -> Result<HttpResponse, LscError> {
    let urls = url_prefix.bind(&req);
    let res = Credentials::save(&req, &urls, &login_credentials).await;
    let username = login_credentials.username.trim();
    match res {
        Ok(_) => do_get(&req, &url_prefix).await,
        Err(LscError::Forbidden(_)) => {
            let reg = fetch_registration_code(
                &req,
                &url_prefix,
                &data_holder,
                username,
            )
            .await;
            match reg {
                Ok(registration_code) => Ok(display_register_page_on_root(
                    &urls,
                    username,
                    &registration_code,
                )),
                Err(e) => Err(e),
            }
        }
        Err(e) => Err(e),
    }
}

fn display_register_page_on_root(
    urls: &impl Urls,
    username: &str,
    registration_code: &StringOrBool,
) -> HttpResponse {
    HttpResponse::Ok().body(
        register::render(
            username,
            registration_code,
            urls,
            register::Message::MustRegister,
        )
        .into_string(),
    )
}

async fn fetch_registration_code(
    req: &HttpRequest,
    url_prefix: &UrlPrefix,
    data_holder: &DataHolder,
    username: &str,
) -> Result<StringOrBool, LscError> {
    let urls = url_prefix.bind(req);
    let url = format!("/v1/newuser/{}", username);
    data_holder
        .api
        .get_no_credentials(&url)
        .await
        .map_err(|e| LscError::from_api_error(req, &urls, e))
        .and_then(|reg_code_json| {
            reg_code_json
                .get("registration_code")
                .ok_or(LscError::internal_error(
                    "Failed to get reg code from signup/username response.",
                    req,
                    &urls,
                    "Failed to get reg code from signup/username response.",
                ))
                .and_then(|value| {
                    if value.is_string() {
                        Ok(StringOrBool::S(value.as_str().unwrap().to_owned()))
                    } else if value.is_boolean() {
                        Ok(StringOrBool::B(value.as_bool().unwrap()))
                    } else {
                        Err(LscError::internal_error(
                            "Registration code was not a string or a bool.",
                            req,
                            &urls,
                            "Registration code was not a string or a bool.",
                        ))
                    }
                })
        })
}

pub async fn get(
    req: HttpRequest,
    url_prefix: UrlPrefix,
) -> Result<HttpResponse, LscError> {
    do_get(&req, &url_prefix).await
}

async fn do_get(
    req: &HttpRequest,
    url_prefix: &UrlPrefix,
) -> Result<HttpResponse, LscError> {
    let urls = url_prefix.bind(req);
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            validate_login(&req, &urls, &credentials).await.map(|_| {
                HttpResponse::Found()
                    .header(
                        http::header::LOCATION,
                        urls.u2("circles", vec![credentials.username]),
                    )
                    .finish()
                    .into_body()
            })
        }
        Err(e) => Err(e),
    }
}

async fn validate_login(
    req: &HttpRequest,
    urls: &impl Urls,
    credentials: &Credentials,
) -> Result<(), LscError> {
    let data_holder = web::Data::<DataHolder>::extract(&req).await.unwrap();
    let url = format!("/v1/user/{}", credentials.username);
    data_holder
        .api
        .get(&credentials, &url)
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .map(|_| ())
}
