use std::collections::HashMap;

use actix_http::http;
use actix_web::{web, HttpRequest, HttpResponse};
use maud::{html, Markup};
use serde::{Deserialize, Serialize};

use crate::credentials::Credentials;
use crate::data_holder::DataHolder;
use crate::form_pairs::form_pairs;
use crate::lsc_error::LscError;
use crate::member::Member;
use crate::pages::all;
use crate::recipient::Recipient;
use crate::urls::{UrlPrefix, Urls};
use crate::usernamenick::UsernameNick;
use crate::wishlist_link::WishlistLink;

#[derive(Deserialize)]
pub struct Path {
    circleid: String,
}

pub async fn get(
    req: HttpRequest,
    url_prefix: UrlPrefix,
    path: web::Path<Path>,
    data_holder: web::Data<DataHolder>,
) -> Result<Markup, LscError> {
    let urls = url_prefix.bind(&req);
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => fetch_circle_info(
            // TODO: fetch_member_circle_info
            &path.circleid,
            &req,
            &urls,
            &data_holder,
            &credentials,
        )
        .await
        .map(|circle_info| {
            let recipients = circle_info.recipients.as_ref().map(|recips| {
                recips
                    .iter()
                    .filter_map(|username| {
                        Recipient::extract_from_circle(
                            username,
                            &circle_info.members,
                        )
                    })
                    .collect()
            });

            let own_recipient_info = Recipient::extract_from_circle(
                &credentials.username,
                &circle_info.members,
            )
            .unwrap_or(Recipient {
                name: credentials.username.clone(),
                wishlist_text: String::from(""),
                wishlist_links: Vec::new(),
            });

            render(
                &credentials.username,
                &urls,
                &circle_info.name,
                &circle_info.circleid,
                &circle_info.draw_date,
                &circle_info.rules,
                &recipients,
                &own_recipient_info,
            )
        }),
        Err(e) => Err(e),
    }
}

#[derive(Debug, Deserialize)]
pub struct MemberCircleInfo {
    pub circleid: String,
    pub name: String,
    pub draw_date: String,
    pub rules: String,
    pub organisers: Vec<UsernameNick>,
    pub members: Vec<Member>,
    pub recipients: Option<Vec<String>>,
}

pub async fn fetch_circle_info(
    circleid: &str,
    req: &HttpRequest,
    urls: &impl Urls,
    data_holder: &DataHolder,
    credentials: &Credentials,
) -> Result<MemberCircleInfo, LscError> {
    let url = format!("/v1/circle/{}", circleid);
    data_holder
        .api
        .get(&credentials, &url)
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .and_then(|circle_json| {
            serde_json::from_value(circle_json).map_err(|e| {
                LscError::internal_error(
                    "Failed to parse Circle",
                    req,
                    urls,
                    &format!("Failed to parse Circle: {}", e),
                )
            })
        })
}

#[derive(Serialize)]
pub struct WishlistApiRequest {
    wishlist_text: String,
    wishlist_links: Vec<WishlistLink>,
}

#[derive(Deserialize)]
pub struct WishlistApiResponse {
    circleid: String,
    // username: String, unused
}

impl WishlistApiRequest {
    pub fn try_from_form(
        new_wishlist_form: web::Form<HashMap<String, String>>,
    ) -> Result<WishlistApiRequest, LscError> {
        let wishlist_text = new_wishlist_form
            .get("wishlist-text")
            .cloned()
            .unwrap_or(String::from(""));

        let wishlist_links = form_pairs(
            &new_wishlist_form,
            "wishlist-link-text-",
            "wishlist-link-url-",
            &|text, url| WishlistLink { text, url },
        );

        Ok(WishlistApiRequest {
            wishlist_text,
            wishlist_links,
        })
    }
}

pub async fn post(
    req: HttpRequest,
    url_prefix: UrlPrefix,
    path: web::Path<Path>,
    data_holder: web::Data<DataHolder>,
    update_wishlist_form: web::Form<HashMap<String, String>>,
) -> Result<HttpResponse, LscError> {
    let urls = url_prefix.bind(&req);
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => data_holder
            .api
            .put(
                &credentials,
                &format!(
                    "/v1/circle/{}/members/{}/wishlist",
                    path.circleid, credentials.username
                ),
                serde_json::to_value(WishlistApiRequest::try_from_form(
                    update_wishlist_form,
                )?)
                .unwrap(),
            )
            .await
            .map_err(|e| LscError::from_api_error(&req, &urls, e))
            .and_then(|res| {
                serde_json::from_value::<WishlistApiResponse>(res)
                    .map(|circle_info| {
                        // Redirect to ourself, which is lazy - we could just
                        // render here, if we fetched from the API
                        HttpResponse::Found()
                            .header(
                                http::header::LOCATION,
                                urls.u2(
                                    "circle",
                                    &[String::from(&circle_info.circleid)],
                                ),
                            )
                            .finish()
                            .into_body()
                    })
                    .map_err(|e| {
                        LscError::internal_error(
                            "Failed to parse API response while updating \
                            circle",
                            &req,
                            &urls,
                            &format!(
                                "Failed to parse API response while \
                                updating circle: {}",
                                e
                            ),
                        )
                    })
            }),
        Err(e) => Err(e),
    }
}

pub fn render(
    username: &str,
    urls: &impl Urls,
    circle_name: &str,
    circleid: &str,
    draw_date: &str,
    rules: &str,
    recipients: &Option<Vec<Recipient>>,
    own_recipient_info: &Recipient,
) -> Markup {
    all::render(
        None,
        circle_name,
        urls,
        None,
        None,
        Some(username),
        html! {
            p {
                strong { "Draw date:" }
                (draw_date)
            }
            p {
                strong { "Rules:" }
                (rules)
            }
            @if let Some(recipients) = recipients {
                @if recipients.is_empty() {
                    h2 { "The draw is done" }
                    p {
                        "You are not giving a gift to anyone. "
                        "(Contact the organiser if that sounds\nwrong.)"
                    }
                } @else {
                    @for recipient in recipients {
                        h2 { "You are buying a gift for:" }
                        p class="recipient" { (recipient.name) }
                        @if recipient.has_wishlist() {
                            h3 { "Wishlist:" }
                            p { (recipient.wishlist_text) }
                            ul {
                                @for link in &recipient.wishlist_links {
                                    li { a href=(link.url) { (link.text) } }
                                }
                            }
                        } @else {
                            h3 { "No wishlist yet" }
                            p {
                                "The person you are buying for has not made "
                                "a wishlist. Contact your circle\norganiser "
                                "if you want to encourage them to make one."
                            }
                        }
                    }
                }
            } @else {
                h2 { "Waiting for the draw" }
                p { "The draw has not happened yet." }
            }
            h2 { "Make a wishlist" }
            p { "You should make a wishlist to help your buyer find a gift." }
            @let action = urls.u2("circle", vec![circleid]);
            form class="wishlist" action=(action) method="post" {
                p {
                    "Describe the types of gifts you would like:"
                    input
                        type="text"
                        id="wishlist-text"
                        name="wishlist-text"
                        value=(own_recipient_info.wishlist_text)
                        {}
                }
                p { "Link to some things you'd like:" }
                table {
                    tr {
                        th { "Link name" }
                        th { "Link" }
                    }
                    @for (i, link) in
                        own_recipient_info.wishlist_links
                            .iter()
                            .chain(two_empty_links().iter())
                            .enumerate() {
                        @let linkname_n = format!("wishlist-link-text-{}", i + 1);
                        @let link_n = format!("wishlist-link-url-{}", i + 1);
                        tr {
                            td {
                                input
                                    type="text"
                                    id=(linkname_n)
                                    name=(linkname_n)
                                    value=(link.text)
                                    {}
                            }
                            td {
                                input
                                    type="text"
                                    id=(link_n)
                                    name=(link_n)
                                    value=(link.url)
                                    placeholder="http://example.com"
                                    {}
                            }
                        }
                    }
                }
                p { "(Save to show more free slots.)" }
                input
                    class="end-submit"
                    type="submit"
                    id="save_wishlist"
                    name="save_wishlist"
                    value="Save wishlist"
                    {}
            }
        },
    )
}

fn two_empty_links() -> Vec<WishlistLink> {
    vec![
        WishlistLink {
            text: String::from(""),
            url: String::from(""),
        },
        WishlistLink {
            text: String::from(""),
            url: String::from(""),
        },
    ]
}
