use actix_web::HttpRequest;
use maud::{html, Markup};
use serde::Deserialize;

use crate::pages::all;
use crate::urls::{UrlPrefix, Urls};

pub async fn get(req: HttpRequest, url_prefix: UrlPrefix) -> Markup {
    let urls = url_prefix.bind(&req);
    render(&urls, Message::NoMessage)
}

#[derive(Deserialize)]
pub enum Message {
    NoMessage,
    PasswordsDoNotMatch,
    UsernameTaken,
}

pub fn render(urls: &impl Urls, message: Message) -> Markup {
    all::render(
        None,
        "Sign up",
        urls,
        match message {
            Message::NoMessage => None,
            Message::PasswordsDoNotMatch => Some(
                html! {"The passwords you typed do not match.  Please try again."},
            ),
            Message::UsernameTaken => Some(html! {
                "That email address is already claimed!  "
                "If you know the password, "
                a href={(urls.u("login"))} { "log in" }
                ".  If not, please use a different email address."
            }),
        },
        None,
        None,
        html! {
            p { "Welcome to Santa Circles!" }
            h2 { "Sign up" }
            p {
                "If this is the first time you have visited, sign up:"
            }
            form class="namevalue" action={(urls.u("register"))} method="post" {
                label for="username" { "Email" }
                input
                    type="text"
                    id="username"
                    name="username"
                    value=""
                    placeholder="E.g. name@example.com" {}
                label for="password" { "Password" }
                input
                    type="password"
                    id="password"
                    name="password"
                    value=""
                    placeholder="E.g. correct horse battery staple"
                    {}
                label for="password_repeat" { "Repeat password" }
                input
                    type="password"
                    id="password_repeat"
                    name="password_repeat"
                    value=""
                    placeholder="Repeat your password"
                    {}
                span {}
                div {
                    button type="submit" { "Sign up" }
                }
            }
            p.cookie-warning {
                "This site uses a single authentication "
                a href="https://en.wikipedia.org/wiki/HTTP_cookie" {
                    "cookie"
                }
                " to enable logging in. It is not used to track any "
                "information about you."
            }
            h2 { "Log in" }
            p { "If you have used this site before, "
                a class="emphasised" href={(urls.u("login"))} {
                    "Log in"
                }
                "."
            }
        },
    )
}
