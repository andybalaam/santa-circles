use maud::{html, Markup, PreEscaped, DOCTYPE};

use crate::urls::Urls;

pub fn render(
    pretitle: Option<Markup>,
    title: &str,
    urls: &impl Urls,
    error_notification: Option<Markup>,
    notification: Option<Markup>,
    username: Option<&str>,
    content: Markup,
) -> Markup {
    let error_notif = if let Some(error_notif) = error_notification {
        html! {header.notification.error { (error_notif) }}
    } else {
        html! {}
    };

    let notif = if let Some(notif) = notification {
        html! {header.notification { (notif) }}
    } else {
        html! {}
    };

    let user = if let Some(user_pretty_name) = username {
        html! {
            header class="user" {
                a href={(urls.u("circles"))} { "My Circles" }
                a href={(urls.u("account"))} { (PreEscaped("&#x1F385; ")) (user_pretty_name) }
            }
        }
    } else {
        html! {}
    };

    raw_render(
        pretitle.unwrap_or(html! {}),
        title,
        &urls.u("style_css"),
        &urls.u("contact"),
        &urls.u("privacy"),
        error_notif,
        notif,
        user,
        content,
    )
}

pub fn raw_render(
    pretitle: Markup,
    title: &str,
    style_url: &str,
    contact_url: &str,
    privacy_url: &str,
    error_notif: Markup,
    notif: Markup,
    user: Markup,
    content: Markup,
) -> Markup {
    html! {
        (DOCTYPE)
        html {
            head {
                meta charset="utf-8";
                meta
                    name="viewport"
                    content="width=device-width, initial-scale=1";
                title { (title) " - Santa Circles" }
                link href=(style_url) rel="stylesheet";
            }
            body {
                (error_notif)
                (notif)
                (user)
                (pretitle)
                h1 { (title) }
                (content)
                (notif)
                (error_notif)
                div class="footer-expand" {}
                footer {
                    p {
                        a href="https://gitlab.com/andybalaam/santa-circles" {
                            "Santa Circles"
                        }
                        " is Free Software, released under the AGPLv3. Find "
                        "out how to "
                        a href=(contact_url) {
                            "contact the server admins"
                        }
                        " or "
                        a href=(privacy_url) {
                            "read the privacy policy"
                        }
                        "."
                    }
                }
            }
        }
    }
}
