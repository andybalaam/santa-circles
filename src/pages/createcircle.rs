use std::collections::HashMap;

use actix_http::http;
use actix_web::{web, HttpRequest, HttpResponse};
use maud::Markup;
use serde::{Deserialize, Serialize};

use crate::credentials::Credentials;
use crate::data_holder::DataHolder;
use crate::draw_item::DrawItem;
use crate::draw_warning::DrawWarning;
use crate::form_pairs::form_pairs;
use crate::lsc_error::LscError;
use crate::pages::organisecircle;
use crate::urls::{BoundUrls, UrlPrefix, Urls};
use crate::usernamenick::{UsernameNick, UsernameNickHasWishlist};

pub async fn get(
    req: HttpRequest,
    url_prefix: UrlPrefix,
) -> Result<Markup, LscError> {
    let urls = url_prefix.bind(&req);
    Credentials::extract(&req, &urls)
        .await
        .map(|credentials| render(&credentials.username, &urls))
}

#[derive(Serialize)]
pub struct CircleApiRequest {
    name: String,
    draw_date: String,
    rules: String,
    members: Vec<UsernameNick>,
    organisers: Vec<UsernameNick>,
    request_draw: bool,
    // deterministic_draw exists on API, for debugging, but we don't need it
    draw: Vec<DrawItem>,
    disallowed_draw: Vec<DrawItem>,
}

impl CircleApiRequest {
    pub fn try_from_form(
        new_circle_form: web::Form<HashMap<String, String>>,
        urls: &impl Urls,
    ) -> Result<CircleApiRequest, LscError> {
        let members = form_pairs(
            &new_circle_form,
            "member-name-",
            "member-email-",
            &|nickname, username| UsernameNick { nickname, username },
        );
        let organisers = form_pairs(
            &new_circle_form,
            "organiser-name-",
            "organiser-email-",
            &|nickname, username| UsernameNick { nickname, username },
        );

        let cancel_draw = new_circle_form.get("cancel_draw").is_some();
        let draw = if cancel_draw {
            Vec::new()
        } else {
            draw_form(&new_circle_form, "draw")
        };
        let disallowed_draw = draw_form(&new_circle_form, "restriction");

        Ok(Self {
            name: new_circle_form
                .get("name")
                .ok_or(LscError::bad_input("'name' field is missing", urls))?
                .clone(),
            draw_date: new_circle_form
                .get("draw_date")
                .map(|s| s.clone())
                .unwrap_or(String::from("")),
            rules: new_circle_form
                .get("rules")
                .map(|s| s.clone())
                .unwrap_or(String::from("")),
            members,
            organisers,
            request_draw: new_circle_form.get("draw_now").is_some(),
            draw,
            disallowed_draw,
        })
    }
}

pub fn draw_form(
    form: &web::Form<HashMap<String, String>>,
    prefix: &str,
) -> Vec<DrawItem> {
    let prefix_1 = format!("{}-from-", prefix);
    let prefix_2 = format!("{}-to-", prefix);
    form_pairs(form, &prefix_1, &prefix_2, &|from_username, to_username| {
        DrawItem {
            from_username,
            to_username,
        }
    })
}

#[derive(Deserialize)]
pub struct CircleApiResponse {
    pub circleid: String,
}

pub async fn post(
    req: HttpRequest,
    url_prefix: UrlPrefix,
    data_holder: web::Data<DataHolder>,
    new_circle_form: web::Form<HashMap<String, String>>,
) -> Result<HttpResponse, LscError> {
    let urls = url_prefix.bind(&req);
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            do_create(&req, data_holder, new_circle_form, urls, credentials)
                .await
        }
        Err(e) => Err(e),
    }
}

pub async fn do_create(
    req: &HttpRequest,
    data_holder: web::Data<DataHolder>,
    new_circle_form: web::Form<HashMap<String, String>>,
    urls: BoundUrls<'_>,
    credentials: Credentials,
) -> Result<HttpResponse, LscError> {
    data_holder
        .api
        .post(
            &credentials,
            "/v1/circle",
            serde_json::to_value(CircleApiRequest::try_from_form(
                new_circle_form,
                &urls,
            )?)
            .unwrap(),
        )
        .await
        .map_err(|e| LscError::from_api_error(&req, &urls, e))
        .and_then(|res| {
            serde_json::from_value::<CircleApiResponse>(res)
                .map(|circle_info| {
                    HttpResponse::Found()
                        .header(
                            http::header::LOCATION,
                            urls.u2(
                                "organisecircle",
                                &[String::from(&circle_info.circleid)],
                            ),
                        )
                        .finish()
                        .into_body()
                })
                .map_err(|e| {
                    LscError::internal_error(
                        "Failed to parse API response while adding new \
                            circle",
                        &req,
                        &urls,
                        &format!(
                            "Failed to parse API response while \
                                adding new circle: {}",
                            e
                        ),
                    )
                })
        })
}

pub fn render(username: &str, urls: &impl Urls) -> Markup {
    let organisers = vec![UsernameNick {
        username: String::from(username),
        nickname: String::from(""),
    }];

    let members: Vec<UsernameNickHasWishlist> = Vec::new();
    let draw: Vec<DrawItem> = Vec::new();
    let disallowed_draw: Vec<DrawItem> = Vec::new();
    let warnings: Vec<DrawWarning> = Vec::new();

    organisecircle::render_impl(
        "Create a circle",
        "Save new circle",
        false,
        "createcircle",
        true,
        username,
        urls,
        "",
        "",
        "",
        "",
        &organisers,
        &members,
        &draw,
        &disallowed_draw,
        &warnings,
        false,
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn draw_form_processes_empty_form() {
        let hm: HashMap<_, _> = [].into();
        assert_eq!(draw_form(&web::Form(hm), "restriction"), vec![]);
    }

    #[test]
    fn draw_form_processes_restriction_form() {
        let hm: HashMap<_, _> = HashMap::from_iter(
            serde_json::json!(
                {
                  "member-name-2": "",
                  "member-email-4": " m4",
                  "member-email-1": " m1@example.com ",
                  "member-email-2": "m2@example.com",
                  "member-email-3": "m3",
                  "restriction-from-2": " m2@example.com",
                  "restriction-from-1": "m1@example.com ",
                  "name": "My Circle",
                  "organiser-email-1": "myuser ",
                  "member-name-3": "",
                  "organiser-name-1": "",
                  "member-name-4": "",
                  "restriction-to-2": " m3@example.com",
                  "member-name-1": "",
                  "restriction-to-1": "m2@example.com "
                }
            )
            .as_object()
            .unwrap()
            .iter()
            .map(|(k, v)| (String::from(k), String::from(v.as_str().unwrap()))),
        );
        assert_eq!(
            draw_form(&web::Form(hm), "restriction"),
            vec![
                DrawItem {
                    from_username: String::from("m1@example.com"),
                    to_username: String::from("m2@example.com")
                },
                DrawItem {
                    from_username: String::from("m2@example.com"),
                    to_username: String::from("m3@example.com")
                },
            ]
        );
    }

    #[test]
    fn draw_now_is_correctly_identified() {
        let with_draw_now = req(serde_json::json!({
          "name": "My Circle",
          "draw_now": "Draw+now!",
        }));
        let without_draw_now = req(serde_json::json!({
          "name": "My Circle",
          "save": "Save changes",
        }));

        assert!(with_draw_now.request_draw);
        assert!(!without_draw_now.request_draw);
    }

    #[test]
    fn cancel_draw_clears_draw() {
        let with_cancel_draw = req(serde_json::json!({
          "name": "My Circle",
          "cancel_draw": "Cancel+the+draw+now!",
          "draw-from-1": "m1",
          "draw-to-1": "m2",
        }));
        let without_cancel_draw = req(serde_json::json!({
          "name": "My Circle",
          "draw-from-1": "m1",
          "draw-to-1": "m2",
          "save": "Save changes",
        }));

        assert_eq!(with_cancel_draw.draw, Vec::new());
        assert_eq!(
            without_cancel_draw.draw,
            vec![DrawItem {
                from_username: String::from("m1"),
                to_username: String::from("m2")
            }]
        );
    }

    fn req(input: serde_json::Value) -> CircleApiRequest {
        let form = make_form(input);
        CircleApiRequest::try_from_form(form, &HashMap::new()).unwrap()
    }

    fn make_form(
        input: serde_json::Value,
    ) -> web::Form<HashMap<String, String>> {
        web::Form(HashMap::from_iter(input.as_object().unwrap().iter().map(
            |(k, v)| (String::from(k), String::from(v.as_str().unwrap())),
        )))
    }
}
