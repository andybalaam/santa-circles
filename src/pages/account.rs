use actix_web::{web, HttpRequest, HttpResponse};
use log::error;
use maud::{html, Markup};
use serde::Deserialize;

use crate::credentials::Credentials;
use crate::data_holder::DataHolder;
use crate::lsc_error::LscError;
use crate::pages::all;
use crate::urls::{UrlPrefix, Urls};

#[derive(Deserialize)]
pub struct NewPassword {
    password: String,
    password_repeat: String,
}

///! Change password
pub async fn post(
    req: HttpRequest,
    new_password: web::Form<NewPassword>,
    url_prefix: UrlPrefix,
    data_holder: web::Data<DataHolder>,
) -> Result<HttpResponse, LscError> {
    let urls = url_prefix.bind(&req);
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            change_password_page(
                &new_password,
                &req,
                &urls,
                &data_holder,
                &credentials,
            )
            .await
        }
        Err(e) => Err(e),
    }
}

async fn change_password_page(
    new_password: &NewPassword,
    req: &HttpRequest,
    urls: &impl Urls,
    data_holder: &web::Data<DataHolder>,
    credentials: &Credentials,
) -> Result<HttpResponse, LscError> {
    if new_password.password != new_password.password_repeat {
        fail(
            req,
            urls,
            data_holder,
            credentials,
            Some(html! { "Passwords didn't match!" }),
        )
        .await
    } else if new_password.password == "" {
        fail(
            req,
            urls,
            data_holder,
            credentials,
            Some(html! {"Passwords can't be empty!"}),
        )
        .await
    } else {
        match change_password(
            new_password,
            req,
            urls,
            &data_holder,
            credentials,
        )
        .await
        {
            Ok(_) => render_account(
                req,
                urls,
                data_holder,
                credentials,
                None,
                Some(html! { "Password changed." }),
            )
            .await
            .map_err(|_| {
                LscError::internal_error(
                    "Failed to convert post user page to string",
                    req,
                    urls,
                    "Failed to convert post user page to string",
                )
            })
            .map(|html| HttpResponse::Ok().body(html.into_string())),
            Err(e) => Err(e),
        }
    }
}

async fn change_password(
    new_password: &NewPassword,
    req: &HttpRequest,
    urls: &impl Urls,
    data_holder: &web::Data<DataHolder>,
    credentials: &Credentials,
) -> Result<(), LscError> {
    let url = format!("/v1/user/{}", credentials.username);
    data_holder
        .api
        .put(
            &credentials,
            &url,
            serde_json::json!({"password": &new_password.password}),
        )
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .map(|_| ())
}

async fn fail(
    req: &HttpRequest,
    urls: &impl Urls,
    data_holder: &web::Data<DataHolder>,
    credentials: &Credentials,
    error_notification: Option<Markup>,
) -> Result<HttpResponse, LscError> {
    render_account(
        req,
        urls,
        data_holder,
        credentials,
        error_notification,
        None,
    )
    .await
    .map_err(|_| {
        LscError::internal_error(
            "Failed to convert user page to string",
            req,
            urls,
            "Failed to convert user page to string",
        )
    })
    .map(|html| HttpResponse::BadRequest().body(html.into_string()))
}

#[derive(Deserialize)]
pub struct Query {
    error: Option<String>,
}

pub async fn get(
    req: HttpRequest,
    url_prefix: UrlPrefix,
    data_holder: web::Data<DataHolder>,
    query: web::Query<Query>,
) -> Result<Markup, LscError> {
    let urls = url_prefix.bind(&req);
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            let error_notification = match query.error.as_deref() {
                Some("sure_required") => {
                    Some(html! { r#"You must choose "I am sure"."# })
                }
                Some(m) => {
                    error!("Unknown error message: {}", m);
                    None
                }
                None => None,
            };
            render_account(
                &req,
                &urls,
                &data_holder,
                &credentials,
                error_notification,
                None,
            )
            .await
        }
        Err(e) => Err(e),
    }
}

async fn render_account(
    req: &HttpRequest,
    urls: &impl Urls,
    data_holder: &web::Data<DataHolder>,
    credentials: &Credentials,
    error_notification: Option<Markup>,
    notification: Option<Markup>,
) -> Result<Markup, LscError> {
    fetch_account_info(req, urls, &data_holder, credentials)
        .await
        .map(|account_info| {
            render(
                &credentials.username,
                account_info,
                urls,
                error_notification,
                notification,
            )
        })
}

pub struct Info {}

impl Info {
    pub fn new() -> Self {
        Self {}
    }
}

async fn fetch_account_info(
    _req: &HttpRequest,
    _urls: &impl Urls,
    _data_holder: &DataHolder,
    _credentials: &Credentials,
) -> Result<Info, LscError> {
    // If we store more info about the user, it will go here.
    Ok(Info::new())
}

pub fn render(
    username: &str,
    _info: Info,
    urls: &impl Urls,
    error_notification: Option<Markup>,
    notification: Option<Markup>,
) -> Markup {
    all::render(
        None,
        "My Account",
        urls,
        error_notification,
        notification,
        Some(username),
        html! {
            h2 { "Log out" }
            form action=(urls.u("login")) method="post" {
                button type="submit" { "Log out" }
            }
            h2 { "Change Password" }
            form class="namevalue" action=(urls.u("account")) method="post" {
                label for="password" { "Password" }
                input id="password"
                    name="password"
                    placeholder="Enter a new password"
                    type="password"
                    {}
                label for="password" { "Password (repeat)" }
                input
                    id="password_repeat"
                    name="password_repeat"
                    placeholder="Repeat new password"
                    type="password"
                    {}
                span {}
                div { button type="submit" { "Change password" } }
            }
            h2 { "Delete Account" }
            p { "Delete account " (username) "?" }
            form action=(urls.u("deleteaccount")) method="post" {
                input
                    type="submit"
                    name="delete-user"
                    class="danger"
                    value="Delete myself and all my circles"
                    {}
                input type="checkbox" name="sure" id="sure" {}
                label for="sure" { "I am sure" }
            }
        },
    )
}
