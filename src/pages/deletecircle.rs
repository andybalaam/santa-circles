use actix_web::http;
use actix_web::{web, HttpRequest, HttpResponse};
use serde::Deserialize;
use url::Url;

use crate::credentials::Credentials;
use crate::data_holder::DataHolder;
use crate::lsc_error::LscError;
use crate::urls::{UrlPrefix, Urls};

#[derive(Deserialize)]
pub struct Path {
    circleid: String,
}

#[derive(Deserialize)]
pub struct DeleteCircle {
    pub delete_circle_sure: Option<String>,
}

pub async fn post(
    req: HttpRequest,
    url_prefix: UrlPrefix,
    path: web::Path<Path>,
    delete_circle: web::Form<DeleteCircle>,
    data_holder: web::Data<DataHolder>,
) -> Result<HttpResponse, LscError> {
    let urls = url_prefix.bind(&req);
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => match &delete_circle.delete_circle_sure {
            Some(on) => {
                if on == "on" {
                    delete_circle_and_redirect(
                        &req,
                        &urls,
                        &path,
                        &data_holder,
                        &credentials,
                    )
                    .await
                } else {
                    Ok(redirect_to_circle(&urls, &path).await)
                }
            }
            _ => Ok(redirect_to_circle(&urls, &path).await),
        },
        Err(e) => Err(e),
    }
}

async fn delete_circle_and_redirect(
    req: &HttpRequest,
    urls: &impl Urls,
    path: &Path,
    data_holder: &DataHolder,
    credentials: &Credentials,
) -> Result<HttpResponse, LscError> {
    let url = format!("/v1/circle/{}", path.circleid);
    data_holder
        .api
        .delete(&credentials, &url, None)
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .map(|_| {
            let mut re_url = urls.url_for_static("circles").unwrap();
            re_url.set_query(Some("message=circle_deleted"));
            HttpResponse::Found()
                .header(http::header::LOCATION, re_url.to_string())
                .finish()
                .into_body()
        })
}

async fn redirect_to_circle(urls: &impl Urls, path: &Path) -> HttpResponse {
    let mut redir_url =
        Url::parse(&urls.u2("organisecircle", [&path.circleid])).unwrap();
    redir_url.set_query(Some("error=sure_required"));

    HttpResponse::Found()
        .header(http::header::LOCATION, redir_url.to_string())
        .finish()
        .into_body()
}
