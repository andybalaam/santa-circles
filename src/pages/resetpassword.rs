use actix_http::http;
use actix_web::{web, HttpRequest, HttpResponse};
use log::error;
use maud::{html, Markup};
use serde::{Deserialize, Serialize};

use crate::data_holder::DataHolder;
use crate::pages::all;
use crate::urls::{UrlPrefix, Urls};

#[derive(Deserialize)]
pub struct ResetPassword {
    username: String,
    password_reset_code: Option<String>,
    password: Option<String>,
    password_repeat: Option<String>,
}

// No need to send anything to the reset API - we just provide the
// username in the URL.
#[derive(Serialize)]
struct ResetInfo {}

#[derive(Serialize)]
struct DoResetInfo {
    password_reset_code: String,
    new_password: String,
}

#[derive(Deserialize)]
pub enum Message {
    NoMessage,
    PasswordsDoNotMatch,
    ResetRequestFailed,
    DoResetRequestFailed,
}

pub async fn post(
    req: HttpRequest,
    url_prefix: UrlPrefix,
    data_holder: web::Data<DataHolder>,
    reset_password: web::Form<ResetPassword>,
) -> HttpResponse {
    let urls = url_prefix.bind(&req);

    match (
        &reset_password.password_reset_code,
        &reset_password.password,
        &reset_password.password_repeat,
    ) {
        (None, None, None) => {
            let res = data_holder
                .api
                .post_no_credentials(
                    &format!("/v1/reset/{}", reset_password.username),
                    serde_json::to_value(&ResetInfo {})
                        .expect("Unable to serialize ResetInfo"),
                )
                .await;
            match res {
                Ok(_) => HttpResponse::Ok()
                    .body(
                        render(
                            &reset_password.username,
                            &urls,
                            Message::NoMessage,
                        )
                        .into_string(),
                    )
                    .into(),
                Err(e) => {
                    error!("API request to reset failed with error: {:?}", e);
                    HttpResponse::BadRequest()
                        .body(
                            render(
                                &reset_password.username,
                                &urls,
                                Message::ResetRequestFailed,
                            )
                            .into_string(),
                        )
                        .into()
                }
            }
        }
        (Some(password_reset_code), Some(password), Some(password_repeat)) => {
            if password != password_repeat {
                return HttpResponse::BadRequest()
                    .body(
                        render(
                            &reset_password.username,
                            &urls,
                            Message::PasswordsDoNotMatch,
                        )
                        .into_string(),
                    )
                    .into();
            }

            let res = data_holder
                .api
                .post_no_credentials(
                    &format!("/v1/do_reset/{}", reset_password.username),
                    serde_json::to_value(DoResetInfo {
                        password_reset_code: password_reset_code.clone(),
                        new_password: password.clone(),
                    })
                    .expect("Unable to serialize DoResetInfo"),
                )
                .await;
            match res {
                Ok(_) => {
                    let mut url = urls.url_for_static("login").unwrap();
                    url.set_query(Some("message=password_reset"));
                    HttpResponse::Found()
                        .header(http::header::LOCATION, url.to_string())
                        .finish()
                }
                Err(e) => {
                    error!(
                        "API request to do_reset failed with error: {:?}",
                        e
                    );
                    HttpResponse::BadRequest()
                        .body(
                            render(
                                &reset_password.username,
                                &urls,
                                Message::DoResetRequestFailed,
                            )
                            .into_string(),
                        )
                        .into()
                }
            }
        }
        _ => {
            error!("Partial resetpassword request unexpectedly received!");
            HttpResponse::InternalServerError()
                .body(
                    render(
                        &reset_password.username,
                        &urls,
                        Message::DoResetRequestFailed,
                    )
                    .into_string(),
                )
                .into()
        }
    }
}

pub fn render(username: &str, urls: &impl Urls, message: Message) -> Markup {
    let (notification, error_notification) = match message {
        Message::NoMessage => (
            Some(html! {
                "Your password reset code has been sent to "
                (username)
            }),
            None,
        ),
        Message::PasswordsDoNotMatch => (
            None,
            Some(html! {
                "The passwords you typed do not match.  Please try again."
            }),
        ),
        Message::ResetRequestFailed => (
            None,
            Some(html! {
                "Failed to send password reset email.  Please try again."
            }),
        ),
        Message::DoResetRequestFailed => (
            None,
            Some(html! {"Failed to reset password.  Please try again."}),
        ),
    };

    all::render(
        Some(html! { header { a href=(urls.u("login")) { "Login" } } }),
        "Reset password",
        urls,
        error_notification,
        notification,
        None,
        html! {
            p { "Enter your code and new password to reset your password:" }
            form
                class="namevalue"
                action={(urls.u("resetpassword"))}
                method="post"
            {
                label for="username" { "Email" }
                input
                    type="text"
                    id="username"
                    name="username"
                    value={(username)}
                    placeholder="E.g. name@example.com" {}
                label
                    for="password_reset_code"
                {
                    "Password reset code (from email)"
                }
                input
                    type="password"
                    id="password_reset_code"
                    name="password_reset_code"
                    value=""
                    placeholder="E.g. 4gsdf_3f" {}
                label for="password" { "New password" }
                input
                    type="password"
                    id="password"
                    name="password"
                    value=""
                    placeholder="E.g. correct horse battery staple"
                    {}
                label for="password_repeat" { "Repeat password" }
                input
                    type="password"
                    id="password_repeat"
                    name="password_repeat"
                    value=""
                    placeholder="Repeat your password"
                    {}
                span {}
                div {
                    button type="submit" { "Reset password" }
                }
            }
        },
    )
}
