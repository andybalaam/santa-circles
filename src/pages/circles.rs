use actix_web::{web, HttpRequest};
use log::error;
use maud::{html, Markup};
use serde::Deserialize;

use crate::credentials::Credentials;
use crate::data_holder::DataHolder;
use crate::lsc_error::LscError;
use crate::pages::all;
use crate::urls::{UrlPrefix, Urls};

#[derive(Deserialize)]
pub struct Query {
    message: Option<String>,
}

pub async fn get(
    req: HttpRequest,
    url_prefix: UrlPrefix,
    data_holder: web::Data<DataHolder>,
    query: web::Query<Query>,
) -> Result<Markup, LscError> {
    let urls = url_prefix.bind(&req);
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            let notification = match query.message.as_deref() {
                Some("circle_deleted") => Some(html! { "Circle deleted." }),
                Some(m) => {
                    error!("Unknown message: {}", m);
                    None
                }
                None => None,
            };
            fetch_circles_info(&req, &urls, &data_holder, &credentials)
                .await
                .map(|circles_info| {
                    render(
                        &credentials.username,
                        circles_info,
                        &urls,
                        notification,
                    )
                })
        }
        Err(e) => Err(e),
    }
}

#[derive(Debug, Deserialize)]
pub struct CircleRef {
    pub name: String,
    pub circleid: String,
}

#[derive(Debug, Deserialize)]
pub struct Info {
    pub member_circles: Vec<CircleRef>,
    pub organiser_circles: Vec<CircleRef>,
}

async fn fetch_circles_info(
    req: &HttpRequest,
    urls: &impl Urls,
    data_holder: &DataHolder,
    credentials: &Credentials,
) -> Result<Info, LscError> {
    let url = "/v1/circle";
    data_holder
        .api
        .get(&credentials, url)
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .and_then(|circles_json| {
            serde_json::from_value(circles_json).map_err(|e| {
                LscError::internal_error(
                    "Failed to parse Circles",
                    req,
                    urls,
                    &format!("Failed to parse Circles: {}", e),
                )
            })
        })
}

fn circle_name(name: &str) -> &str {
    if name.len() > 0 {
        name
    } else {
        "(unnamed)"
    }
}

pub fn render(
    username: &str,
    info: Info,
    urls: &impl Urls,
    notification: Option<Markup>,
) -> Markup {
    let member_of = if info.member_circles.len() > 0 {
        html! {
            h2 { "Circles I am in" }
            ul {
                @for c in info.member_circles {
                    li {
                        a href=(&urls.u2("circle", vec![c.circleid])) {
                            (circle_name(&c.name))
                        }
                    }
                }
            }
        }
    } else {
        html! {
            h2 { "Circles I am in" }
            p { "(You are not in any circles yet!)" }
        }
    };

    let organiser_of = if info.organiser_circles.len() > 0 {
        html! {
            h2 { "Circles I am organising" }
            ul {
                @for c in info.organiser_circles {
                    li {
                        a href=(&urls.u2("organisecircle", vec![c.circleid])) {
                            (circle_name(&c.name))
                        }
                    }
                }
            }
        }
    } else {
        html! {}
    };

    all::render(
        None,
        "My Circles",
        urls,
        None,
        notification,
        Some(username),
        html! {
            (member_of)
            (organiser_of)
            h2 { "Create a new circle" }
            form action=(urls.u("createcircle")) method="get" {
                button type="submit" { "Create a circle" }
            }
        },
    )
}
