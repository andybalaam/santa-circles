use actix_web::{web, HttpRequest};
use log::error;
use maud::{html, Markup};
use serde::{Deserialize, Serialize};

use crate::actix_session_remember::Session;
use crate::credentials::Credentials;
use crate::data_holder::DataHolder;
use crate::lsc_error::LscError;
use crate::pages::all;
use crate::urls::{UrlPrefix, Urls};

#[derive(Serialize)]
struct TokenToDelete {
    token: String,
}

#[derive(Deserialize)]
pub struct QueryInfo {
    pub message: Option<String>,
    pub error: Option<String>,
}

pub async fn get(
    web::Query(query_info): web::Query<QueryInfo>,
    req: HttpRequest,
    url_prefix: UrlPrefix,
) -> Markup {
    let urls = url_prefix.bind(&req);
    let error_notification = match query_info.error.as_deref() {
        Some("login_failed") => {
            Some(html! { "Login failed or timed out - please try again" })
        }
        Some(m) => {
            error!("Unknown error message: {}", m);
            None
        }
        None => None,
    };
    let notification = match query_info.message.as_deref() {
        Some("logged_out") => Some(html! { "Logged out." }),
        Some("account_deleted") => {
            Some(html! { "Account deleted. Please log in." })
        }
        Some("password_reset") => {
            Some(html! { "Password was successfully changed.  Please log in." })
        }
        Some(m) => {
            error!("Unknown message: {}", m);
            None
        }
        None => None,
    };
    render(&urls, error_notification, notification)
}

/// The user clicked Log out.
pub async fn post(
    req: HttpRequest,
    url_prefix: UrlPrefix,
    session: Session,
    data_holder: web::Data<DataHolder>,
) -> Result<Markup, LscError> {
    let urls = url_prefix.bind(&req);
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            session.purge();
            delete_token(&req, &urls, &data_holder, &credentials).await?;
            Ok(render(&urls, None, Some(html! { "Logged out." })))
        }
        Err(e) => Err(e),
    }
}

async fn delete_token(
    req: &HttpRequest,
    urls: &impl Urls,
    data_holder: &DataHolder,
    credentials: &Credentials,
) -> Result<(), LscError> {
    let url = format!("/v1/login/{}", &credentials.username);
    data_holder
        .api
        .delete(
            &credentials,
            &url,
            Some(
                serde_json::to_value(TokenToDelete {
                    token: String::from(&credentials.token),
                })
                .expect("Unable to serialize TokenToDelete"),
            ),
        )
        .await
        .map(|_| ())
        .map_err(|e| LscError::from_api_error(req, urls, e))
}

pub fn render(
    urls: &impl Urls,
    error_notification: Option<Markup>,
    notification: Option<Markup>,
) -> Markup {
    all::render(
        None,
        "Log in",
        urls,
        error_notification,
        notification,
        None,
        html! {
            p class="intro" {
                r#"Welcome to Santa Circles, helping organise "secret santa""#
                "-style gift-giving games."
            }
            p class="intro" { "To start a game:" }
            ul {
                li {
                    "Register and create a circle, adding your friends "
                    "as members of the circle."
                }
                li {
                    r#"Click "Draw now!" and everyone will be secretly "#
                    "assigned another person to give a gift to."
                }
            }
            p class="intro" { "To join a game:" }
            ul {
                li {
                    "Register (with the same email address the circle "
                    "organiser used to add you)."
                }
                li {
                    "Open the circle. You will be able to see the person "
                    "you are giving to, and anything they have added to "
                    "their wish list."
                }
                li {
                    "You won't be able to see who is giving a gift to you "
                    "- it's a secret!"
                }
            }
            h2 { "Sign up" }
            p {
                "If this is the first time you have visited, "
                a class="emphasised" href={(urls.u("signup"))} {
                    "Sign up for free"
                }
                "."
            }
            h2 { "Log in" }
            p { "If you have used this site before, log in:" }
            form class="namevalue" action={(urls.u("root"))} method="post" {
                label for="username" { "Email" }
                input
                    type="text"
                    id="username"
                    name="username"
                    value=""
                    placeholder="E.g. name@example.com" {}
                label for="password" { "Password" }
                input
                    type="password"
                    id="password"
                    name="password"
                    value=""
                    placeholder="E.g. correct horse battery staple"
                    {}
                span {}
                span.remember {
                    a
                        class="forgot_password"
                        href={(urls.u("forgotpassword"))}
                        { "Forgot password?" }
                    input
                        type="checkbox"
                        id="remember_me"
                        name="remember_me"
                        {}
                    label for="remember_me" { "Remember me" }
                }
                span {}
                div {
                    button type="submit" { "Log in" }
                }
            }
            p.cookie-warning {
                "This site uses a single authentication "
                a href="https://en.wikipedia.org/wiki/HTTP_cookie" {
                    "cookie"
                }
                " to enable logging in. It is not used to track any "
                "information about you."
            }
        },
    )
}
