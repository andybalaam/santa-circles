use actix_web::{web, HttpRequest};
use maud::{html, Markup, PreEscaped};

use crate::data_holder::DataHolder;
use crate::lsc_error::LscError;
use crate::pages::all;
use crate::urls::{UrlPrefix, Urls};

pub async fn get(
    req: HttpRequest,
    url_prefix: UrlPrefix,
    data_holder: web::Data<DataHolder>,
) -> Result<Markup, LscError> {
    // Note: no credentials needed for this page.
    let urls = url_prefix.bind(&req);
    data_holder
        .api
        .get_no_credentials("/v1/privacy")
        .await
        .map_err(|e| LscError::from_api_error(&req, &urls, e))
        .and_then(|privacy_json| {
            privacy_json
                .as_str()
                .ok_or_else(|| {
                    LscError::internal_error(
                        "Privacy response was not a string.",
                        &req,
                        &urls,
                        &format!(
                            "Privacy response was not a string: {}.",
                            privacy_json,
                        ),
                    )
                })
                .map(|privacy_html| render(&urls, privacy_html))
        })
}

pub fn render(urls: &impl Urls, privacy_html: &str) -> Markup {
    all::render(
        Some(html! {
            header class="user" {
                a href=(urls.u("circles")) { "My Circles" }
            }
        }),
        "Privacy",
        urls,
        None,
        None,
        None,
        html! {
            div class="info-div" {
            (PreEscaped(privacy_html))
            }
        },
    )
}
