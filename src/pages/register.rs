use actix_web::{web, HttpRequest};
use maud::{html, Markup};
use serde::{Deserialize, Serialize};

use crate::api::ApiError;
use crate::data_holder::DataHolder;
use crate::lsc_error::LscError;
use crate::pages::{all, register, signup};
use crate::string_or_bool::StringOrBool;
use crate::urls::{UrlPrefix, Urls};

#[derive(Deserialize)]
pub struct CreatedUser {
    pub username: String,
    pub registration_code: StringOrBool,
}

#[derive(Deserialize)]
pub struct SignupCredentials {
    pub username: String,
    pub password: String,
    pub password_repeat: String,
}

#[derive(Serialize)]
pub struct NewUser {
    pub username: String,
    pub password: String,
}

impl From<&SignupCredentials> for NewUser {
    fn from(signup_credentials: &SignupCredentials) -> NewUser {
        NewUser {
            username: String::from(signup_credentials.username.trim()),
            password: String::from(&signup_credentials.password),
        }
    }
}

pub async fn post(
    req: HttpRequest,
    url_prefix: UrlPrefix,
    data_holder: web::Data<DataHolder>,
    signup_credentials: web::Form<SignupCredentials>,
) -> Result<Markup, LscError> {
    let urls = url_prefix.bind(&req);
    if signup_credentials.password == signup_credentials.password_repeat {
        create_user(&data_holder, &signup_credentials)
            .await
            .map_or_else(
                |e| process_signup_error(e, &req, &urls),
                |user_json| {
                    parse_created_user(&req, &urls, user_json).map(
                        |created_user| {
                            register::render(
                                &created_user.username,
                                &created_user.registration_code,
                                &urls,
                                register::Message::NoMessage,
                            )
                        },
                    )
                },
            )
    } else {
        Ok(signup::render(&urls, signup::Message::PasswordsDoNotMatch))
    }
}

async fn create_user(
    data_holder: &DataHolder,
    signup_credentials: &SignupCredentials,
) -> Result<serde_json::Value, ApiError> {
    data_holder
        .api
        .post_no_credentials(
            "/v1/newuser",
            serde_json::to_value(NewUser::from(signup_credentials))
                .expect("Unable to serialize UpdatedList"),
        )
        .await
}

fn parse_created_user(
    req: &HttpRequest,
    urls: &impl Urls,
    user_json: serde_json::Value,
) -> Result<CreatedUser, LscError> {
    serde_json::from_value(user_json).map_err(|_| {
        LscError::internal_error(
            "Failed to parse CreatedUser",
            &req,
            urls,
            "Failed to parse CreatedUser",
        )
    })
}

fn process_signup_error(
    e: ApiError,
    req: &HttpRequest,
    urls: &impl Urls,
) -> Result<Markup, LscError> {
    match e {
        ApiError::UserAlreadyExists => {
            Ok(signup::render(urls, signup::Message::UsernameTaken))
        }
        _ => Err(LscError::from_api_error(&req, urls, e)),
    }
}

#[derive(Deserialize)]
pub enum Message {
    NoMessage,
    MustRegister,
    RegistrationFailed,
}

pub fn render(
    username: &str,
    registration_code: &StringOrBool,
    urls: &impl Urls,
    message: Message,
) -> Markup {
    let notification = match registration_code {
        StringOrBool::S(s) => html! {
            "Your registration code is "
            span class="reg-code" { (s) }
            ".  Please enter it."
        },
        StringOrBool::B(_) => {
            html! {
                "Your registration code has been emailed to " (username)
                    ". Please enter it."
            }
        }
    };

    all::render(
        None,
        "Register",
        urls,
        match message {
            Message::MustRegister => {
                Some(html! { "You must register before you can log in." })
            }
            Message::RegistrationFailed => {
                Some(html! { "Registration failed.  Please try again." })
            }
            _ => None,
        },
        Some(notification),
        None,
        html!(
            h2 {"Enter registration code"}
            form
                class="namevalue"
                action={(urls.u("afterregister"))}
                method="post"
            {
                input
                    type="hidden"
                    id="username"
                    name="username"
                    value={ (username) }
                    {}
                label for="registration_code"{ "Registration code" }
                input
                    id="registration_code"
                    name="registration_code"
                    placeholder="Enter the code e.g. dF-9H"
                    {}
                @if let StringOrBool::S(reg) = registration_code {
                    input
                        id="real_registration_code"
                        name="real_registration_code"
                        type="hidden"
                        value={(reg)}
                        {}
                }
                span {}
                div { button type="submit" {"Register"} }
            }
        ),
    )
}
