use std::collections::HashMap;

use actix_http::http;
use actix_web::{web, HttpRequest, HttpResponse};
use log::error;
use maud::{html, Markup, PreEscaped};
use serde::Deserialize;

use crate::credentials::Credentials;
use crate::data_holder::DataHolder;
use crate::draw_item::DrawItem;
use crate::draw_warning::{DrawWarning, DrawWarningSeverity};
use crate::form_pairs::form_pairs;
use crate::lsc_error::LscError;
use crate::pages::all;
use crate::pages::createcircle::{
    draw_form, CircleApiRequest, CircleApiResponse,
};
use crate::urls::{BoundUrls, UrlPrefix, Urls};
use crate::usernamenick::{UsernameNick, UsernameNickHasWishlist};

#[derive(Deserialize)]
pub struct Path {
    circleid: String,
}

#[derive(Deserialize)]
pub struct Query {
    error: Option<String>,
}

pub async fn get(
    req: HttpRequest,
    url_prefix: UrlPrefix,
    path: web::Path<Path>,
    data_holder: web::Data<DataHolder>,
    query: web::Query<Query>,
) -> Result<Markup, LscError> {
    let urls = url_prefix.bind(&req);
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            let must_be_sure = match query.error.as_deref() {
                Some("sure_required") => true,
                Some(e) => {
                    error!("Unknown error: {}", e);
                    false
                }
                None => false,
            };
            fetch_circle_info(
                &path.circleid,
                &req,
                &urls,
                &data_holder,
                &credentials,
            )
            .await
            .map(|circle_info| {
                render(
                    &credentials.username,
                    &urls,
                    &circle_info.name,
                    &circle_info.circleid,
                    &circle_info.draw_date,
                    &circle_info.rules,
                    &circle_info.organisers,
                    &circle_info.members,
                    &circle_info.draw,
                    &circle_info.disallowed_draw,
                    &circle_info.draw_warnings,
                    must_be_sure,
                )
            })
        }
        Err(e) => Err(e),
    }
}

pub async fn post(
    req: HttpRequest,
    url_prefix: UrlPrefix,
    path: web::Path<Path>,
    data_holder: web::Data<DataHolder>,
    update_circle_form: web::Form<HashMap<String, String>>,
) -> Result<HttpResponse, LscError> {
    let urls = url_prefix.bind(&req);
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            if update_circle_form.contains_key("copy_circle") {
                let circle_info = OrganiserCircleInfo::try_from_form(
                    update_circle_form,
                    &urls,
                )?;
                Ok(HttpResponse::Ok().body(
                    render_impl(
                        "Create a copy of a circle",
                        "Save new circle",
                        false,
                        "createcircle",
                        true,
                        &credentials.username,
                        &urls,
                        &format!("Copy of {}", circle_info.name),
                        "", // Don't copy circle id
                        &circle_info.draw_date,
                        &circle_info.rules,
                        &circle_info.organisers,
                        &circle_info.members,
                        &Vec::new(), // Don't copy the draw
                        &circle_info.disallowed_draw,
                        &Vec::new(), // Don't copy draw warnings
                        false,
                    )
                    .into_string(),
                ))
            } else {
                do_update(
                    &req,
                    path,
                    data_holder,
                    update_circle_form,
                    urls,
                    credentials,
                )
                .await
            }
        }
        Err(e) => Err(e),
    }
}

async fn do_update(
    req: &HttpRequest,
    path: web::Path<Path>,
    data_holder: web::Data<DataHolder>,
    update_circle_form: web::Form<HashMap<String, String>>,
    urls: BoundUrls<'_>,
    credentials: Credentials,
) -> Result<HttpResponse, LscError> {
    data_holder
        .api
        .put(
            &credentials,
            &format!("/v1/circle/{}", path.circleid),
            serde_json::to_value(CircleApiRequest::try_from_form(
                update_circle_form,
                &urls,
            )?)
            .unwrap(),
        )
        .await
        .map_err(|e| LscError::from_api_error(&req, &urls, e))
        .and_then(|res| {
            serde_json::from_value::<CircleApiResponse>(res)
                .map(|circle_info| {
                    // Redirect to ourself, which is lazy - we could just
                    // render here, if we fetched from the API
                    HttpResponse::Found()
                        .header(
                            http::header::LOCATION,
                            urls.u2(
                                "organisecircle",
                                &[String::from(&circle_info.circleid)],
                            ),
                        )
                        .finish()
                        .into_body()
                })
                .map_err(|e| {
                    LscError::internal_error(
                        "Failed to parse API response while updating \
                            circle",
                        &req,
                        &urls,
                        &format!(
                            "Failed to parse API response while updating \
                                circle: {}",
                            e
                        ),
                    )
                })
        })
}

#[derive(Debug, Deserialize)]
pub struct OrganiserCircleInfo {
    pub circleid: String,
    pub name: String,
    pub draw_date: String,
    pub rules: String,
    pub organisers: Vec<UsernameNick>,
    pub members: Vec<UsernameNickHasWishlist>,
    pub draw: Vec<DrawItem>,
    pub disallowed_draw: Vec<DrawItem>,
    pub draw_warnings: Vec<DrawWarning>,
}

impl OrganiserCircleInfo {
    pub fn try_from_form(
        organise_circle_form: web::Form<HashMap<String, String>>,
        urls: &impl Urls,
    ) -> Result<Self, LscError> {
        let members = form_pairs(
            &organise_circle_form,
            "member-name-",
            "member-email-",
            &|nickname, username| UsernameNickHasWishlist {
                nickname,
                username,
                wishlist_text: None,
                wishlist_links: None,
            },
        );
        let organisers = form_pairs(
            &organise_circle_form,
            "organiser-name-",
            "organiser-email-",
            &|nickname, username| UsernameNick { nickname, username },
        );

        let cancel_draw = organise_circle_form.get("cancel_draw").is_some();
        let draw = if cancel_draw {
            Vec::new()
        } else {
            draw_form(&organise_circle_form, "draw")
        };
        let disallowed_draw = draw_form(&organise_circle_form, "restriction");

        Ok(Self {
            circleid: String::from(""),
            name: organise_circle_form
                .get("name")
                .ok_or(LscError::bad_input("'name' field is missing", urls))?
                .clone(),
            draw_date: organise_circle_form
                .get("draw_date")
                .map(|s| s.clone())
                .unwrap_or(String::from("")),
            rules: organise_circle_form
                .get("rules")
                .map(|s| s.clone())
                .unwrap_or(String::from("")),
            members,
            organisers,
            draw,
            disallowed_draw,
            draw_warnings: Vec::new(),
        })
    }
}

pub async fn fetch_circle_info(
    circleid: &str,
    req: &HttpRequest,
    urls: &impl Urls,
    data_holder: &DataHolder,
    credentials: &Credentials,
) -> Result<OrganiserCircleInfo, LscError> {
    let url = format!("/v1/circle/{}?view=organise", circleid);
    data_holder
        .api
        .get(&credentials, &url)
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .and_then(|circle_json| {
            serde_json::from_value(circle_json).map_err(|e| {
                LscError::internal_error(
                    "Failed to parse Circle",
                    req,
                    urls,
                    &format!("Failed to parse Circle: {}", e),
                )
            })
        })
}

pub fn render(
    username: &str,
    urls: &impl Urls,
    circle_name: &str,
    circleid: &str,
    draw_date: &str,
    rules: &str,
    organisers: &Vec<UsernameNick>,
    members: &Vec<UsernameNickHasWishlist>,
    draw: &Vec<DrawItem>,
    disallowed_draw: &Vec<DrawItem>,
    warnings: &Vec<DrawWarning>,
    must_be_sure: bool,
) -> Markup {
    render_impl(
        "Organise circle",
        "Save changes",
        true,
        "organisecircle",
        false,
        username,
        urls,
        circle_name,
        circleid,
        draw_date,
        rules,
        organisers,
        members,
        draw,
        disallowed_draw,
        warnings,
        must_be_sure,
    )
}

pub fn render_impl(
    title: &str,
    submit_text: &str,
    circle_exists: bool,
    target_page: &str,
    show_reassurance: bool,
    username: &str,
    urls: &impl Urls,
    circle_name: &str,
    circleid: &str,
    draw_date: &str,
    rules: &str,
    organisers: &Vec<UsernameNick>,
    members: &Vec<UsernameNickHasWishlist>,
    draw: &Vec<DrawItem>,
    disallowed_draw: &Vec<DrawItem>,
    warnings: &Vec<DrawWarning>,
    must_be_sure: bool,
) -> Markup {
    let rules_placeholder =
        PreEscaped("E.g. &pound;10 limit, second-hand only.");

    let plain_members = members
        .iter()
        .map(|m| UsernameNick {
            username: m.username.clone(),
            nickname: m.nickname.clone(),
        })
        .collect();

    let draw_section = if draw.is_empty() {
        render_restrictions(&plain_members, disallowed_draw, submit_text)
    } else {
        render_draw(&plain_members, draw, disallowed_draw, submit_text)
    };

    let error_notification = if must_be_sure {
        Some(html! {
            r#"You must tick the "I am sure" box to delete a circle."#
        })
    } else {
        None
    };

    all::render(
        render_warnings(&plain_members, draw, warnings),
        title,
        urls,
        error_notification,
        None,
        Some(username),
        html! {
            @if show_reassurance { p { "(You can change all this later.)" } }
            // When target_page is createcircle, we don't actually have
            // to substitute anything here, but we call u2 anyway and the
            // second argument has no effect.
            @let action = urls.u2(target_page, vec![circleid]);
            form action={(action)} method="post" {
                div class="namevalue" {
                    label for="name" { "Circle Name" }
                    input
                        type="text"
                        id="name"
                        name="name"
                        value=(circle_name)
                        placeholder="E.g. Alice's Party"
                        {}
                    label for="draw_date" {
                        "Draw Date "
                        a href="#info_draw_date" class="info" { "?" }
                    }
                    input
                        type="text"
                        id="draw_date"
                        name="draw_date"
                        value=(draw_date)
                        placeholder="E.g. 12 December 2022"
                        {}
                    div id="close_info_draw_date" {}
                    div id="info_draw_date" {
                        strong { "Draw Date:" }
                        " when will the draw be done? This is the\n"
                        "        day when names will be chosen, not when the "
                        "gifts will be exchanged."
                        a href="#close_info" { "close" }
                    }
                    label for="rules" { "Rules" }
                    input
                        type="text"
                        id="rules"
                        name="rules"
                        value=(rules)
                        placeholder={ (rules_placeholder) }
                        {}
                }
                input
                    class="end-submit"
                    type="submit"
                    id="save_info"
                    name="save_info"
                    value=(submit_text)
                    {}
                h2 { "Members" }
                @if show_reassurance {
                    p { "(You can add or remove people later.)" }
                }
                table {
                    tr {
                        th { "Name" }
                        th { "Email" }
                        th class="wishlist" { "Wishlist?" }
                    }
                    @for ( n, member ) in
                        members.iter().chain(
                            two_empty_has_wishlist().iter()
                        ).enumerate()
                    {
                        tr {
                            td {
                                input
                                    type="text"
                                    id={ (format!("member-name-{}", n + 1)) }
                                    name={ (format!("member-name-{}", n + 1)) }
                                    value=(member.nickname)
                                    {}
                            }
                            td {
                                input
                                    type="text"
                                    id={ (format!("member-email-{}", n + 1)) }
                                    name={ (format!("member-email-{}", n + 1)) }
                                    value=(member.username)
                                    {}
                            }
                            td {
                                @if member.has_wishlist() {
                                    (PreEscaped("&#x1F31F;"))
                                }
                            }
                        }
                    }
                }
                p { "(Save to show more free slots.)" }
                input
                    class="end-submit"
                    type="submit"
                    id="save_members"
                    name="save_members"
                    value=(submit_text)
                    {}
                h2 { "Draw" }
                (draw_section)
                @if !draw.is_empty() {
                    h3 { "Cancel draw" }
                    input
                        class="danger"
                        type="submit"
                        id="cancel_draw"
                        name="cancel_draw"
                        value="Cancel the draw now!"
                        {}
                    p { "(You will be able to change the restrictions and draw again.)" }
                    }
                h2 { "Organisers" }
                @if show_reassurance {
                    p { "(You can add or remove people later.)" }
                }
                table {
                    tr { th { "Name" } th { "Email" } }
                    @for (n, UsernameNick {username: u, nickname}) in
                        organisers.iter().chain(two_empty().iter()).enumerate()
                    {
                        tr {
                            td {
                                input
                                    type="text"
                                    id={ (format!("organiser-name-{}", n + 1)) }
                                    name={ (format!("organiser-name-{}", n + 1)) }
                                    value=(nickname)
                                    placeholder=[
                                        if u == username {
                                            Some("Enter your name here")
                                        } else {
                                            None
                                        }
                                    ]
                                    {}
                            }
                            td {
                                input
                                    type="text"
                                    id={ (format!("organiser-email-{}", n + 1)) }
                                    name={ (format!("organiser-email-{}", n + 1)) }
                                    value=(u)
                                    {}
                            }
                        }
                    }
                }
                input
                    class="end-submit"
                    type="submit"
                    id="save_organisers"
                    name="save_organisers"
                    value=(submit_text)
                    {}
                @if circle_exists {
                    h2 { "Copy circle" }
                    p { "Create a new circle based on this one." }
                    input
                        class="end-submit"
                        type="submit"
                        id="copy_circle"
                        name="copy_circle"
                        value="Make a copy"
                        {}
                }
            }
            @if circle_exists {
                form action=(urls.u2("deletecircle", [circleid])) method="post" {
                    h2 { "Delete circle" }
                    div id="delete-circle-buttons" {
                       input
                           class="danger end-submit"
                           type="submit"
                           name="delete_circle"
                           id="delete_circle"
                           value="Delete this circle"
                           {}
                       input
                           type="checkbox"
                           name="delete_circle_sure"
                           id="delete_circle_sure"
                           {}
                       label for="delete_circle_sure" { "I am sure" }
                    }
                }
           }
        },
    )
}

fn render_warnings(
    members: &Vec<UsernameNick>,
    draw: &Vec<DrawItem>,
    warnings: &Vec<DrawWarning>,
) -> Option<Markup> {
    if !warnings.is_empty() && !draw.is_empty() {
        Some(html! {
            div class="warnings" {
                h2 { "Warnings" }
                @for warning in warnings {
                    p class=(warning_class(warning)) {
                        (PreEscaped(warning_sign(warning)))
                        (warning_text(members, warning))
                    }
                }
            }
        })
    } else {
        None
    }
}

fn render_draw(
    members: &Vec<UsernameNick>,
    draw: &Vec<DrawItem>,
    disallowed_draw: &Vec<DrawItem>,
    submit_text: &str,
) -> Markup {
    html!(
        @if members.iter().any(|mem| mem.username == "") {
             p class="draw-warning" {
                (PreEscaped("&#x26A0;&#xFE0F; "))
                "Members must have an email address before "
                "you can choose them."
            }
        }
        table class="draw" {
            @for (n, DrawItem {from_username, to_username}) in
                draw
                    .iter()
                    .chain(two_empty_draw_items().iter())
                    .enumerate()
            {
                tr {
                    td {
                        select
                            id={ (format!("draw-from-{}", n + 1)) }
                            name={ (format!("draw-from-{}", n + 1)) }
                            { (options(members, from_username)) }
                    }
                    td {
                        "is giving to"
                    }
                    td class="reveal" {
                        details open=[
                            if to_username.is_empty() {
                                Some(true)
                            } else {
                                None
                            }
                        ] {
                            select
                                id={ (format!("draw-to-{}", n + 1)) }
                                name={ (format!("draw-to-{}", n + 1)) }
                                { (options(members, to_username)) }
                            summary { "(click to reveal)" }
                        }
                    }
                }
            }
        }
        p { "(Save to update the available names.)" }
        input
            class="end-submit"
            type="submit"
            id="save_draw"
            name="save_draw"
            value=(submit_text)
            {}
        @for (n, DrawItem {from_username, to_username}) in
            disallowed_draw.iter().enumerate()
        {
            input
                type="hidden"
                id={ (format!("restriction-from-{}", n + 1)) }
                name={ (format!("restriction-from-{}", n + 1)) }
                value={ (from_username) }
                {}
            input
                type="hidden"
                id={ (format!("restriction-to-{}", n + 1)) }
                name={ (format!("restriction-to-{}", n + 1)) }
                value={ (to_username) }
                {}
        }
    )
}

fn render_restrictions(
    members: &Vec<UsernameNick>,
    disallowed_draw: &Vec<DrawItem>,
    submit_text: &str,
) -> Markup {
    html!(
    p {
        "The names have not yet been drawn to decide who"
        " gives presents to whom."
    }
    h3 { "Restrictions" }
    @if members.iter().any(|mem| mem.username == "") {
         p class="draw-warning" {
            (PreEscaped("&#x26A0;&#xFE0F; "))
            "Members must have an email address before "
            "you can choose them."
        }
    }
    p { "Choose here any restrictions on the draw:" }
    table class="draw" {
        @for (n, DrawItem {from_username, to_username}) in
            disallowed_draw
                .iter()
                .chain(two_empty_draw_items().iter())
                .enumerate()
        {
            tr {
                td {
                    select
                        id={ (format!("restriction-from-{}", n + 1)) }
                        name={ (format!("restriction-from-{}", n + 1)) }
                        { (options(members, from_username)) }
                }
                td {
                    "cannot give to"
                }
                td {
                    select
                        id={ (format!("restriction-to-{}", n + 1)) }
                        name={ (format!("restriction-to-{}", n + 1)) }
                        { (options(members, to_username)) }
                }
            }
        }
    }
    p { "(Save to update the available names.)" }
    input
        class="end-submit"
        type="submit"
        id="save_restrictions"
        name="save_restrictions"
        value=(submit_text)
        {}
    h3 { "Draw now" }
    @if members.iter().any(|mem| mem.username == "") {
         p class="draw-warning" {
            (PreEscaped("&#x26A0;&#xFE0F; "))
            "Members must have an email address before "
            "they are included in the draw."
        }
    }
    input
        type="submit"
        id="draw_now"
        name="draw_now"
        value={
            @if members.iter().any(|mem| mem.username == "") {
                (PreEscaped("&#x26A0;&#xFE0F; Draw now!"))
            } @else {
                "Draw now!"
            }
        }
        {}
    )
}

fn two_empty() -> Vec<UsernameNick> {
    vec![
        UsernameNick {
            username: String::from(""),
            nickname: String::from(""),
        },
        UsernameNick {
            username: String::from(""),
            nickname: String::from(""),
        },
    ]
}

fn two_empty_has_wishlist() -> Vec<UsernameNickHasWishlist> {
    vec![
        UsernameNickHasWishlist {
            username: String::from(""),
            nickname: String::from(""),
            wishlist_text: None,
            wishlist_links: None,
        },
        UsernameNickHasWishlist {
            username: String::from(""),
            nickname: String::from(""),
            wishlist_text: None,
            wishlist_links: None,
        },
    ]
}

fn two_empty_draw_items() -> Vec<DrawItem> {
    vec![
        DrawItem {
            from_username: String::from(""),
            to_username: String::from(""),
        },
        DrawItem {
            from_username: String::from(""),
            to_username: String::from(""),
        },
    ]
}

fn options(members: &Vec<UsernameNick>, selected: &str) -> Markup {
    let blank = UsernameNick {
        username: String::from(""),
        nickname: String::from(""),
    };
    let blank_and_members = std::iter::once(&blank)
        .chain(members.iter().filter(|mem| mem.username != ""));
    let mut done_selection = false;

    html! {
        @for UsernameNick { username, nickname } in blank_and_members {
            @let n = if nickname.is_empty() { username } else { nickname };
            @if selected == username && !done_selection {
                ({
                    done_selection = true;
                    html! {option value=(username) selected { (n) }}
                })
            } @else {
                option value=(username) { (n) }
            }
        }
    }
}

fn name<'a>(members: &Vec<UsernameNick>, username: &str) -> String {
    for member in members {
        if member.username == username {
            return String::from(member.display_name());
        }
    }
    String::from("")
}

fn warning_class(warning: &DrawWarning) -> &'static str {
    match warning.severity() {
        DrawWarningSeverity::Warning => "draw-warning",
        DrawWarningSeverity::Info => "draw-info",
    }
}

fn warning_text(members: &Vec<UsernameNick>, warning: &DrawWarning) -> String {
    match warning {
        DrawWarning::NotReceiving(username) => {
            format!("No-one is giving to {}!", name(members, username))
        }
        DrawWarning::NotGiving(username) => {
            format!("{} is not giving to anyone!", name(members, username))
        }
        DrawWarning::GivingMultiple(from_username, to_usernames) => format!(
            "{} is giving to {} people",
            name(members, from_username),
            to_usernames.len()
        ),
        DrawWarning::NoUsername(nickname) => format!(
            "{} has no email address - they can't be included in the draw!",
            nickname,
        ),
        DrawWarning::ReceivingMultiple(to_username, from_usernames) => format!(
            "{} people are giving to {}",
            from_usernames.len(),
            name(members, to_username),
        ),
        DrawWarning::BreaksRestriction(draw_item) => format!(
            "{} is giving to {} but that breaks a restriction!",
            name(members, &draw_item.from_username),
            name(members, &draw_item.to_username),
        ),
        DrawWarning::EmptyFrom(to_username) => {
            format!("<blank> is giving to {}", name(members, to_username))
        }
        DrawWarning::EmptyTo(from_username) => {
            format!("{} is giving to <blank>", name(members, from_username))
        }
    }
}

fn warning_sign(warning: &DrawWarning) -> &str {
    match warning.severity() {
        DrawWarningSeverity::Warning => "&#x26A0;&#xFE0F; ",
        DrawWarningSeverity::Info => "&#x2139;&#xFE0F; ",
    }
}
