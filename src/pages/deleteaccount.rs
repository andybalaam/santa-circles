use actix_http::http;
use actix_web::{web, HttpRequest, HttpResponse};
use maud::{html, Markup};
use serde::Deserialize;

use crate::credentials::Credentials;
use crate::data_holder::DataHolder;
use crate::lsc_error::LscError;
use crate::pages::all;
use crate::urls::{UrlPrefix, Urls};

#[derive(Deserialize)]
pub struct SureForm {
    sure: Option<String>,
    really_sure: Option<String>,
}

pub async fn post(
    req: HttpRequest,
    sure_form: web::Form<SureForm>,
    url_prefix: UrlPrefix,
    data_holder: web::Data<DataHolder>,
) -> Result<HttpResponse, LscError> {
    let urls = url_prefix.bind(&req);
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            let sure =
                sure_form.sure.as_ref().map(|s| s == "on").unwrap_or(false);
            let really_sure = sure_form
                .really_sure
                .as_ref()
                .map(|s| s == "on")
                .unwrap_or(false);

            if !sure {
                let mut re_url = urls.url_for_static("account").unwrap();
                re_url.set_query(Some("error=sure_required"));
                Ok(HttpResponse::Found()
                    .header(http::header::LOCATION, re_url.to_string())
                    .finish()
                    .into_body())
            } else if !really_sure {
                Ok(HttpResponse::Ok().body(
                    render(&credentials.username, &urls, None, None)
                        .into_string(),
                ))
            } else {
                delete_user_and_redirect(
                    &req,
                    &urls,
                    &data_holder,
                    &credentials,
                )
                .await
            }
        }
        Err(e) => Err(e),
    }
}

async fn delete_user_and_redirect(
    req: &HttpRequest,
    urls: &impl Urls,
    data_holder: &DataHolder,
    credentials: &Credentials,
) -> Result<HttpResponse, LscError> {
    let url = format!("/v1/user/{}", credentials.username);
    data_holder
        .api
        .delete(&credentials, &url, None)
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .map(|_| {
            let mut re_url = urls.url_for_static("login").unwrap();
            re_url.set_query(Some("message=account_deleted"));
            HttpResponse::Found()
                .header(http::header::LOCATION, re_url.to_string())
                .finish()
                .into_body()
        })
}

pub fn render(
    username: &str,
    urls: &impl Urls,
    error_notification: Option<Markup>,
    notification: Option<Markup>,
) -> Markup {
    all::render(
        None,
        "Delete Account?",
        urls,
        error_notification,
        notification,
        Some(username),
        html! {
            p { "Delete account " (username) "?" }
            p { r#"Confirm by choosing "I am REALLY sure"."# }
            form
                id="delete-user-form"
                class="secondary-section"
                action=(urls.u("deleteaccount"))
                method="post"
            {
                input
                    type="submit"
                    name="delete-user"
                    class="danger"
                    value="Delete myself and my circles"
                    {}
                input type="hidden" name="sure" value="on" {}
                input
                    type="checkbox"
                    name="really_sure"
                    id="really_sure"
                    {}
                label for="really_sure" { "I am REALLY sure" }
            }
        },
    )
}
