use actix_web::HttpRequest;
use maud::{html, Markup};

use crate::pages::all;
use crate::urls::{UrlPrefix, Urls};

pub async fn get(req: HttpRequest, url_prefix: UrlPrefix) -> Markup {
    let urls = url_prefix.bind(&req);
    render(&urls)
}

pub fn render(urls: &impl Urls) -> Markup {
    all::render(
        Some(html! { header { a href=(urls.u("login")) { "Login" } } }),
        "Forgot password",
        urls,
        None,
        None,
        None,
        html! {
            p { "Enter your email address to request a reset password email:" }
            form
                class="namevalue"
                action={(urls.u("resetpassword"))}
                method="post"
            {
                label for="username" { "Email" }
                input
                    type="text"
                    id="username"
                    name="username"
                    value=""
                    placeholder="E.g. name@example.com" {}
                span {}
                div {
                    button type="submit" { "Request password reset code" }
                }
            }
        },
    )
}
