use actix_web::{web, HttpRequest};
use maud::{html, Markup, PreEscaped};

use crate::data_holder::DataHolder;
use crate::lsc_error::LscError;
use crate::pages::all;
use crate::urls::{UrlPrefix, Urls};

pub async fn get(
    req: HttpRequest,
    url_prefix: UrlPrefix,
    data_holder: web::Data<DataHolder>,
) -> Result<Markup, LscError> {
    // Note: no credentials needed for this page.
    let urls = url_prefix.bind(&req);
    Ok(render(&urls, &data_holder.contact_page.contents))
}

pub fn render(urls: &impl Urls, contact_html: &str) -> Markup {
    all::render(
        Some(html! {
            header class="user" {
                a href=(urls.u("circles")) { "My Circles" }
            }
        }),
        "Contact",
        urls,
        None,
        None,
        None,
        html! {
            div class="info-div" {
            (PreEscaped(contact_html))
            }
        },
    )
}
