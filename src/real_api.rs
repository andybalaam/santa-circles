use async_trait::async_trait;
use log::{debug, error, info};
use reqwest;

use crate::api::{Api, ApiError};
use crate::credentials::Credentials;

pub struct RealApi {
    server_url: String,
    reqwest_client: reqwest::Client,
}

impl RealApi {
    pub fn new(server_url: String) -> RealApi {
        RealApi {
            server_url,
            reqwest_client: reqwest::Client::new(),
        }
    }

    async fn request(
        &self,
        credentials: Option<&Credentials>,
        path: &str,
        method: reqwest::Method,
        body: Option<serde_json::Value>,
    ) -> Result<serde_json::Value, ApiError> {
        let url = format!("{}{}", self.server_url, path);
        info!("{} {}", method, url);
        debug!(
            "Request content: {}",
            if let Some(b) = &body {
                b.to_string()
            } else {
                String::from("")
            }
        );
        let req = self.reqwest_client.request(method, &url);

        let req = if let Some(credentials) = credentials {
            req.basic_auth(&credentials.username, Some(&credentials.token))
        } else {
            req
        };

        let req = if let Some(body) = body {
            req.json(&body)
        } else {
            req
        };

        let res: reqwest::Response = req.send().await.map_err(|_e| {
            ApiError::InternalError(String::from("Sending request failed."))
        })?;

        let status = res.status();
        if status == reqwest::StatusCode::NO_CONTENT {
            info!("Response {} from {}", status, url);
            return Ok(serde_json::Value::String(String::from("")));
        }

        if status.is_success() {
            info!("Response {} from {}", status, url);

            let text = res.text().await.map_err(|_e| {
                ApiError::InternalError(format!(
                    "Unable to read response. Status: {}",
                    status
                ))
            })?;

            let json = serde_json::from_str(&text).map_err(|_e| {
                ApiError::InternalError(format!(
                    "Response was not valid JSON: {} {}",
                    status, text
                ))
            })?;

            Ok(json)
        } else {
            let text = res
                .text()
                .await
                .map_err(|_e| {
                    error!("Unable to read response. Status: {}", status)
                })
                .unwrap_or(String::from(""));

            error!("Error {} {} from {}", status, text, url);
            Err(to_api_error(status, text).await)
        }
    }
}

async fn to_api_error(status: reqwest::StatusCode, text: String) -> ApiError {
    match status {
        reqwest::StatusCode::UNAUTHORIZED => ApiError::AuthenticationFailed,
        reqwest::StatusCode::FORBIDDEN => ApiError::Forbidden,
        reqwest::StatusCode::CONFLICT => ApiError::UserAlreadyExists,
        _ => ApiError::InternalError(format!("{} {}", status, text)),
    }
}

#[async_trait]
impl Api for RealApi {
    async fn get(
        &self,
        credentials: &Credentials,
        path: &str,
    ) -> Result<serde_json::Value, ApiError> {
        self.request(Some(credentials), path, reqwest::Method::GET, None)
            .await
    }

    async fn get_no_credentials(
        &self,
        path: &str,
    ) -> Result<serde_json::Value, ApiError> {
        self.request(None, path, reqwest::Method::GET, None).await
    }

    async fn put(
        &self,
        credentials: &Credentials,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        self.request(Some(credentials), path, reqwest::Method::PUT, Some(body))
            .await
    }

    async fn put_no_credentials(
        &self,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        self.request(None, path, reqwest::Method::PUT, Some(body))
            .await
    }

    async fn post(
        &self,
        credentials: &Credentials,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        self.request(Some(credentials), path, reqwest::Method::POST, Some(body))
            .await
    }

    async fn post_no_credentials(
        &self,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        self.request(None, path, reqwest::Method::POST, Some(body))
            .await
    }

    async fn delete(
        &self,
        credentials: &Credentials,
        path: &str,
        body: Option<serde_json::Value>,
    ) -> Result<serde_json::Value, ApiError> {
        self.request(Some(credentials), path, reqwest::Method::DELETE, body)
            .await
    }
}
