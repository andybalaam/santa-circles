use serde::Deserialize;

#[derive(Deserialize)]
#[serde(untagged)]
pub enum StringOrBool {
    S(String),
    B(bool),
}
