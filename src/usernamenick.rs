use serde::{Deserialize, Serialize};

use crate::wishlist_link::WishlistLink;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct UsernameNick {
    pub username: String,
    pub nickname: String,
}

impl UsernameNick {
    pub fn display_name(&self) -> &str {
        if self.nickname != "" {
            &self.nickname
        } else {
            &self.username
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct UsernameNickHasWishlist {
    pub username: String,
    pub nickname: String,
    pub wishlist_text: Option<String>,
    pub wishlist_links: Option<Vec<WishlistLink>>,
}

impl UsernameNickHasWishlist {
    pub fn has_wishlist(&self) -> bool {
        if let Some(text) = &self.wishlist_text {
            if text.len() > 0 {
                return true;
            }
        }
        if let Some(links) = &self.wishlist_links {
            if links.len() > 0 {
                return true;
            }
        }
        false
    }
}
