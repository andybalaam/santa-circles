--- cookie_session-v0.4.1.rs	2022-01-02 10:40:36.314511773 +0000
+++ cookie.rs	2022-01-02 10:43:44.050742383 +0000
@@ -14,7 +14,7 @@
 use serde_json::error::Error as JsonError;
 use time::{Duration, OffsetDateTime};
 
-use crate::{Session, SessionStatus};
+use crate::actix_session_remember::{Session, SessionStatus};
 
 /// Errors that can occur during handling cookie session
 #[derive(Debug, From, Display)]
@@ -71,6 +71,7 @@
         &self,
         res: &mut ServiceResponse<B>,
         state: impl Iterator<Item = (String, String)>,
+        remember_me: Option<bool>,
     ) -> Result<(), Error> {
         let state: HashMap<String, String> = state.collect();
 
@@ -105,6 +106,21 @@
             cookie.set_same_site(same_site);
         }
 
+        if let Some(remember_me) = remember_me {
+            // We were explicitly told to remember this user
+            if remember_me {
+                // TODO: hard-coded to 30 days
+                cookie.set_max_age(time::Duration::days(30));
+            } else {
+                // TODO: no way to UNset max age on a cookie!
+            }
+        } else {
+            if let Some(max_age) = self.max_age {
+                // The default was set when we created the CookieSession
+                cookie.set_max_age(max_age);
+            }
+        }
+
         let mut jar = CookieJar::new();
 
         match self.security {
@@ -192,7 +208,7 @@
 ///
 /// # Examples
 /// ```
-/// use actix_session::CookieSession;
+/// use santa_circles::actix_session_remember::CookieSession;
 /// use actix_web::{web, App, HttpResponse, HttpServer};
 ///
 /// let app = App::new().wrap(
@@ -358,28 +374,38 @@
         async move {
             fut.await.map(|mut res| {
                 match Session::get_changes(&mut res) {
-                    (SessionStatus::Changed, Some(state))
-                    | (SessionStatus::Renewed, Some(state)) => {
-                        res.checked_expr(|res| inner.set_cookie(res, state))
-                    }
-                    (SessionStatus::Unchanged, Some(state))
-                        if prolong_expiration =>
-                    {
-                        res.checked_expr(|res| inner.set_cookie(res, state))
-                    }
-                    (SessionStatus::Unchanged, _) =>
+                    (
+                        SessionStatus::Changed,
+                        Some(state),
+                        Some(remember_me),
+                    )
+                    | (
+                        SessionStatus::Renewed,
+                        Some(state),
+                        Some(remember_me),
+                    ) => res.checked_expr(|res| {
+                        inner.set_cookie(res, state, remember_me)
+                    }),
+                    (
+                        SessionStatus::Unchanged,
+                        Some(state),
+                        Some(remember_me),
+                    ) if prolong_expiration => res.checked_expr(|res| {
+                        inner.set_cookie(res, state, remember_me)
+                    }),
+                    (SessionStatus::Unchanged, _, _) =>
                     // set a new session cookie upon first request (new client)
                     {
                         if is_new {
                             let state: HashMap<String, String> = HashMap::new();
                             res.checked_expr(|res| {
-                                inner.set_cookie(res, state.into_iter())
+                                inner.set_cookie(res, state.into_iter(), None)
                             })
                         } else {
                             res
                         }
                     }
-                    (SessionStatus::Purged, _) => {
+                    (SessionStatus::Purged, _, _) => {
                         let _ = inner.remove_cookie(&mut res);
                         res
                     }
@@ -418,6 +444,50 @@
     }
 
     #[actix_rt::test]
+    async fn dont_remember_me() {
+        let mut app = test::init_service(
+            App::new()
+                .wrap(CookieSession::signed(&[0; 32]).secure(false))
+                .service(
+                    web::resource("/")
+                        .to(|_ses: Session| async move { "test" }),
+                ),
+        )
+        .await;
+
+        let request = test::TestRequest::get().to_request();
+        let response = app.call(request).await.unwrap();
+        let cookie = response
+            .response()
+            .cookies()
+            .find(|c| c.name() == "actix-session")
+            .unwrap();
+        assert_eq!(cookie.max_age(), None);
+    }
+
+    #[actix_rt::test]
+    async fn remember_me() {
+        let mut app = test::init_service(
+            App::new()
+                .wrap(CookieSession::signed(&[0; 32]).secure(false))
+                .service(web::resource("/").to(|ses: Session| async move {
+                    ses.remember_me(true);
+                    "test"
+                })),
+        )
+        .await;
+
+        let request = test::TestRequest::get().to_request();
+        let response = app.call(request).await.unwrap();
+        let cookie = response
+            .response()
+            .cookies()
+            .find(|c| c.name() == "actix-session")
+            .unwrap();
+        assert_eq!(cookie.max_age(), Some(time::Duration::days(30)));
+    }
+
+    #[actix_rt::test]
     async fn private_cookie() {
         let mut app = test::init_service(
             App::new()
@@ -575,4 +645,56 @@
 
         assert!(expires_2 - expires_1 >= Duration::seconds(1));
     }
+
+    #[actix_rt::test]
+    async fn purge() {
+        let mut app = test::init_service(
+            App::new()
+                .wrap(
+                    CookieSession::signed(&[0; 32])
+                        .path("/test/")
+                        .name("actix-test")
+                        .domain("localhost")
+                        .http_only(true)
+                        .same_site(SameSite::Lax)
+                        .max_age(100),
+                )
+                .service(web::resource("/").to(|ses: Session| async move {
+                    let _ = ses.set("a", 1);
+                    "test"
+                }))
+                .service(web::resource("/p").to(|ses: Session| async move {
+                    ses.purge();
+                    "purged"
+                })),
+        )
+        .await;
+
+        let request = test::TestRequest::get().to_request();
+        let response = app.call(request).await.unwrap();
+        let cookie = response
+            .response()
+            .cookies()
+            .find(|c| c.name() == "actix-test")
+            .unwrap()
+            .clone();
+        assert_eq!(cookie.path().unwrap(), "/test/");
+
+        let request = test::TestRequest::with_uri("/p")
+            .cookie(cookie)
+            .to_request();
+        let response = app.call(request).await.unwrap();
+        let cookie = response
+            .response()
+            .cookies()
+            .find(|c| c.name() == "actix-test")
+            .unwrap()
+            .clone();
+        assert_eq!(cookie.path().unwrap(), "/test/");
+        assert_eq!(cookie.value(), "");
+        assert_eq!(cookie.max_age().unwrap(), time::Duration::seconds(0));
+        assert_eq!(cookie.domain(), None);
+        assert_eq!(cookie.http_only(), None);
+        assert_eq!(cookie.same_site(), None);
+    }
 }
