use actix_web::body::Body;
use actix_web::http::StatusCode;
use actix_web::{http, HttpRequest, HttpResponse, ResponseError};
use log::error;
use maud::html;
use std::fmt::{Display, Formatter};
use url::Url;

use crate::api::ApiError;
use crate::pages::all;
use crate::urls::Urls;

/**
 * An error specific to Santa Circles.
 */
#[derive(Debug)]
pub enum LscError {
    AuthenticationFailed(Url),
    Forbidden(Url),
    InternalError {
        message: String,
        home_url: Url,
        style_css_url: Url,
        contact_url: Url,
        privacy_url: Url,
    },
    BadInput {
        message: String,
        style_css_url: Url,
        contact_url: Url,
        privacy_url: Url,
    },
    NoCredentialsProvided(Url),
}

impl LscError {
    pub fn no_credentials_provided(urls: &impl Urls) -> LscError {
        LscError::NoCredentialsProvided(urls.url_for_static("login").unwrap())
    }
    pub fn authentication_failed(urls: &impl Urls) -> LscError {
        let mut url = urls.url_for_static("login").unwrap();
        url.set_query(Some("error=login_failed"));
        LscError::AuthenticationFailed(url)
    }
    pub fn forbidden(urls: &impl Urls) -> LscError {
        let mut url = urls.url_for_static("root").unwrap();
        url.set_query(Some("failed=true"));
        LscError::Forbidden(url)
    }
    pub fn internal_error(
        message: &str,
        req: &HttpRequest,
        urls: &impl Urls,
        private_log_message: &str,
    ) -> LscError {
        dbg!(private_log_message);
        error!("Internal error at {}: {}", req.path(), private_log_message);
        LscError::InternalError {
            message: String::from(message),
            home_url: urls.url_for_static("root").unwrap(),
            style_css_url: urls.url_for_static("style_css").unwrap(),
            contact_url: urls.url_for_static("contact").unwrap(),
            privacy_url: urls.url_for_static("privacy").unwrap(),
        }
    }
    pub fn bad_input(message: &str, urls: &impl Urls) -> LscError {
        LscError::BadInput {
            message: String::from(message),
            style_css_url: urls.url_for_static("style_css").unwrap(),
            contact_url: urls.url_for_static("contact").unwrap(),
            privacy_url: urls.url_for_static("privacy").unwrap(),
        }
    }
    pub fn from_api_error(
        req: &HttpRequest,
        urls: &impl Urls,
        api_error: ApiError,
    ) -> LscError {
        match api_error {
            ApiError::AuthenticationFailed => {
                LscError::authentication_failed(urls)
            }
            ApiError::Forbidden => LscError::forbidden(urls),
            ApiError::InternalError(msg) => LscError::internal_error(
                "Api returned internal error",
                req,
                urls,
                &format!("API returned Internal Error: {}", msg),
            ),
            ApiError::UserAlreadyExists => LscError::internal_error(
                "User already exists",
                req,
                urls,
                "User already exists.",
            ),
            ApiError::CircleCreatorMustBeAnOrganiser => LscError::bad_input(
                "If you create a circle, you must be an organiser!",
                urls,
            ),
            ApiError::CircleDoesNotExist => {
                LscError::bad_input("Circle does not exist", urls)
            }
        }
    }
}

impl Display for LscError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "{}",
            match self {
                LscError::AuthenticationFailed(_) =>
                    String::from("Supplied login details are not correct."),
                LscError::Forbidden(_) =>
                    String::from("You do not have permission to do this."),
                LscError::InternalError { .. } =>
                    String::from("Internal error"),
                LscError::BadInput { message, .. } =>
                    format!("Bad input: {}", message),
                LscError::NoCredentialsProvided(_) =>
                    String::from("Login is required."),
            }
        )
    }
}

impl ResponseError for LscError {
    fn status_code(&self) -> StatusCode {
        match self {
            LscError::AuthenticationFailed(_) => StatusCode::UNAUTHORIZED,
            LscError::Forbidden(_) => StatusCode::FORBIDDEN,
            LscError::InternalError { .. } => StatusCode::INTERNAL_SERVER_ERROR,
            LscError::BadInput { .. } => StatusCode::BAD_REQUEST,
            LscError::NoCredentialsProvided(_) => StatusCode::UNAUTHORIZED,
        }
    }
    fn error_response(&self) -> HttpResponse<Body> {
        match self {
            LscError::AuthenticationFailed(url) => redirect_response(url),
            LscError::Forbidden(url) => redirect_response(url),
            LscError::InternalError {
                message,
                home_url,
                style_css_url,
                contact_url,
                privacy_url,
            } => internal_error_response(
                message,
                home_url,
                style_css_url,
                contact_url,
                privacy_url,
            ),
            LscError::BadInput {
                message,
                style_css_url,
                contact_url,
                privacy_url,
            } => bad_input_response(
                message,
                style_css_url.as_str(),
                contact_url.as_str(),
                privacy_url.as_str(),
            ),
            LscError::NoCredentialsProvided(url) => redirect_response(url),
        }
    }
}

fn internal_error_response(
    message: &str,
    home_url: &Url,
    style_css_url: &Url,
    contact_url: &Url,
    privacy_url: &Url,
) -> HttpResponse {
    let page = all::raw_render(
        html! {},
        "Internal error",
        style_css_url.as_str(),
        contact_url.as_str(),
        privacy_url.as_str(),
        html! { header.notification.error { "Internal error!" } },
        html! {},
        html! {},
        html! {
            p {
                "Sorry, something went wrong.  Please try again, "
                "and if that doesn't work, go back to "
                a href=(home_url) { "My Circles" }
                " or "
                a href=(contact_url) {
                    "contact the server administrators"
                }
                "."
            }
            p {
                "Error message:"
                pre { (message) }
            }
        },
    );
    HttpResponse::InternalServerError().body(page.into_string())
}

fn redirect_response(url: &Url) -> HttpResponse {
    HttpResponse::Found()
        .header(http::header::LOCATION, url.to_string())
        .finish()
        .into_body()
}

fn bad_input_response(
    message: &str,
    style_css_url: &str,
    contact_url: &str,
    privacy_url: &str,
) -> HttpResponse {
    HttpResponse::BadRequest().body(String::from(all::raw_render(
        html! {},
        "Unexpected input",
        style_css_url,
        contact_url,
        privacy_url,
        html! {},
        html! {},
        html! {},
        html! { p { (message) } },
    )))
}
