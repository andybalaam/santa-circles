use crate::member::Member;
use crate::wishlist_link::WishlistLink;

#[derive(Debug)]
pub struct Recipient {
    pub name: String,
    pub wishlist_text: String,
    pub wishlist_links: Vec<WishlistLink>,
}

impl Recipient {
    pub fn new(
        name: &str,
        wishlist_text: &str,
        wishlist_links: Vec<WishlistLink>,
    ) -> Self {
        Self {
            name: String::from(name),
            wishlist_text: String::from(wishlist_text),
            wishlist_links,
        }
    }

    pub fn extract_from_circle(
        username: &str,
        members: &Vec<Member>,
    ) -> Option<Self> {
        members
            .iter()
            .find(|member| member.username == username)
            .map(|member| Self {
                name: String::from(member.display_name()),
                wishlist_text: member.wishlist_text.clone(),
                wishlist_links: member.wishlist_links.clone(),
            })
    }

    pub fn has_wishlist(&self) -> bool {
        self.wishlist_text != "" || !self.wishlist_links.is_empty()
    }
}
