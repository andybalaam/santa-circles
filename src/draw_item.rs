use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct DrawItem {
    pub from_username: String,
    pub to_username: String,
}
