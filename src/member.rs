use serde::{Deserialize, Serialize};

use crate::wishlist_link::WishlistLink;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Member {
    pub username: String,
    pub nickname: String,
    pub wishlist_text: String,
    pub wishlist_links: Vec<WishlistLink>,
}

impl Member {
    pub fn display_name(&self) -> &str {
        if self.nickname != "" {
            &self.nickname
        } else {
            &self.username
        }
    }
}
