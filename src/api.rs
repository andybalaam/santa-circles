use async_trait::async_trait;

use crate::credentials::Credentials;

#[derive(Debug)]
pub enum ApiError {
    AuthenticationFailed,
    CircleCreatorMustBeAnOrganiser,
    Forbidden,
    InternalError(String),
    UserAlreadyExists,
    CircleDoesNotExist,
}

#[async_trait]
pub trait Api {
    async fn get(
        &self,
        credentials: &Credentials,
        path: &str,
    ) -> Result<serde_json::Value, ApiError>;

    async fn get_no_credentials(
        &self,
        path: &str,
    ) -> Result<serde_json::Value, ApiError>;

    async fn put(
        &self,
        credentials: &Credentials,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError>;

    async fn put_no_credentials(
        &self,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError>;

    async fn post(
        &self,
        credentials: &Credentials,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError>;

    async fn post_no_credentials(
        &self,
        path: &str,
        body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError>;

    async fn delete(
        &self,
        credentials: &Credentials,
        path: &str,
        body: Option<serde_json::Value>,
    ) -> Result<serde_json::Value, ApiError>;
}
