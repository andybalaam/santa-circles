use crate::api::Api;
use crate::contact_page::ContactPage;
use crate::urls::UrlPrefix;

pub struct DataHolder {
    pub api: Box<dyn Api>,
    pub contact_page: ContactPage,
    pub url_prefix: UrlPrefix,
}

impl DataHolder {
    pub fn new(
        api: Box<dyn Api>,
        contact_page: ContactPage,
        url_prefix: UrlPrefix,
    ) -> DataHolder {
        DataHolder {
            api,
            contact_page,
            url_prefix,
        }
    }
}
