all: test

fmt:
	cargo fmt

build: fmt
	cargo build

test: build
	cargo test -- --nocapture

watch-test:
	cargo watch --shell "make test"

run: test
	cargo run

run-without-test:
	cargo run

watch-run:
	cargo watch --shell "make run"

doc:
	rustup doc
	cargo doc --open

DEPL := santacircles.artificialworlds.net:santa-circles

deploy:
	# Prerequisites:
	# rustup target add x86_64-unknown-linux-musl
	# sudo apt install musl-tools
	cargo build --target x86_64-unknown-linux-musl --release
	scp \
		target/x86_64-unknown-linux-musl/release/santa-circles \
		${DEPL}/santa-circles.new
	scp contact-artificialworlds.html ${DEPL}/contact.html
