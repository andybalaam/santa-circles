# TODO

## Basic site

- [x] Login, registration
- [x] Circles (organiser)
- [x] Circles (member)
- [x] Deployment
- [x] Set up regular backups in line with privacy policy
- [x] Serve up .well-known
- [x] Release!
- [x] UI removes spaces from beginning and end of all usernames
- [x] API should return draw items in alphabetical order (when draw is created)
- [x] Explain what it is on the login page

## Admin help, and password reset (0.3.0)

- [x] Admin can see who made a wish list
- [x] Allow copying circles
- [x] Password reset via email

## More admin help

- [ ] Case-insensitive usernames
- [ ] Admin can see who is registered
- [ ] Admin can send reminder email for each stage

## Prettier

- [ ] Feedback when you save your wishlist - it seems like nothing happened
- [ ] Explain what it is on the login page
- [ ] Show status of each member e.g. Registered, made wishes, acquired gift
- [ ] Show organiser on member page
- [ ] Dynamic lists, when JS is available
- [ ] Display user's pretty name in top right
- [ ] Cheery background image
- [ ] Cheery colour scheme
- [ ] Auto-login after registration

## Safety

- [ ] Invites to circles, instead of auto-joining
- [ ] Block players
- [ ] Monitoring and logging
- [ ] Security limits e.g. blocking after too many login attempts

## Code cleanup

- [ ] Upgrade API to more recent actix-web
- [ ] Really, really make it log better when there is an error
- [ ] Upgrade API to more recent actix-web
- [ ] Fix the Urls thing so it's less awkward
- [ ] Rename LscError to ScError
- [ ] Contribute actix-session change upstream
- [ ] Stop redirecting after posting to organisecircle or circle?
- [ ] Document API usage in its README
- [ ] Publish OpenAPI docs for the API
- [ ] API: Make Database async
- [ ] API: Return JSON on invalid username/password
- [ ] Check all TODOs and do them
- [ ] Restrict usernames to url-friendly characters, and add friendly names

## Future

- [ ] Allow Matrix addresses as well as emails
- [ ] Scalable data storage e.g. MariaDB, MongoDB
- [ ] Stop returning registration code over HTTP - use email?
- [ ] Send emails on your behalf
- [ ] Port to a Matrix bot
