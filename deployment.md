## Deployment notes

After running `make deploy`:

```bash
ssh santacircles.artificialworlds.net
cd santa-circles
tmux
ln -s ../santacircles.artificialworlds.net/.well-known
mv santa-circles.new santa-circles
./santa-circles
```

Proxy: santacircles.artificialworlds.net 8089
