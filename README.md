# Santa Circles (secret santa gift exchange)

To use Santa Circles, visit
[santacircles.artificialworlds.net](https://santacircles.artificialworlds.net).

This is the client component of Santa Circles, a "secret santa" gift exchange.

It uses server-side rendered HTML for most of its UI, with some minimal
optional JavaScript for convenience.

It requires a running server like
[santa-circles-api](https://gitlab.com/andybalaam/santa-circles-api).

## Prerequisites

[Install Rust](https://www.rust-lang.org/tools/install).

Optionally, to use the convenience commands in Makefile:

```bash
rustup component add rustfmt
cargo install cargo-watch
```

## Build

```bash
cargo test
```

## Run

Run the server (santa-circles-api) locally, then run:

```bash
cargo run
```

Now visit http://127.0.0.1:8089/

For more options, including specifying where to find the server and what port
to listen on, run:

```bash
cargo run -- --help
```

## Containerisation

There is a Dockerfile to build an arm64 image:

```
FROM debian:buster-slim

RUN mkdir /opt/santa-circles

COPY target/aarch64-unknown-linux-gnu/release/santa-circles /usr/local/bin/santa-circles

WORKDIR /opt/santa-circles (this folder should be mapped when the container is run, to allow persistent config files)
```

## Helm chart

To enable running in Kubernetes, a sample Helm chart has been provided (there is also a Dockerfile and Helm chart 

for santa circles client api)

You should deploy the API and client into the same namespace.

Example values:

```
---
image: myrepo/santa-circles (the repo that contains the built image)
tag: latest (the image tag)
name: santa-circles (the name of the deployment)
namespace: santa-circles (the namespace - this needs to be created before deployment)
host: santacircles.domain.com (the domain name)
secret: santa-circles-tls (the kubernetes secret name)
ingress: "nginx" (the type of ingress, in this case ngnix)
cert_manager: "letsencrypt-prod" (the certificate manager, in this case letsencrypt)
config: /data/santa-circles (the local folder to map)
```

Connecting to the API. A ClusterIP service is configured by the API that exposes port 8088 at cluster level.

The santa circles client then connects to the service using:

```
command: ["santa-circles"]
        args: ["--server_url", "http://santa-circles-api:8088"]
```

## License

Copyright 2020-2022 Andy Balaam and contributors, released under the
[AGPLv3 license](LICENSE) or later.

Source files inside `src/actix_session_remember` are forked from
[actix-session](https://github.com/actix/actix-extras/tree/master/actix-session/src)
(at tag
[session-v0.4.1](https://github.com/actix/actix-extras/releases/tag/session-v0.4.1))
- these files were released under a choice of Apache 2.0 or MIT licenses, and
the derived files are released here under the same conditions.

## Code of conduct

Please note that this project is released with a
[Contributor Code of Conduct](code_of_conduct.md).  By participating in this
project you agree to abide by its terms.

[![Contributor Covenant](contributor-covenant-v2.0-adopted-ff69b4.svg)](code_of_conduct.md)
